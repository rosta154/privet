<?php

namespace App\Models;

use App\Http\Traits\AdminTrait;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use AdminTrait;
    protected $fillable = ['name', 'description', 'lifetime'];


    public static function boot()
    {
        parent::boot();

        static::created(function (Subscription $subscription) {
            $request = request();

                $subscription->addTreatments($request->treatments, $subscription->id);
                $subscription->addPetTypes($request->pet_types, $subscription->id);

        });

    }
    public function subscriptionTreatments()
    {
        return $this->hasMany(SubscriptionTreatment::class);
    }

    public function subscriptionPetTypes()
    {
        return $this->hasMany(SubscriptionPetType::class);
    }
}
