<?php


namespace App\Helpers\DTO;


use App\Models\Commission;
use Illuminate\Support\Collection;

class ProviderAppointmentReport
{
    public $quantity;
    public $income;
    public $commission;
    public $netto;
    public $appointments;

    public function __construct(Collection $appointments, Commission $commission)
    {
        $this->quantity = $appointments->count();
        $this->income = $appointments->sum('price');
        $this->commission = $commission->calculateIncomeReportCommission($appointments);
        $this->netto = $this->income - $this->commission;
        $this->appointments = $appointments;
    }
}
