<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporarySchedule extends Model
{
    /******* Properties *******/
    protected $fillable =[
        'start_date',
        'end_date',
        'opening_time',
        'closing_time',
        'available',
        'business_item_id',
    ];

    protected $casts = [
        'available' => 'boolean',
    ];


    /******* Relations *******/
    public function business()
    {
        return $this->belongsTo(BusinessItem::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }

    /******* Static functions *******/

    /**
     * Add new specific schedule rules.
     *
     * @param array $data
     */
    public static function saveRange(array $data)
    {
        foreach ($data as $element) {
            $businessItemId = $element['business_item_id'];
            foreach ($element['schedules'] as $schedule) {
                $newSchedule = self::create($schedule + ['business_item_id' => $businessItemId]);

                if(isset($schedule['cities'])) {
                    $newSchedule->cities()->attach($schedule['cities']);
                }
            }
        }
    }

    /**
     * Delete specific schedule rules.
     *
     * @param array $data
     */
    public static function deleteRange(array $data)
    {
        foreach ($data as $id) {
            $schedule = TemporarySchedule::find($id);
            $schedule->cities()->detach();
            $schedule->delete();
        }
    }
}
