<?php

namespace App\Models;

use App\Http\Traits\PetLogic;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int client_id
 * @property string photo
 */
class Pet extends Model
{
    use PetLogic;

    protected $fillable = [
        'photo',
        'name',
        'date_of_birth',
        'weight',
        'gender',
        'castrated',
        'pet_type_id',
        'breed_id',
    ];

    protected $guarded = [
        'client_id'
    ];

    /**
     * Override base boot method.
     *
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::creating(function (Pet $pet) {
            $pet->client_id = auth()->user()->client->id;
//            $pet->addPhoto(request()->photo);
        });

        static::deleting(function (Pet $pet) {
            $path = config('constants.image_folder.pets_photo.save_path');
            $pet->deletePhoto64($pet->getOriginal('photo'),$path);
        });
    }

    /******* Relations *******/
    public function petTypes()
    {
        return $this->belongsTo(PetType::class,'pet_type_id');
    }

    public function breeds()
    {
        return $this->belongsTo(Breed::class,'breed_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class,'client_id');
    }

    public function appointments()
    {
        return $this->belongsToMany(Appointment::class);
    }

    public function appointmentItems()
    {
        return $this->hasMany(AppointmentItem::class);
    }
    public function comment()
    {
        return $this->hasMany(AppointmentComment::class,'pet_id');
    }

    /******* Accessors *******/
    public function getPhotoAttribute($value)
    {
        $getPath = config('constants.image_folder.pets_photo.get_path');
        return url($getPath . $value);
    }

}
