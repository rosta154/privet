<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VetCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visit_price' => 'required|numeric',
            'min_visit_price' => 'required|numeric',
            'evening_visit_price' => 'required|numeric',
            'business_id' => 'required|exists:businesses,id',
            'treatment_id' => 'required|exists:treatments,id',
            'schedule' => 'required',
            'place' => 'string|max:255',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'radius' => 'numeric'
        ];
    }
}
