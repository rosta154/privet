<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Schedule;
use Faker\Generator as Faker;

$factory->define(Schedule::class, function (Faker $faker) {
    // TODO: incorrect time and unique days.
    $days = array_keys(Schedule::WEEK_DAYS);
    shuffle($days);
    return [
        'week_day' => array_pop($days),
        'opening_time' => $faker->time($format = 'H:i', $max = '10:00'),
        'closing_time' => $faker->time($format = 'H:i', $max = '19:00'),
        'rule' => $faker->boolean(),
        'available' => $faker->boolean()
    ];
});
