<?php


namespace App\Providers\Validators;


use Illuminate\Support\ServiceProvider;
use Intervention\Image\ImageManagerStatic;

class Base64ImageValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('is_base64_image', function ($attribute, $value, $parameters)
        {
            try {
                ImageManagerStatic::make($value);
                return true;
            } catch (\Exception $e) {
                return false;
            }
        });
    }

    public function register()
    {
        //
    }
}
