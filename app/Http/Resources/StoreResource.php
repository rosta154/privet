<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = $this->businessItems->first();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'address' => $this->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'license' => $this->license,
            'website' => $this->website,
            'about' => $this->about,
            'type_id' => $this->type_id,
            'rating' => $this->rating,
            'count_user' => $this->count_user,
            'schedule' => $item->schedule,
            'products' => $this->products,
            'distance' => $this->distance,
            'images' => $this->images,
            'newToken' => $this->newToken,
        ];
    }
}
