<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 1/7/20
 * Time: 2:10 PM
 */

namespace App\Http\Traits;


use App\Models\SubscriptionPetType;
use App\Models\SubscriptionTreatment;

trait AdminTrait
{
    public function addTreatments(array $treatments, int $subscriptionId)
    {
        foreach ($treatments as $element) {
            SubscriptionTreatment::create([
                'treatment_id' => $element,
                'subscription_id' => $subscriptionId
            ]);
        }
    }

    public function addPetTypes(array $petTypes, int $subscriptionId)
    {
        foreach ($petTypes as $element) {
            SubscriptionPetType::create([
                'pet_type_id' => $element,
                'subscription_id' => $subscriptionId
                ]);
        }
    }
}