<?php

namespace App\Console\Commands;

use App\Helpers\Interfaces\INotifier;
use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClientTomorrowAppointmentReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:client-first';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder about tomorrow appointment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO possible time zone conflicts. carbon return Greenwich.
        $searchTime = Carbon::now()->addDays(1);
        $end = $searchTime->toDateTime();
        $start = $searchTime->addMinutes(-59);

        $appointments = Appointment::where('status', Appointment::APPROVED_STATUS)
            ->whereBetween('date', [$start, $end])
            ->with('business', 'client.user')
            ->get();

        if($appointments != null) {
            $notifier = resolve(INotifier::class);
            foreach ($appointments as $appointment) {
                $notifier->customerDailyMeetingReminder($appointment);
            }
        }
    }
}
