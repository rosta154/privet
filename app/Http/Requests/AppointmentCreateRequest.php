<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AppointmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => '|numeric',
            'date' => '|date_format:Y-m-d H:i|after:' ,
            'comment' => 'string|max:255',
            'address' => 'string|max:255',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'business_id' => 'required|exists:businesses,id',
            'business_item_id' => '|current_business_item',
            'type_id' => 'required|is_mane_treatment',
            'treatments' => 'required|array',
                'treatments.*.pet_id' => 'required|current_user_pet',
                'treatments.*.services' => 'required|array',
                    'treatments.*.services.*.id' => 'required|current_business_service',
                    'treatments.*.services.*.quantity' => 'required|numeric',
        ];
    }
}
