<?php

use App\Models\Pet;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('photo');
            $table->string('name');
            $table->date('date_of_birth');
            $table->string('weight');
            $table->string('gender');
            $table->boolean('castrated');

            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('pet_type_id');
            $table->unsignedBigInteger('breed_id');

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('pet_type_id')->references('id')->on('pet_types');
            $table->foreign('breed_id')->references('id')->on('breeds');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
