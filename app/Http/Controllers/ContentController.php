<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContentRequest;
use App\Http\Requests\ContentUpdateRequest;
use App\Http\Traits\PhotoTrait;
use App\Models\ContactUs;
use App\Models\Content;
use App\Models\User;
use Illuminate\Http\Request;
use App\Helpers\Interfaces\IFileManager;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
class ContentController extends Controller
{
use PhotoTrait;
    protected $filemanager;

    public function __construct(IFileManager $filemanager)
    {
        $this->filemanager = $filemanager;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->ok(Content::all());
    }

    /**
     * @param Content $content
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Content $content)
    {
        return $this->ok($content);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ContentRequest $request)
    {
        $content = new Content($request->all());
        $path =config('constants.image_folder.content_photo.save_path');
        $content->image =  $this->savePhoto($request->image, $path);
        $content->save();
        return $this->created($content);
    }
    public function update(ContentUpdateRequest $request, Content $content)
    {
        //TODO check if request has photo and delete if it present.
        if($request->has('image')) {
            $path =config('constants.image_folder.content_photo.save_path');
            if(!is_null($content->image)){
                $this->deletePhoto($content->getOriginal('image'), $path);
            }
            $content->image = $this->savePhoto($request['image'], $path);
            $content->update($request->except('image'));
        }
        $content->update($request->except('image'));
        return response()->json(['success' => true, 'data' => $content]);
    }

    public function destroy(Content $content)
    {
        return $this->deleted($content->delete());
    }

}
