<?php

namespace App\Http\Requests;

use App\Models\Invite;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class InviteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => Rule::unique('invites')->where(function ($query) {
                return $query->where('user_status', Invite::INVATED);
            })
        ];
    }
}
