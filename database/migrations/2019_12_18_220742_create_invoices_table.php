<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('price');
            $table->unsignedBigInteger('cash_back')->default(0);
            $table->unsignedBigInteger('appointment_id');
            $table->unsignedBigInteger('business_id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedInteger('status')->default(1);
            $table->text('payme_sale_url')->nullable();
            $table->text('payme_sale_id')->nullable();
            $table->text('payme_sale_code')->nullable();

            $table->timestamps();

            // TODO add foreign keys after test.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
