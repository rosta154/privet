<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'name',
        'latitude',
        'longitude',
    ];

    public function businessItems()
    {
        return $this->belongsToMany(BusinessItem::class);
    }

    public function temporarySchedules()
    {
        return $this->belongsToMany(TemporarySchedule::class);
    }

    public function schedules()
    {
        return $this->belongsToMany(Schedule::class);
    }
}
