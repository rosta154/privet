<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int amount
 * @property bool active
 */
class CashBack extends Model
{
    protected $fillable = [
        'amount',
        'appointment_id',
        'client_id',
        'end_date',
        'active',
        'description',
    ];


    public function appointment(){
        return $this->belongsTo(Appointment::class);
    }
    /**
     * Add client cash-back.
     *
     * @param Appointment $appointment
     */
    public static function add(Appointment $appointment)
    {
        $conditions = CashBackCondition::where('business_id', $appointment->business_id)
            ->orWhere('business_type_id', $appointment->business_type_id)
            ->get();

        if($conditions->count() != 0) {
            $condition = $conditions->where('business_id', $appointment->business_id)->first() ?? $conditions->first();

            self::create([
                'appointment_id' => $appointment->id,
                'client_id' => $appointment->client_id,
                'end_date' => Carbon::now()->addDays($condition->life_time),
                'description' => "Appointment #$appointment->id",
                'amount' => self::calculateAmount($condition, $appointment->price),
            ]);
        }
    }

    /**
     * Write off available balance.
     *
     * @param Invoice $invoice
     */
    public static function remove(Invoice $invoice)
    {
        $amount = $invoice->cash_back;
        if($amount != 0) {
            $clientCash = Client::find($invoice->client_id)->cashBack;
            $clientCash->each(function (CashBack $item) use (&$amount) {
                if($amount > 0) {
                    $amount -= $item->amount;
                    $item->active = false;
                    $item->save();
                }
            });
        }
    }

    /**
     * Calculate cash-back amount.
     *
     * @param CashBackCondition $condition
     * @param float $price
     * @return float|int
     */
    private static function calculateAmount(CashBackCondition $condition, float $price)
    {
        if($condition->type == CashBackCondition::PERCENT_TYPE) {
            return $price * $condition->amount / 100;
        }
        return $condition->amount;
    }
}
