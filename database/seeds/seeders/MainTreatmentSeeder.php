<?php

use App\Models\BusinessType;
use App\Models\Treatment;
use Illuminate\Database\Seeder;

class MainTreatmentSeeder extends Seeder
{
    public static $treatmentCollection = [
        ['id' => 1, 'name' => 'Home visit', 'main' => true, 'min_price' => 200, 'max_price' => 400, 'business_type_id' => BusinessType::VETERINARIAN],
        ['id' => 2, 'name' => 'Clinic visit', 'main' => true, 'min_price' => 100, 'max_price' => 300, 'business_type_id' => BusinessType::VETERINARIAN],
        ['id' => 3, 'name' => 'Day', 'main' => true, 'min_price' => 200, 'max_price' => 400, 'business_type_id' => BusinessType::HOTEL],
        ['id' => 4, 'name' => 'Night', 'main' => true, 'min_price' => 100, 'max_price' => 300, 'business_type_id' => BusinessType::HOTEL],
        ['id' => 5, 'name' => 'Day', 'main' => true, 'min_price' => 200, 'max_price' => 400, 'business_type_id' => BusinessType::TRAINER],
        ['id' => 6, 'name' => 'Night', 'main' => true, 'min_price' => 100, 'max_price' => 300, 'business_type_id' => BusinessType::TRAINER],
        ['id' => 7, 'name' => 'Day', 'main' => true, 'min_price' => 200, 'max_price' => 400, 'business_type_id' => BusinessType::WALKER],
        ['id' => 8, 'name' => 'Night', 'main' => true, 'min_price' => 100, 'max_price' => 300, 'business_type_id' => BusinessType::WALKER],
        ['id' => 9, 'name' => 'Barbershop', 'main' => true, 'min_price' => 200, 'max_price' => 400, 'business_type_id' => BusinessType::BARBERSHOP],
        ['id' => 10, 'name' => 'Portable Barbershop', 'main' => true, 'min_price' => 100, 'max_price' => 300, 'business_type_id' => BusinessType::BARBERSHOP],
        ['id' => 11, 'name' => 'Walker 30 min', 'main' => false, 'min_price' => 30, 'max_price' => 60, 'business_type_id' => BusinessType::WALKER],
        ['id' => 12, 'name' => 'Walker 45 min', 'main' => false, 'min_price' => 45, 'max_price' => 90, 'business_type_id' => BusinessType::WALKER],
        ['id' => 13, 'name' => 'Walker 60 min', 'main' => false, 'min_price' => 60, 'max_price' => 120, 'business_type_id' => BusinessType::WALKER],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::$treatmentCollection as $treatment) {
            Treatment::create($treatment);
        }
    }
}
