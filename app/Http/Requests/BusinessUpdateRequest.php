<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusinessUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place' => 'string|max:255',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'license' => 'string|max:255',
            'website' => 'url|max:255',
            'main_image' => '',
            'images' => 'array|max:3',

        ];
    }
}
