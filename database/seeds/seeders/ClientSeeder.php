<?php

use Illuminate\Database\Seeder;
use App\Models\Client;
use Illuminate\Support\Facades\Event;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::fake();
        factory(Client::class, DatabaseSeeder::CLIENTS_COUNTS)->create();
    }
}
