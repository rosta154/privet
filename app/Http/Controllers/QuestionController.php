<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->ok(Question::all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionRequest $request)
    {
        $question = new Question($request->all());
      if($question->pluck('percent')->sum() + $request->percent > 100){
          return response()->json(['success' => false,'message' => 'the percentage of all questions is more than 100']);
      };
      if($question->count()+1 > 3){
          return response()->json(['success' => false,'message' => 'questions can not be more than 3']);
      }
        $question->save();
        return $this->created($question);
    }

    /**
     * @param Request $request
     * @param Question $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(QuestionRequest $request, Question $question)
    {
        $question->update($request->all());
        return $this->updated($question);
    }

    /**
     * @param Question $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Question $question)
    {
        return $this->ok($question);
    }


    /**
     * @param Question $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Question $question)
    {
        return $this->deleted($question->delete());
    }


}
