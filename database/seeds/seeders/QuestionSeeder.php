<?php

use Illuminate\Database\Seeder;
use App\Models\Question;
class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $question = new Question();
        $question->name = 'question 1';
        $question->save();

        $question = new Question();
        $question->name = 'question 2';
        $question->save();

        $question = new Question();
        $question->name = 'question 3';
        $question->save();
    }
}
