<?php

namespace App\Providers;

use App\Helpers\CollectionUpdater;
use App\Helpers\Interfaces\ICollectionUpdater;
use App\Helpers\Interfaces\INotifier;
use App\Helpers\FirebaseNotifier;
use App\Helpers\Interfaces\IScheduleHelper;
use App\Helpers\ScheduleHelper;
use App\Helpers\TwilioSmsClient;
use App\Helpers\LocalFileManager;
use App\Helpers\Interfaces\ISmsService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Helpers\Interfaces\IFileManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // IoC Containers
        $this->app->bind(ISmsService::class, TwilioSmsClient::class);
        $this->app->bind(IFileManager::class, LocalFileManager::class);
        $this->app->bind(INotifier::class, FirebaseNotifier::class);
        $this->app->bind(ICollectionUpdater::class, CollectionUpdater::class);
        $this->app->bind(IScheduleHelper::class, ScheduleHelper::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(177);
    }
}
