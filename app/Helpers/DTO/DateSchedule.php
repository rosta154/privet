<?php


namespace App\Helpers\DTO;


use Illuminate\Support\Collection;

class DateSchedule
{
    public $date;
    public $schedule;
    public $appointments;
    public $openingTime;
    public $closingTime;

    public function __construct(string $date, array $schedule, Collection $appointments)
    {
        $this->date = $date;
        $this->schedule = collect($schedule)->sortBy('rule')->sortBy('updated_at')->values();
        $this->appointments = $appointments->values();
    }
}
