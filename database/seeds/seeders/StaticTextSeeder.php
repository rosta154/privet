<?php

use Illuminate\Database\Seeder;
use App\Models\StaticText;
class StaticTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $staticText = new StaticText();
         $staticText->title = 'Term of Use';
         $staticText->field_1 = 'Reef brutal forehand hook oil water fat hallow grom? Rank wax the stick air drop, heavy section skegs hack a thon don. Rip the pit priority bottomed out nose kook, hang 5 whippin da yams. Success wedge factory glass, too deep big fish tube, making it rain fired up king of the peak. Wave of the winter smooth lineup lil ripper duck diving. Rocker clamped kook wave double up. Rip tide backside attack backhand attack frothy top wax the stick skegs ripper cracking it Bells poor.';
         $staticText->save();


        $staticText = new StaticText();
        $staticText->title = 'About Us';
        $staticText->field_1 = 'Line-up firing dry reef pocket pearl green room. Making the section marine layer fin quiver air reverse pose on the nose claw hands outer bar snap clean. Tube power gouge J.O.B. dredging set rippin glazz firing shorebreak! Shredded bunny chow transition deadly, bottomed out. John John Florence epic sandbar a-frame fired up. Shacked Mick Fanning clean face Dane Reynolds pumping gnarley. Fun board down the line nose pick digs rail whitewash jumbled spitting barrels.';
        $staticText->save();

        $staticText = new StaticText();
        $staticText->title = 'About Us';
        $staticText->field_1 = 'Pig dog tagging layback daggers Snapper Rocks so pitted slob. Fog socked in stick wax firing shorebreak back dooring. Forehand hook aerial fog pocket, soft lip section no kooks. Fan a spray WQS lay day stab mag outside spitting barrels air reverse. Crumbly lip DOH, surf bottom turn clamped';
        $staticText->save();


    }
}
