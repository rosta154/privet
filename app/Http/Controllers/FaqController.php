<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['success' => true, 'data' => Faq::all()]);
    }

    /**
     * @param Faq $faq
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Faq $faq)
    {
        return response()->json(['success' => true,'data' => $faq]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $faq = new Faq($request->all());
        return response()->json(['success' => $faq->save(),'data' => $faq]);
    }

    /**
     * @param Faq $faq
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( Faq $faq,Request $request)
    {
        return response()->json(['success' => $faq->update($request->all()),'data' => $faq]);
    }

    /**
     * @param Faq $faq
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Faq $faq)
    {
        return response()->json(['success' => $faq->delete(),'message' => 'FAQ has been deleted']);
    }
}
