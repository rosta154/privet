<?php

namespace App\Http\Controllers;

use App\Helpers\Interfaces\ICollectionUpdater;
use App\Http\Requests\BusinessCalendarRequest;
use App\Http\Requests\BusinessProviderCalendarRequest;
use App\Http\Requests\BusinessSearchByNameRequest;
use App\Http\Requests\BusinessSearchByTreatmentRequest;
use App\Http\Requests\BusinessUpdateServicesRequest;
use App\Http\Requests\IndexStoreRequest;
use App\Http\Traits\BusinessSearch;
use App\Http\Traits\DistanceCalculator;
use App\Models\Business;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use App\Models\BusinessImage;
use App\Helpers\Interfaces\IFileManager;
use App\Http\Requests\BusinessCreateRequest;
use App\Http\Requests\BusinessUpdateRequest;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class BusinessController extends Controller
{
    use SoftDeletes, DistanceCalculator, BusinessSearch;

    protected $fileManager;
    protected $count_user = 3;

    public function __construct(IFileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    /**
     * Get all businesses from db.
     *
     * @param IndexStoreRequest $request
     * @return JsonResponse
     */
    public function index(IndexStoreRequest $request)
    {
        $businesses = Business::with('images')
            ->SearchByName()
//            ->stores()
            ->get();

        return $this->ok($businesses);
    }

    /**
     * Get business info.
     *
     * @param Business $business
     * @return JsonResponse
     */
    public function show(Business $business)
    {
        //TODO move to model
        $types = Str::plural(strtolower($business->type->name));
        $business->load([
            $types,
            "$types.treatment",
            'images',
            'businessItems',
            'businessItems.schedule.cities',
            'businessItems.temporarySchedule.cities',
            'businessItems.cities',
        ]);
        $business->services = $business->services();
        //$business->cities();
        return $this->ok($business);
    }

    /**
     * Get calendar for customer.
     *
     * @param BusinessCalendarRequest $request
     * @param Business $business
     * @return JsonResponse
     */
    public function getCustomerCalendar(BusinessCalendarRequest $request, Business $business)
    {
        $result = $business->customerCalendar($request->all());
        return $this->ok($result);
    }

    /**
     * Get calendar for provider.
     *
     * @param BusinessProviderCalendarRequest $request
     * @param Business $business
     * @return JsonResponse
     */
    public function getProviderCalendar(BusinessProviderCalendarRequest $request, Business $business)
    {
        $result = $business->providerCalendar($request->all());
        return $this->ok($result);
    }

    /**
     * Get calendar for customer.
     *
     * @param BusinessCalendarRequest $request
     * @param Business $business
     * @return JsonResponse
     */
    public function getCalendar(BusinessCalendarRequest $request, Business $business)
    {
        $result = $business->calendar($request->all());
        return $this->ok($result);
    }

    /**
     * Delete business from db.
     *
     * @param Business $business
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Business $business)
    {
        return $this->deleted($business->delete());
    }

    /**
     * Add new business.
     *
     * @param BusinessCreateRequest $request
     * @return JsonResponse
     */
    public function store(BusinessCreateRequest $request)
    {
        $business = Business::create($request->all());
        $types = Str::plural(strtolower($business->type->name));
        $business->load([$types, 'images', 'businessItems', 'businessItems.schedule']);
        $business->services = $business->services();
        $business->newToken = auth()->login(auth()->user());
        return $this->created($business);
    }

    /**
     * Update business info.
     *
     * @param BusinessUpdateRequest $request
     * @param Business $business
     * @return JsonResponse
     */
    public function update(BusinessUpdateRequest $request, Business $business)
    {
        $fileManager = resolve(IFileManager::class);
        $business->saveImages($request->all(), $fileManager);
        $business->update($request->all());
        return $this->updated($business->load('mainImage', 'images'));
    }

    /**
     * Update business items.
     *
     * @param BusinessUpdateServicesRequest $request
     * @param Business $business
     * @return JsonResponse
     */
    public function updateServices(BusinessUpdateServicesRequest $request, Business $business)
    {
        $updater = resolve(ICollectionUpdater::class);
        $updater->updateServices(collect($request->services), $business);
        return $this->ok();
    }

    /**
     * Remove business image from db and storage.
     *
     * @param BusinessImage $image
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroyImage(BusinessImage $image)
    {
        $path = config('constants.image_folder.business_photo.save_path');
        $this->fileManager->deleteFile($image->getOriginal('name'), $path);
        return $this->deleted($image->delete());
    }

    /**
     * Get business's treatments.
     *
     * @param Business $business
     * @return JsonResponse
     */
    public function businessTreatments(Business $business)
    {
        return $this->ok($business->treatments);
    }

    /**
     * Find businesses by name.
     *
     * @param BusinessSearchByNameRequest $request
     * @return JsonResponse
     */
    public function searchByName(BusinessSearchByNameRequest $request)
    {
        $businesses = Business::where('name', 'like', "%$request->name%")
            ->where('type_id', $request->type)
            ->with(['images', 'businessItems', 'businessItems.schedule'])
            ->get();

        return $this->ok($businesses);
    }

    /**
     * Get all businesses by treatment.
     *
     * @param BusinessSearchByTreatmentRequest $request
     * @return JsonResponse
     */
    public function searchByTreatment(BusinessSearchByTreatmentRequest $request)
    {
        // TODO: add schedule search options
        $type = $this->getType($request->business_type_id);

        $businesses = Business::with([
            $type,
            'mainImage',
            'businessItems',
            'businessItems.schedule',
            'businessItems.temporarySchedule',
            'businessItems.appointments',
            'businessItems.cities',
        ])->whereHas($type, function (Builder $query) use ($request) {
                $query->whereIn("treatment_id", $request->treatments);
            }, '=', count($request->treatments))
            ->get();

        if($businesses) {
            $result = $this->filterBusinesses($businesses, $request);
            return $this->ok($result);
        }
        return $this->ok();
    }
}
