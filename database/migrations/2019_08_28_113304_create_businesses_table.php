<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('address')->nullable();
            $table->double('latitude',10,6)->nullable();
            $table->double('longitude',10,6)->nullable();
            $table->string('license')->nullable();
            $table->string('website')->nullable();
            $table->string('canceled_policy')->nullable();
            $table->string('count_user')->default(0);
            $table->text('about');
            $table->float('rating')->default(0);
            $table->string('payme_id')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('type_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('business_types')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
