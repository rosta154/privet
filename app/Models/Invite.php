<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{

    public const INVATED = 1;
    public const CANCELED= 2;
    public const ACCEPTED = 3;

    protected $fillable = ['sender_id','recipient_id','phone','user_status'];

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
    public function recipient()
    {
        return $this->belongsTo(User::class,'recipient_id');
    }

}
