<?php


namespace App\Providers\Validators;


use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class IsMondayValidator extends ServiceProvider
{
    private const MONDAY = 1;

    public function boot()
    {
        $this->app['validator']->extend('monday', function ($attribute, $value, $parameters)
        {
            return Carbon::parse($value)->dayOfWeek == self::MONDAY;
        });
    }
}
