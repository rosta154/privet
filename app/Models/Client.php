<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 */
class Client extends Model
{
    /******* Properties *******/
    protected $fillable = [
        'status',
    ];

    protected $guarded = [
        'user_id'
    ];

    protected $hidden = [
        //Влад я тут убрал created_At, он мне нужен в админке был
        'updated_at',
    ];

    protected $appends = [
        'clientCashBack'
    ];

    /**
     * Override base boot method.
     *
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::creating(function (Client $client) {
            $client->user_id = User::getJWTUser()->id;
        });
    }

    /******* Relations *******/
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }

    public function cashBack()
    {
        return $this->hasMany(CashBack::class)
            ->whereDate('end_date', '>', Carbon::now())
            ->where('active', true);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    /******* Accessors *******/
    public function getClientCashBackAttribute()
    {
        return $this->cashBack()->sum('amount');
    }
}
