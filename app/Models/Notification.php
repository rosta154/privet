<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public const REMINDER = 1;

    protected $fillable = [
        'title',
        'body',
        'is_read',
        'type',
        'metadata',
        'user_id',
        'appointment_id',
        'recipient_business_id',
    ];

    protected $casts = [
        'metadata' => 'array'
    ];

    public function appointment()
    {
       return $this->belongsTo(Appointment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function createNew(string $title, string $body, int $userId, $businessId = null, $appointmentId = null, int $type = 1, $metadata = null)
    {
        self::create([
            'title' => $title,
            'body' => $body,
            'type' => $type,
            'metadata' => $metadata,
            'appointment_id' => $appointmentId,
            'user_id' => $userId,
            'recipient_business_id' => $businessId,
        ]);
    }
}
