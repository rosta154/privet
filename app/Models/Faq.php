<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    /******* Attributes *******/
    protected $fillable = [
        'title',
        'description'
    ];
}
