<?php

namespace App\Models;

use App\Helpers\Interfaces\INotifier;
use App\Http\Requests\AppointmentApproveRequest;
use App\Http\Requests\AppointmentNotifyAdminRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @property  int client_id
 * @property  int business_id
 * @property  int business_type_id
 * @property  int type_id
 * @property  int status
 * @property  int payment_status
 * @property  string note
 * @property  int business_item_id
 * @property  float price
 * @property  string address
 * @property  string latitude
 * @property  string longitude
 * @property  string end_date
 * @property  int id
 * @property Appointment history
 */
class Appointment extends Model
{
    public const PENDING_STATUS = 1;
    public const APPROVED_STATUS = 2;
    public const CANCELED_STATUS = 3;

    public const APPOINTMENT_CREATED = 0;
    public const PENDING_PAYMENT_STATUS = 1;
    public const PROVIDER_NOTIFIED_PAYMENT_STATUS = 2;
    public const ADMIN_NOTIFIED_PAYMENT_STATUS = 3;
    public const PAID_PAYMENT_STATUS = 4;

    /******* Properties *******/
    protected $fillable = [
        'price',
        'date',
        'end_date',
        'comment',
        'type_id',
        'business_id',
        'business_type_id',
        'rating',
        'disput',
        'business_item_id',
        'address',
        'cashback_info',
        'latitude',
        'longitude',
        'status',
        'payment_status'
    ];

    protected $guarded = [
        'client_id',
        'status',
        'payment_status',
        'note',
    ];

    protected $appends = [
        'type',
        'pets',
        'notifications'
    ];

    protected $casts = [
        'history' => 'array'
    ];

    /******* Relations *******/
    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function businessItem()
    {
        return $this->belongsTo(BusinessItem::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class);
    }

    public function items()
    {
        return $this->hasMany(AppointmentItem::class);
    }

    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }

    public function pets()
    {
        return $this->belongsToMany(Pet::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function history()
    {
        return $this->hasMany(HistoryAppointment::class, 'appointment_id');
    }

    public function comments()
    {
        return $this->hasMany(AppointmentComment::class, 'appointment_id');
    }

    public function decline_comments()
    {
        return $this->hasMany(DeclineComment::class, 'appointment_id');
    }

    public function cashbacks()
    {
        return $this->hasMany(CashBack::class);
    }

    /**
     * Override base boot method.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        $request = request();
        $business = Business::whereId($request->business_id)->first();

        static::creating(function (Appointment $appointment) use ($request, $business) {
            $appointment->addAddress($request, $appointment, $business);
            $appointment->business_id = $business->id;
            $appointment->business_type_id = $business->type_id;
            $appointment->type_id = $request->type_id;
            $appointment->business_item_id = $request->business_item_id;
            $appointment->client_id = User::getJWTUser()->client;
//            $appointment->cashback_info = 'min: '.$appointment->business->commission->min.', max:'.$appointment->business->commission->min;

        });


        static::created(function (Appointment $appointment) use ($request, $business) {

            $services = $business->services()->with('treatment')->get();
            foreach ($request->treatments as $treatment) {

                foreach ($treatment['services'] as $service) {
                    $currentService = $services->where('id', $service['id'])->first();
                    $appointmentItem = $appointment->addItem($currentService, $service['quantity'], $treatment['pet_id'], $appointment->id);
                    $currentService->items()->save($appointmentItem);
                    $appointmentItem->save();
                    $appointment->price += $appointmentItem->price;
                }
                $appointment->pets()->attach($treatment['pet_id']);

            }

            resolve(INotifier::class)->newAppointmentRequest($appointment);
        });

        static::updating(function (Appointment $appointment) {
            HistoryAppointment::create([
                'status' => $appointment->status,
                'payment_status' => $appointment->payment_status,
                'appointment_id' => $appointment->id,
            ]);
            //$appointment->updateStatusAndSendPush($appointment);
        });
    }

    /**
     * Update appointment status and send notification.
     *
     * @param Appointment $appointment
     */
    private function updateStatusAndSendPush(Appointment $appointment)
    {
        $user = auth()->user();

        if ($user && ($user->client == null || $appointment->client_id != $user->client->id)) {
            $notifier = resolve(INotifier::class);
            $notifier->appointmentUpdated($appointment);
        }
    }


    /**
     * Update all services in appointment.
     */
    public function updateServices()
    {
        $request = request();
        $oldItems = collect($this->items);
        $business = Business::whereId($this->business_id)->first();
        $services = $business->services()->with('treatment')->get();
        $updated = array();
        $this->price = 0;
        foreach ($request->treatments as $treatment) {
            $petId = $treatment['pet_id'];

            foreach ($treatment['services'] as $element) {
                $serviceId = $element['id'];
                $quantity = $element['quantity'];

                $service = $oldItems->where('service_id', $serviceId)->where('pet_id', $petId)->first();

                if ($service) {
                    $service->update(['quantity' => $element['quantity']]);
                    $updated[] = $service->id;
                    $this->price += $service->price * $element['quantity'];
                } else {
                    $currentService = $services->where('id', $serviceId)->first();
                    $appointmentItem = $this->addItem($currentService, $quantity, $petId, $this->id);
                    $currentService->items()->save($appointmentItem);
                    $appointmentItem->save();
                    $this->price += $appointmentItem->price;
                }
            }
        }

        $this->pets()->sync(collect($request->treatments)->pluck('pet_id'), true);
        $this->save();
        AppointmentItem::destroy($oldItems->whereNotIn('id', $updated)->pluck('id')->toArray());
    }

    /**
     * Get appointment item price.
     *
     * @param $service
     * @return int
     */
    public function calculatePrice($service)
    {
        //TODO add logic by technical task
        //return $service->visit_price;
        return 100;
    }

    /******* Accessors *******/
    public function getTypeAttribute()
    {
        return $this->business()->first()->services()->where('treatment_id', $this->type_id)->first();
    }

    public function getNotificationsAttribute()
    {
        return $this->notifications()->where('title', '=', 'Payment declined')->orderBy('id', 'desc')->get();
    }

    public function getPetsAttribute()
    {
        return $this->pets()
            ->with(array('appointmentItems' => function ($query) {
                $query->where('appointment_id', $this->id);
            }))
            ->get();
    }

    /******* SCOPES *******/
    public function scopePayed($query)
    {
        $request = request();
        if (isset($request->paid)) {
            if ($request->paid) {
                $query->where('payment_status', self::PAID_PAYMENT_STATUS);
            } else {
                $query->where('payment_status', '!=', self::PAID_PAYMENT_STATUS);
            }
        }
        return $query;
    }

    public function scopeApproved($query)
    {
        $request = request();
        if (isset($request->approved)) {
            if ($request->approved) {
                $query->where('status', self::APPROVED_STATUS);
            } else {
                $query->where('status', '!=', self::APPROVED_STATUS);
            }
        }
        return $query;
    }

    public function scopeTakeCount($query)
    {
        $request = request();
        if ($request->take) {
            $query->orderBy('date', 'asc')->take($request->take);
        }
        return $query;
    }

    public function scopeGetByBusinessType($query)
    {
        $request = request();
        if ($request->businessType) {
            $query->where('business_type_id', $request->businessType);
        }
        return $query;
    }

    public function scopeGetByType($query)
    {
        $request = request();
        if ($request->type) {
            $query->where('type_id', $request->type);
        }
        return $query;
    }

    public function scopeGetByStatus($query)
    {
        $request = request();
        if (isset($request->payment_status)) {
            $query->where('payment_status', $request->payment_status);
        }
        return $query;
    }

    /**
     * Change approved and pay status.
     *
     * @param AppointmentApproveRequest $request
     */
    public function changeStatus(AppointmentApproveRequest $request)
    {
        $notifier = resolve(INotifier::class);
        $this
            ->setApprovedStatus($request, $notifier)
            ->setReadyStatus($request->ready, $notifier)
            ->save();
    }

    /**
     * Change approved status.
     *
     * @param Request $request
     * @param INotifier $notifier
     * @return $this
     */
    private function setApprovedStatus(Request $request, INotifier $notifier)
    {
        if (isset($request->approved)) {
            switch ($request->approved) {
                case true:
                    $this->status = self::APPROVED_STATUS;
                    $this->note = $request->note;
                    $this->end_date = $request->endDate;
                    $notifier->confirmedAppointment($this);
                    $this->addComments($request->message);
                    break;

                case false:
                    $this->status = self::CANCELED_STATUS;
                    $notifier->declinedAppointment($this);
                    $this->declineComment($request->declineMessage);
                    break;
            }
        }

        return $this;

    }

    /**
     * Set ready to pay status.
     *
     * @param $ready
     * @param INotifier $notifier
     * @return $this
     */
    private function setReadyStatus($ready, INotifier $notifier)
    {
        if ($ready) {
            $this->payment_status = self::PENDING_PAYMENT_STATUS;
            $notifier->finishedAppointment($this);
        }
        return $this;
    }

    /**
     * Add comments to appointment.
     *
     * @param $comments
     */
    private function addComments($comments)
    {
        if ($comments != null && count($comments) > 0) {
            $this->comments()->delete();
            foreach ($comments as $value) {
                AppointmentComment::create([
                    'appointment_id' => $this->id,
                    'pet_id' => $value['pet_id'],
                    'comment' => $value['comment'],
                ]);
            }
        }
    }

    private function declineComment($comments)
    {
        if ($comments != null && count($comments) > 0) {
            $this->decline_comments()->delete();
            foreach ($comments as $value) {
                DeclineComment::create([
                    'appointment_id' => $this->id,
                    'comment' => $value['comment'],
                ]);
            }
        }
    }

    /**
     * Change appointment status and notify admin.
     *
     * @param AppointmentNotifyAdminRequest $request
     */
    public function notifyAdmin(AppointmentNotifyAdminRequest $request)
    {
        $this->payment_status = self::ADMIN_NOTIFIED_PAYMENT_STATUS;
        $this->save();
        // TODO notification for admin
    }

    /**
     * Create new Appointment item instance.
     *
     * @param $service
     * @param int $quantity
     * @param int $petId
     * @param int $appointmentId
     * @return AppointmentItem
     */
    private function addItem($service, int $quantity, int $petId, int $appointmentId): AppointmentItem
    {
        $appointmentItem = new AppointmentItem();
        $appointmentItem->name = $service['treatment']['name'];
        $appointmentItem->appointment_id = $appointmentId;
        $appointmentItem->pet_id = $petId;
        $appointmentItem->price = $service['price'] * $quantity;
        $appointmentItem->treatment_id = $service['treatment']['id'];
        $appointmentItem->quantity = $quantity;

        return $appointmentItem;
    }

    /**
     * Add appointment coordinates.
     *
     * @param Request $request
     * @param Appointment $appointment
     * @param Business $business
     */
    private function addAddress(Request $request, Appointment $appointment, Business $business)
    {
        if (!$request->address || !$request->latitude || !$request->longitude) {
            //$item = BusinessItem::find($request->business_item_id);
            //if (!$item->main) {
//                $appointment->address = $item->address;
//                $appointment->latitude = $item->latitude;
//                $appointment->longitude = $item->longitude;
//            } else {
            $appointment->address = $business->address;
            $appointment->latitude = $business->latitude;
            $appointment->longitude = $business->longitude;
            return;
//            }
        }
        $appointment->address = $request->address;
        $appointment->latitude = $request->latitude;
        $appointment->longitude = $request->longitude;
    }
}
