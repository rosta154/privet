<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Barbershop;
use Faker\Generator as Faker;

$factory->define(Barbershop::class, function (Faker $faker) {
    return [
        'business_id' => rand(1, DatabaseSeeder::BUSINESSES_COUNT),
        'treatment_id' => rand(1,DatabaseSeeder::TREATMENT_COUNTS),
        'price' => rand(0,500),
    ];
});
