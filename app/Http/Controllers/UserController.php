<?php

namespace App\Http\Controllers;

use App\Http\Requests\FollowerAddRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Client;
use App\Models\Follower;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class UserController extends Controller
{
    /**
     * Get current user information.
     *
     * @return JsonResponse
     */
    public function profile()
    {
        $user = auth()->user();
        return $this->ok($user->load('businesses', 'client'));
    }

    /**
     * Update user info.
     *
     * @param UserUpdateRequest $request
     * @return JsonResponse
     */
    public function update(UserUpdateRequest $request)
    {
        $user = auth()->user();
        $user->update($request->all());
        $client = Client::where('user_id', $user->id)->first();
        if ($request->is_client == 1 && !$client) {
            Client::create();
        }
        $user->newToken = auth()->login(auth()->user());
        return $this->ok($user);
    }

    /**
     * Login for admin.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function adminLogin(Request $request)
    {
        //TODO check login
        // шо за нах?
        $credentials = $request->only('email', 'password');
        $check = User::where('email', $credentials['email'])->where('admin', 1)
            ->first();
        if (!$check) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.notExist')]);
        }

        if (!Hash::check($credentials['password'], $check->password)) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.invalidPassword')]);
        }

        $user = User::getByEmail($credentials['email']);
        $token = null;
        try {
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json(['success' => false, 'message' => trans('messages.errors.cantFindAccount')], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.failedLogin')], 500);
        }
        return response()->json(['success' => $user->load('permission'), 'token' => $token]);
    }

    /**
     * Add new follower and sent notification.
     *
     * @param FollowerAddRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function addFollower(FollowerAddRequest $request)
    {
        Follower::findOrAdd($request->phone);
        return $this->ok();
    }
}
