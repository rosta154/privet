<?php

namespace App\Helpers\Interfaces;


interface IScheduleHelper
{
    public function generateTimeSchedule(array $businessItems, string $startDate, string $endDate): array;
    //public function periodSchedule(array $businessItems, string $startDate, string $endDate);
}
