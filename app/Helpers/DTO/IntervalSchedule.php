<?php


namespace App\Helpers\DTO;


class IntervalSchedule
{
    public $date;
    public $time;
    public $available;
    public $appointments;

    /**
     * IntervalSchedule constructor.
     * @param string $date
     * @param string $time
     * @param bool $available
     * @param array $appointments
     */
    public function __construct(string $date, string $time, bool $available, array $appointments)
    {
        $this->date = $date;
        $this->time = $time;
        $this->available = $available;
        $this->appointments = $appointments;
    }
}
