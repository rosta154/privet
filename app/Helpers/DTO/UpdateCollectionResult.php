<?php


namespace App\Helpers\DTO;


use Illuminate\Support\Collection;

class UpdateCollectionResult
{
    public $deleted;
    public $created;
    public $updated;

    public function __construct(Collection $deleted, Collection $created, Collection $updated)
    {
        $this->deleted = $deleted;
        $this->created = $created;
        $this->updated = $updated;
    }
}
