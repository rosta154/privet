<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessImage extends Model
{
    /******* Attributes *******/
    protected $fillable = [
        'photo',
        'main',
        'business_id'
    ];

    protected $casts = [
        'main' => 'boolean',
    ];

    /******* Accessors *******/
    public function getPhotoAttribute($value)
    {
        return url(config('constants.image_folder.business_photo.get_path') . $value);
    }
}
