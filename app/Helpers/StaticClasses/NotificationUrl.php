<?php


namespace App\Helpers\StaticClasses;


class NotificationUrl
{
    public const NEW_APPOINTMENT = "/main/new-appointment/";
    public const CLIENT_APPOINTMENT = "/main/client-appointment/";
    public const APPOINTMENT_COMPLETED = "/past-appointment/";
    public const APPOINTMENT_PENDING_PAYMENT = "/main/pending-pay-client-appointment/";
    public const APPOINTMENT_DECLINED_PAYMENT = "/pending-pay-appointment/";

    public const PROVIDER_CALENDAR = "/calendar-for-provider";

    public const START_WALK = "/g-map";
    public const END_WALK = "/main/client-appointment/";
}
