<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /******* Attributes *******/
    protected $fillable = [
        'price',
        'business_id',
        'treatment_id',
    ];


    /******* Relations *******/
    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }

    public function businesses()
    {
        return $this->belongsTo(Business::class,'business_id');
    }

    public function items()
    {
        return $this->morphMany(AppointmentItem::class, 'service');
    }
}
