<?php


namespace App\Providers\Validators;


use App\Models\Treatment;
use Illuminate\Support\ServiceProvider;

class ServiceCorrectPriceValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('correct_price', function ($attribute, $value, $parameters)
        {
            $treatment = Treatment::find($value['treatment_id']);
            $currentPrice = $value['price'];
            return $treatment != null && $currentPrice >= $treatment->min_price && $currentPrice <= $treatment->max_price;
        });
    }

    public function register()
    {
        //
    }
}
