<?php

namespace App\Models;

use App\Helpers\DTO\PriceRange;
use App\Helpers\Interfaces\IFileManager;
use App\Http\Traits\ServiceType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property int min_price
 * @property int max_price
 * @property boolean main
 * @property int business_type_id
 * @property int pet_type_id
 * @property int id
 */
class Treatment extends Model
{
    use ServiceType;

    public const VET_HOME_TREATMENT_ID = 1;
    public const VET_CLINIC_TREATMENT_ID = 2;
    public const HOTEL_DAY_TREATMENT_ID = 3;
    public const HOTEL_NIGHT_TREATMENT_ID = 4;
    public const TRAINER_DAY_TREATMENT_ID = 5;
    public const TRAINER_NIGHT_TREATMENT_ID = 6;
    public const WALKER_DAY_TREATMENT_ID = 7;
    public const WALKER_NIGHT_TREATMENT_ID = 8;
    public const BARBERSHOP_TREATMENT_ID = 9;
    public const PORTABLE_BARBERSHOP_TREATMENT_ID = 10;

    protected $fillable = [
        'name',
        'min_price',
        'max_price',
        'business_type_id',
        'pet_type_id',
    ];

    protected $guarded = [
        'main'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function (Treatment $treatment) {
            $request = request();
            if($request->info) {
                $fileManager = resolve(IFileManager::class);
                $path = config('constants.image_folder.treatment_photo.save_path');
                $filename = $fileManager->saveBase64($request->info['photo'], $path);
                TreatmentAdditionalInfo::create([
                    'description' => $request->info['description'],
                    'image' => $filename,
                    'treatment_id' => $treatment->id,
                ]);
            }
        });

        static::updated(function (Treatment $treatment) {
            $request = request();
            if($request->info) {
                $info = TreatmentAdditionalInfo::Where('treatment_id', $treatment->id)->first();
                $fileManager = resolve(IFileManager::class);
                $path = config('constants.image_folder.treatment_photo.save_path');

                if($info) {
                    if($request->info['photo']) {
                        $oldImage = $info->getOriginal('image');
                        $fileManager->deleteFile($oldImage, $path);
                        $newPhoto = $fileManager->saveBase64($request->info['photo'], $path);
                    } else {
                        $newPhoto = $info->image;
                    }
                    $info->update([
                        'description' => $request->info['description'],
                        'image' => $newPhoto,
                    ]);
                } else {
                    $filename = $fileManager->saveBase64($request->info->photo, $path);
                    TreatmentAdditionalInfo::create([
                        'description' => $request->info->description,
                        'image' => $filename,
                        'treatment_id' => $treatment->id,
                    ]);
                }
            }
        });
    }

    /******* Relations *******/
    public function type()
    {
        return $this->belongsTo(BusinessType::class,'business_type_id');
    }

    public function vets()
    {
        return $this->hasMany(Vet::class);
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class);
    }

    public function barbershops()
    {
        return $this->hasMany(Barbershop::class);
    }

    public function trainers()
    {
        return $this->hasMany(Trainer::class);
    }

    public function walkers()
    {
        return $this->hasMany(Walker::class);
    }

    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function appointmentsItem()
    {
        return $this->hasMany(AppointmentItem::class);
    }

    public function info()
    {
        return $this->hasOne(TreatmentAdditionalInfo::class);
    }

    public function subscriptionTreatments()
    {
        return $this->hasMany(SubscriptionTreatment::class);
    }

    /******* Scopes *******/
    public function scopeBusinessType($query)
    {
        $request = request();
        if($request->business_type) {
            $query->where('business_type_id', $request->business_type);
        }
        return $query->where([
            ['business_type_id', '!=', BusinessType::WALKER],
            ['business_type_id', '!=', BusinessType::STORE],
        ]);
    }

    public function scopePets($query)
    {
        $request = request();
        if($request->pet_type) {
            $query->where('pet_type_id', $request->pet_type)->orWhere('pet_type_id', null);
        }
        return $query;
    }

    public function scopeMain($query)
    {
        $request = request();
        return $query->where('main', $request->main);
    }

    public function scopeGetBase($query)
    {
        return $query->get(['id', 'name', 'business_type_id']);
    }

    public function scopeWhereName($query)
    {
        $request = request();
        if($request->name) {
            $query->where('name', 'like', '%' . $request->name . '%');
        }
        return $query;
    }

    /******* Methods *******/

    /**
     * Add price range to all treatments.
     *
     * @param Collection $treatments
     * @return Collection
     */
    public static function addPriceRange(Collection $treatments)
    {
        $treatments->map(function ($treatment, $key) {
            $query = self::getType($treatment['business_type_id'])
                ->where('treatment_id', $treatment['id'])
                ->getQuery();
            $minPrice = $query->min('price');
            $maxPrice = $query->max('price');
            $treatment->price_range = new PriceRange($minPrice, $maxPrice);
        });
        return $treatments;
    }

    /**
     * Check that type is main for providers.
     *
     * @param int $type
     * @return bool
     */
    public static function isMane(int $type)
    {
        return $type == self::VET_HOME_TREATMENT_ID ||
            $type == self::VET_CLINIC_TREATMENT_ID ||
            $type == self::WALKER_DAY_TREATMENT_ID ||
            $type == self::WALKER_NIGHT_TREATMENT_ID ||
            $type == self::BARBERSHOP_TREATMENT_ID ||
            $type == self::PORTABLE_BARBERSHOP_TREATMENT_ID ||
            $type == self::HOTEL_DAY_TREATMENT_ID ||
            $type == self::HOTEL_NIGHT_TREATMENT_ID ||
            $type == self::TRAINER_DAY_TREATMENT_ID ||
            $type == self::TRAINER_NIGHT_TREATMENT_ID;
    }

    /**
     * Get main treatments for current business.
     *
     * @param int $businessType
     * @return Collection
     */
    public static function mainTreatments(int $businessType) : Collection
    {
        switch ($businessType) {
            case BusinessType::VETERINARIAN:
                return new Collection([self::VET_CLINIC_TREATMENT_ID, self::VET_HOME_TREATMENT_ID]);
                break;
            case BusinessType::WALKER:
                return new Collection([self::WALKER_DAY_TREATMENT_ID, self::WALKER_NIGHT_TREATMENT_ID]);
                break;
            case BusinessType::BARBERSHOP:
                return new Collection([self::BARBERSHOP_TREATMENT_ID, self::PORTABLE_BARBERSHOP_TREATMENT_ID]);
                break;
            case BusinessType::HOTEL:
                return new Collection([self::HOTEL_DAY_TREATMENT_ID, self::HOTEL_NIGHT_TREATMENT_ID]);
                break;
            case BusinessType::TRAINER:
                return new Collection([self::TRAINER_DAY_TREATMENT_ID, self::TRAINER_NIGHT_TREATMENT_ID]);
                break;
            default :
                return new Collection();
        }
    }
}
