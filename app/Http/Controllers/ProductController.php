<?php

namespace App\Http\Controllers;

use App\Helpers\Interfaces\IFileManager;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Traits\PhotoTrait;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Intervention\Image\ImageManagerStatic;

class ProductController extends Controller
{
    use PhotoTrait;

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $product = Product::get();
        return response()->json(['success' => true, 'data' => $product->load('stores','categories')]);
    }

    /**
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product)
    {
        return response()->json(['success' => true,'data' => $product->load('stores','categories')]);
    }

    /**
     * @param Product $product
     * @return JsonResponse
     */
    public function destroy(Product $product)
    {
        return response()->json(['success' => $product->delete(),'message' => 'Product has been deleted']);
    }

    /**
     * Add products to store.
     *
     * @param ProductCreateRequest $request
     * @return JsonResponse
     */
    public function store(ProductCreateRequest $request)
    {
        $path = config('constants.image_folder.product_photo.save_path');
        $filemanager = resolve(IFileManager::class);
        foreach ($request->products as $item) {
           Product::addNew($item, $request->business_id, $path, $filemanager);
        }
        return $this->ok();
    }

    /**
     * Update store products collection.
     *
     * @param ProductUpdateRequest $request
     * @return JsonResponse
     */
    public function update(ProductUpdateRequest $request)
    {
        $path = config('constants.image_folder.product_photo.save_path');
        $filemanager = resolve(IFileManager::class);

        $updated = collect($request->products)->where('id', '!=', null)->pluck('id');
        $oldProducts = Product::where('business_id', $request->business_id)->pluck('id');
        Product::destroy($oldProducts->diff($updated));
        foreach ($request->products as $item) {
            if (isset($item['id'])) {
                $product = Product::find($item['id']);
                try {
                    ImageManagerStatic::make($item['image']);
                    $this->deletePhoto($product->getOriginal('image'), $path);
                    $product->image = $this->savePhoto($item['image'], $path);
                } catch (\Exception $e) {}
                $product->title = $item['title'];
                $product->save();
            } else {
                Product::addNew($item, $request->business_id, $path, $filemanager);
            }
        }
        return $this->updated();
    }
}
