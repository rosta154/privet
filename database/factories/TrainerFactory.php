<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Trainer;
use Faker\Generator as Faker;

$factory->define(Trainer::class, function (Faker $faker) {
    return [
        'price' => rand(50, 100),
        'non_working_price' => rand(100, 300),
    ];
});
