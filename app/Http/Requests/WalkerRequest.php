<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WalkerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "30_min_price" => 'required',
            "45_min_price" => 'required',
            "60_min_price" => 'required',
            'treatment_id' => 'required|exists:treatments,id'
        ];
    }
}
