<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionTreatment extends Model
{
    protected $fillable = ['subscription_id','treatment_id'];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }
}
