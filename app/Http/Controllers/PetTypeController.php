<?php

namespace App\Http\Controllers;

use App\Http\Requests\PetTypeRequest;
use App\Models\Breed;
use App\Models\PetType;
use Illuminate\Http\Request;

class PetTypeController extends Controller
{

    //TODO отключить возможности редактирования первых двух айди. Владиславу.
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        return response()->json(['success' => true, 'data' => PetType::whereNotIn('id',[1,2])->with('breeds')->get()]);
    }

    /**
     * @param PetType $petType
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(PetType $petType)
    {
        return response()->json(['success' => true, 'data' => $petType->load('breeds')]);
    }

    /**
     * @param PetTypeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PetTypeRequest $request)
    {
        $pet = new PetType($request->all());
        return response()->json(['success' => $pet->save(), 'data' => $pet]);
    }

    /**
     * @param PetType $petType
     * @param PetTypeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PetType $petType, PetTypeRequest $request)
    {
        return response()->json(['success' => $petType->update($request->all()),'data' => $petType]);
    }

    /**
     * @param PetType $petType
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(PetType $petType)
    {
        return response()->json(['success' => $petType->delete(),'message' => 'Pet Type has been deleted']);
    }

    public function breeds($petType)
    {
        $breeds = Breed::where('pet_type_id',$petType)->get();
        return $this->ok($breeds);
    }
}
