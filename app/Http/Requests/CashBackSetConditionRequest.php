<?php

namespace App\Http\Requests;

use App\Models\CashBackCondition;
use Illuminate\Foundation\Http\FormRequest;

class CashBackSetConditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = [CashBackCondition::FIXED_TYPE, CashBackCondition::PERCENT_TYPE];

        return [
            'business_type_id' => 'exists:business_types,id|required_without:business_id',
            'business_id' => 'exists:businesses,id|required_without:business_type_id',
            'type' => 'required|in:' .implode($types, ','),
            'amount' => 'required|numeric',
            'life_time' => 'numeric',
        ];
    }
}
