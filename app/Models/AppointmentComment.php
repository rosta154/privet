<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentComment extends Model
{
    protected $fillable = ['appointment_id','pet_id','comment'];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }
}
