<?php


namespace App\Helpers;


use App\Helpers\DTO\UpdateCollectionResult;
use App\Helpers\Interfaces\ICollectionUpdater;
use App\Models\Barbershop;
use App\Models\Business;
use App\Models\BusinessItem;
use App\Models\BusinessType;
use App\Models\Hotel;
use App\Models\Trainer;
use App\Models\Treatment;
use App\Models\Vet;
use Illuminate\Support\Collection;

class CollectionUpdater implements ICollectionUpdater
{
    public function updateSchedules(Collection $newSchedules, BusinessItem $item)
    {
        $oldSchedules = $item->schedules;
    }

    /**
     * Update business service collection.
     *
     * @param Collection $newServices
     * @param Business $business
     */
    public function updateServices(Collection $newServices, Business $business)
    {
        $oldServices = $business->services;
        $result = $this->getUpdatedResult($oldServices->pluck('treatment_id'), $newServices->pluck('treatment_id'));
        $this->updateByType($result, $newServices, $oldServices, $business);
        $this->updateBusinessItems($newServices, $business);
    }

    private function updateBusinessItems(Collection $newServices, Business $business)
    {
        $mainTreatmentIds = Treatment::mainTreatments($business->type_id);
        $main = $mainTreatmentIds->first();
        $notMain = $mainTreatmentIds->last();

        $mainTreatments = $newServices->whereIn('treatment_id', $mainTreatmentIds)->pluck('treatment_id');
        $items = $business->businessItems;

        if($mainTreatments->contains($main) && !$items->contains('main', true)) {
            BusinessItem::create([
                'business_id' => $business->id,
                'address' => $business->address,
                'latitude' => $business->latitude,
                'longitude' => $business->longitude,
                'main' => true,
            ]);
        }

        if($mainTreatments->contains($notMain) && !$items->contains('main', false)) {
            BusinessItem::create([
                'business_id' => $business->id,
                'address' => $business->address,
                'latitude' => $business->latitude,
                'longitude' => $business->longitude,
                'radius' => 1,
                'main' => false,
            ]);
        }

        if(!$mainTreatments->contains($main) && $items->contains('main', true)) {
            BusinessItem::destroy($items->where('main', true)->pluck('id'));
        }

        if(!$mainTreatments->contains($notMain) && $items->contains('main', false)) {
            BusinessItem::destroy($items->where('main', false)->pluck('id'));
        }
    }

    /**
     * Get collection of created/updated/deleted items.
     *
     * @param Collection $old
     * @param Collection $new
     * @return UpdateCollectionResult
     */
    private function getUpdatedResult(Collection $old, Collection $new)
    {
        $updated = $old->intersect($new);
        $deleted = $old->diff($new);
        $created = $new->diff($old);
        return new UpdateCollectionResult($deleted, $created, $updated);
    }

    /**
     * Update services by business type.
     *
     * @param UpdateCollectionResult $result
     * @param Collection $newServices
     * @param Collection $oldServices
     * @param Business $business
     */
    private function updateByType(UpdateCollectionResult $result, Collection $newServices, Collection $oldServices, Business $business)
    {
        switch ($business->type_id) {
            case BusinessType::VETERINARIAN;
                $this->updateVet($result, $newServices, $oldServices, $business);
                break;
            case BusinessType::TRAINER:
                $this->updateTrainer($result, $newServices, $oldServices, $business);
                break;
            case BusinessType::HOTEL:
                $this->updateHotel($result, $newServices, $oldServices, $business);
                break;
            case BusinessType::BARBERSHOP:
                $this->updateBarberShop($result, $newServices, $oldServices, $business);
                break;
        }
    }

    /**
     * Update vet services.
     *
     * @param UpdateCollectionResult $result
     * @param Collection $newServices
     * @param Collection $oldServices
     * @param Business $business
     */
    private function updateVet(UpdateCollectionResult $result, Collection $newServices, Collection $oldServices, Business $business)
    {
        Vet::destroy($oldServices->whereIn('treatment_id', $result->deleted)->pluck('id'));
        $newServices->whereIn('treatment_id', $result->created)->each(function ($item) use ($business) {
            Vet::create($item + ['business_id' => $business->id]);
        });
        $oldServices->whereIn('treatment_id', $result->updated)->each(function ($item) use ($newServices) {
            $item->update($newServices->where('treatment_id', $item['treatment_id'])->first());
        });
    }

    /**
     * Update trainer services.
     *
     * @param UpdateCollectionResult $result
     * @param Collection $newServices
     * @param Collection $oldServices
     * @param Business $business
     */
    private function updateTrainer(UpdateCollectionResult $result, Collection $newServices, Collection $oldServices, Business $business)
    {
        Trainer::destroy($oldServices->whereIn('treatment_id', $result->deleted)->pluck('id'));
        $newServices->whereIn('treatment_id', $result->created)->each(function ($item) use ($business) {
            Trainer::create($item + ['business_id' => $business->id]);
        });
        $oldServices->whereIn('treatment_id', $result->updated)->each(function ($item) use ($newServices) {
            $item->update($newServices->where('treatment_id', $item['treatment_id'])->first());
        });
    }

    /**
     * Update hotel services.
     *
     * @param UpdateCollectionResult $result
     * @param Collection $newServices
     * @param Collection $oldServices
     * @param Business $business
     */
    private function updateHotel(UpdateCollectionResult $result, Collection $newServices, Collection $oldServices, Business $business)
    {
        Hotel::destroy($oldServices->whereIn('treatment_id', $result->deleted)->pluck('id'));
        $newServices->whereIn('treatment_id', $result->created)->each(function ($item) use ($business) {
            Hotel::create($item + ['business_id' => $business->id]);
        });
        $oldServices->whereIn('treatment_id', $result->updated)->each(function ($item) use ($newServices) {
            $item->update($newServices->where('treatment_id', $item['treatment_id'])->first());
        });
    }

    /**
     * Update barbershop services.
     *
     * @param UpdateCollectionResult $result
     * @param Collection $newServices
     * @param Collection $oldServices
     * @param Business $business
     */
    private function updateBarbershop(UpdateCollectionResult $result, Collection $newServices, Collection $oldServices, Business $business)
    {
        Barbershop::destroy($oldServices->whereIn('treatment_id', $result->deleted)->pluck('id'));
        $newServices->whereIn('treatment_id', $result->created)->each(function ($item) use ($business) {
            Barbershop::create($item + ['business_id' => $business->id]);
        });
        $oldServices->whereIn('treatment_id', $result->updated)->each(function ($item) use ($newServices) {
            $item->update($newServices->where('treatment_id', $item['treatment_id'])->first());
        });
    }
}
