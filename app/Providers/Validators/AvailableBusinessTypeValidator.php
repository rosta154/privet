<?php


namespace App\Providers\Validators;


use App\Models\BusinessType;
use Illuminate\Support\ServiceProvider;

class AvailableBusinessTypeValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('available_business_type', function ($attribute, $value, $parameters)
        {
            return $value != BusinessType::STORE && $value <= 6;
        });
    }

    public function register()
    {
        //
    }
}
