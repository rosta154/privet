<?php

namespace App\Console\Commands;

use App\Helpers\Interfaces\INotifier;
use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NextWeekTreatment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:next-week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $searchTime = Carbon::now()->addDays(7);
        $start = $searchTime->copy()->startOfDay();
        $end = $searchTime->copy()->endOfDay();

        $appointments = Appointment::whereBetween('date', [$start, $end])
            ->with('client.user', 'items.pet')
            ->get();

        if($appointments != null) {
            $notifier = resolve(INotifier::class);
            foreach ($appointments as $appointment) {
                $firstTreatment = $appointment->items->first();
                $notifier->treatmentFirstReminder($firstTreatment->pet->name, $firstTreatment->name, $appointment->client->user);
            }
        }
    }
}
