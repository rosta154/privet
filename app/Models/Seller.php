<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = [
        'business_id',
        'seller_payme_id',
        'seller_payme_secret',
        'seller_dashboard_signup_link',
    ];
}
