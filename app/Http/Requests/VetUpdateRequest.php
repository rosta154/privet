<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VetUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visit_price' => 'numeric',
            'min_visit_price' => 'numeric',
            'evening_visit_price' => 'numeric',
            'business_id' => 'exists:businesses,id',
            'treatment_id' => 'exists:treatments,id',
            'place' => 'string|max:255',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'radius' => 'numeric'
        ];
    }
}
