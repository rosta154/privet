<?php


namespace App\Providers\Validators;


use App\Models\BusinessItem;
use Illuminate\Support\ServiceProvider;

class CorrectBusinessItemValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('current_business_item', function ($attribute, $value, $parameters)
        {
            $items = BusinessItem::where('business_id', request()->business_id)->pluck('id');
            return $items->contains($value);
        });
    }

    public function register()
    {
        //
    }
}
