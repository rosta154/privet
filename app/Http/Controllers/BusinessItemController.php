<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddTemporaryScheduleRequest;
use App\Http\Requests\BusinessItemRequest;
use App\Http\Requests\DeleteTemporaryScheduleRequest;
use App\Http\Traits\BusinessLogic;
use App\Http\Traits\PhotoTrait;
use App\Models\Business;
use App\Models\BusinessItem;
use App\Models\TemporarySchedule;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BusinessItemController extends Controller
{
    use BusinessLogic;
    use PhotoTrait;

    /**
     * Update Business Items with schedules.
     *
     * @param Business $business
     * @param BusinessItemRequest $request
     * @return JsonResponse
     */
    public function update(Business $business, BusinessItemRequest $request)
    {
        $business->load('businessItems', 'businessItems.schedule')->updateBusinessItems(collect($request->items));
        return $this->ok($business->businessItems);
    }

    /**
     * Add new specific schedule rules.
     *
     * @param AddTemporaryScheduleRequest $request
     * @return JsonResponse
     */
    public function addTemporarySchedule(AddTemporaryScheduleRequest $request)
    {
        TemporarySchedule::saveRange($request->temporary_schedules);
        return $this->ok();
    }

    /**
     * Delete specific schedule rules.
     *
     * @param DeleteTemporaryScheduleRequest $request
     * @return JsonResponse
     */
    public function deleteTemporarySchedule(DeleteTemporaryScheduleRequest $request)
    {
        TemporarySchedule::deleteRange($request->temporary_schedules);
        return $this->ok();
    }
}
