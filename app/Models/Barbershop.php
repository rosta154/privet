<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barbershop extends Model
{
    /******* Attributes *******/
    protected $fillable = [
        'business_id',
        'treatment_id',
        'price'
    ];

    /******* Relations *******/
    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }

    public function items()
    {
        return $this->morphMany(AppointmentItem::class, 'service');
    }
}
