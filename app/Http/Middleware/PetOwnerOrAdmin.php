<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;

class PetOwnerOrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $pet = $request->route('pet');
        $user = auth()->user();

        if ($pet->client_id == $user->client->id || $user->admin) {
            return $next($request);
        }
        else {
            abort(403, 'Access denied');
        }
    }
}
