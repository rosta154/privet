<?php

namespace App\Models;

use App\Helpers\Interfaces\IFileManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Product extends Model
{
    protected $fillable = [
      'title',
      'image',
      'business_id',
//      'category_id',
    ];

    public function categories()
    {
        return $this->belongsTo(CategoryProduct::class,'category_id');
    }

    public function stores()
    {
        return $this->belongsTo(Business::class);
    }

    public function getImageAttribute($value)
    {
        $getPath = Config::get('constants.image_folder.product_photo.get_path');
        return url($getPath . $value);
    }

    public static function addNew(array $item, int $businessId, string $path, IFileManager $filemanager)
    {
        self::create([
            'title' => $item['title'],
            'business_id' => $businessId,
            'image' => $filemanager->saveBase64($item['image'], $path)
        ]);
    }

}

