<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property int type
 * @property int amount
 */
class Commission extends Model
{
    public const FIXED_TYPE = 1;
    public const PERCENT_TYPE = 2;

    private const DEFAULT_COMMISSION = 1; // 1$ or 1%

    protected $fillable = [
        'business_type_id',
        'business_id',
        'type',
        'amount',
        'life_time',
    ];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function type()
    {
        return $this->belongsTo(BusinessType::class);
    }

    /**
     * Add business or business commissions.
     *
     * @param array $data
     */
    public static function add(array $data)
    {
        if(isset($data['business_type_id'])) {
            $condition = self::where('business_type_id', $data['business_type_id'])->first();
            if($condition) {
                $condition->update($data);
            } else {
                self::create($data);
            }
        }

        if(isset($data['business_id'])) {
            $condition = self::where('business_id', $data['business_id'])->first();
            if($condition != null) {
                $condition->update($data);
            } else {
                self::create($data);
            }
        }
    }

    public function calculateIncomeReportCommission(Collection $appointments)
    {
        if($this->type == self::PERCENT_TYPE) {
            return $appointments->sum('price') * $this->amount / 100;
        }
        return $appointments->count() * $this->amount;
    }
}
