<?php


namespace App\Helpers\Interfaces;


use App\Models\Business;
use Illuminate\Support\Collection;

interface ICollectionUpdater
{
    function updateServices(Collection $newServices, Business $business);
}
