<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PetType extends Model
{
    protected $fillable = [
      'name'
    ];

    public const PET_TYPE_DOGS = 1;
    public const PET_TYPE_CAT = 2;
    public const PET_TYPE_HORSE = 3;
    public const PET_TYPE_PARROT = 4;
    public const PET_TYPE_PIG = 5;

    public function pets()
    {
        return $this->hasOne('App\Models\Pet');
    }

    public function breeds()
    {
        return $this->hasMany('App\Models\Breed');
    }

    public function subscriptionPetTypes()
    {
        return $this->hasMany(SubscriptionPetType::class);
    }
}
