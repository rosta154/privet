<?php

namespace App\Http\Controllers;

use App\Helpers\Interfaces\INotifier;
use App\Helpers\PayMeClient;
use App\Http\Requests\InvoiceCreateRequest;
use App\Http\Requests\SellerCreateRequest;
use App\Models\Appointment;
use App\Models\Business;
use App\Models\CashBack;
use App\Models\Invoice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PayController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = new PayMeClient();
    }

    /**
     * Add new seller.
     *
     * @param SellerCreateRequest $request
     * @param Business $business
     * @return JsonResponse
     * @throws \Exception
     */
    public function addSeller(SellerCreateRequest $request, Business $business)
    {
        $seller = $this->client->addSeller($request->all(), $business);
        return $this->ok($seller);
    }

    /**
     * Generate new invoice and redirect to payment form.
     *
     * @param InvoiceCreateRequest $request
     * @param Appointment $appointment
     * @return RedirectResponse
     * @throws \Exception
     */
    public function generateInvoice(InvoiceCreateRequest $request, Appointment $appointment)
    {
        $result = $this->client->generateSale($appointment, $request->cash_back);
        return Redirect::to($result->payme_sale_url);
    }

    /**
     * Pay me call-back endpoint.
     *
     * @param Request $request
     * @param Invoice $invoice
     * @return JsonResponse
     */
    public function payResult(Request $request, Invoice $invoice)
    {
        if($request->payme_sale_id == $invoice->payme_sale_id && $request->payme_sale_status == "completed") {
            $invoice->status = Invoice::SUCCESS_STATUS;
            $invoice->save();

            $appointment = Appointment::find($invoice->appointment_id);
            $appointment->payment_status = Appointment::PAID_PAYMENT_STATUS;
            $appointment->save();

            $notifier = resolve(INotifier::class);
            $notifier->appointmentPaid($invoice->appointment_id);
            $notifier->appointmentCompleted($invoice->appointment_id);

            CashBack::remove($invoice);
            CashBack::add($appointment);
        } else {
            $invoice->status = Invoice::FAILED_STATUS;
            $invoice->save();
        }
        return $this->ok();
    }
}
