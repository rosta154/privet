<?php

use Illuminate\Database\Seeder;
use App\Models\Faq;
class FAQSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faq = new Faq();
        $faq->title = 'title';
        $faq->description = 'description';
        $faq->save();

    }
}
