<?php


namespace App\Providers\Validators;


use App\Http\Traits\ServiceType;
use App\Models\Business;
use Illuminate\Support\ServiceProvider;

class CurrentBusinessServiceValidator extends ServiceProvider
{
    use ServiceType;
    public function boot()
    {
        $this->app['validator']->extend('current_business_service', function ($attribute, $value, $parameters)
        {
            $businessId = request()->business_id;
            if($businessId) {
                $business = Business::find($businessId);
                if($business == null) {
                    return false;
                }
                $serviceIds = $this->getType($business->type_id)->where('business_id', $businessId)->pluck('id');
                return $serviceIds->contains($value);
            }
            return false;
        });
    }

    public function register()
    {
        //
    }
}
