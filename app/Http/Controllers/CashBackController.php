<?php

namespace App\Http\Controllers;

use App\Http\Requests\CashBackSetCashBackRequest;
use App\Http\Requests\CashBackSetConditionRequest;
use App\Models\CashBack;
use App\Models\CashBackCondition;
use Illuminate\Http\JsonResponse;

class CashBackController extends Controller
{

    /**
     * Set cash-back rule for business or business type.
     *
     * @param CashBackSetConditionRequest $request
     * @return JsonResponse
     */
    public function setCondition(CashBackSetConditionRequest $request)
    {
        CashBackCondition::add($request->all());
        return $this->created(null);
    }

    /**
     * Add extra bonus to client.
     *
     * @param CashBackSetCashBackRequest $request
     * @return JsonResponse
     */
    public function setCashBack(CashBackSetCashBackRequest $request)
    {
        CashBack::create($request->all());
        return $this->created(null);
    }

    /**
     * Get client extra bonuses collection.
     *
     * @param int $clientId
     * @return JsonResponse
     */
    public function getBonuses(int $clientId)
    {
        $bonuses = CashBack::where('client_id', $clientId)
            ->where('appointment_id', null)
            ->get();

        return $this->ok($bonuses);
    }

    /**
     * Get cash-back rules for all business types.
     *
     * @return JsonResponse
     */
    public function getBusinessTypeConditions()
    {
        $conditions = CashBackCondition::whereNotNull('business_type_id')->get();
        return $this->ok($conditions);
    }

    /**
     * Remove cash-back rule.
     *
     * @param CashBackCondition $condition
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteCondition(CashBackCondition $condition)
    {
        return $this->ok($condition->delete());
    }

    /**
     * Remove client cash-back.
     *
     * @param CashBack $cashBack
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteCashBack(CashBack $cashBack)
    {
        return $this->ok($cashBack->delete());
    }
}
