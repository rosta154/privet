<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Faker\Provider\en_ZA\PhoneNumber;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(User::class, function (Faker $faker) {
    $faker->addProvider(new PhoneNumber($faker));
    return [
        'phone' => $faker->mobileNumber,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'address' => $faker->address,
        'latitude' => $faker->latitude($min = 31.705791, $max = 32.919945),
        'longitude' => $faker->longitude($min = 34.643497, $max = 35.290146),
    ];

});
