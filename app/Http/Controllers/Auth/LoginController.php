<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LogOutRequest;
use App\Models\DeviceToken;
use App\Models\Follower;
use App\Models\Invite;
use App\Models\User;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Helpers\Interfaces\ISmsService;
use App\Http\Requests\LoginVerifyRequest;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    private $smsService;

    public function __construct(ISmsService $smsService)
    {
        $this->smsService = $smsService;
    }

    /**
     * Verify user phone and sent sms.
     *
     * @param LoginVerifyRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function verifyPhone(LoginVerifyRequest $request)
    {
        $this->smsService->checkPhone($request->phone);
        return $this->ok();
    }

    /**
     * Check sms code and login user.
     *
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function login(LoginRequest $request)
    {
        $this->smsService->codeVerification($request->phone, $request->code);
        $user = User::where('phone', $request->phone)->first();

        if(!$user) {
            $user = User::create(['phone' => $request->phone]);
            $follower = Follower::where([
                'phone' => $request->phone,
                'accepted' => false,
            ])->first();

            if($follower) {
                $follower->update();
            }
        }
        $invite_phone = Invite::where('phone',$user->phone)->first();
        if(!Empty($invite_phone)){
            $invite_phone->recipient_id = $user->id;
            $invite_phone->user_status = 3;
            $invite_phone->save();
            }
        $user->token = auth()->login($user);

        if($request->device_token) {
            $user->deviceTokens()->delete();
            DeviceToken::firstOrCreate(['user_id' => $user->id], ['token' => $request->device_token]);
        }

        return $this->ok($user->load('businesses', 'client'));
    }

    /**
     * Remove device token if user logout from app.
     *
     * @param LogOutRequest $request
     * @return JsonResponse
     */
    public function logOut(LogOutRequest $request)
    {
        DeviceToken::remove($request->device_token);
        return $this->ok();
    }
}
