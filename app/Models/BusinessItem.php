<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 */
class BusinessItem extends Model
{
    // TODO костиль)))
    public const DEFAULT_RADIUS = 50;

    /******* Attributes *******/
    protected $fillable = [

        'name',
        'image',
        'business_id',
        'address',
        'latitude',
        'longitude',
        'radius',
        'main',
        'about',
        'website',
        'city_id',
    ];


    /******* Relations *******/
    public function business()
    {
        return $this->belongsTo(Business::class);
    }

//    public function products()
//    {
//        return $this->hasMany(Product::class,'business_item_id');
//    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class);
    }

    public function temporarySchedule()
    {
        return $this->hasMany(TemporarySchedule::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }

    /******* Methods *******/
//    public static function boot()
//    {
//        parent::boot();
//
//        // After update.
//        static::updated(function (BusinessItem $businessItem) {
////            $request = request();
////            $fileManager = resolve(IFileManager::class);
////            if ($request->type_id == BusinessType::STORE) {
////                $business->updateStore($business, $request->schedule, $business->id);
////            } else {
////                $business->updateServices($request->services, $business->id, $business->type_id);
////                $business->updateBusinessItems($request->items, $business->id,$business);
////            }
////            $business->updateImages($request->all(), $fileManager);
//        });
//    }


    /******* Accessors *******/
    public function getImageAttribute($value)
    {
        return url(config('constants.image_folder.store_photo.get_path') . $value);
    }
}
