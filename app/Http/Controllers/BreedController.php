<?php

namespace App\Http\Controllers;

use App\Http\Requests\BreedRequest;
use App\Models\Breed;
use Illuminate\Http\Request;

class BreedController extends Controller
{

    /**
     * Get all a breeds from db.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->ok(Breed::all());
    }

    /**
     * Get breed by id.
     *
     * @param Breed $breed
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Breed $breed)
    {
        return $this->ok($breed);
    }

    /**
     * Create a new Breed.
     *
     * @param BreedRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BreedRequest $request)
    {
        $breed = Breed::create($request->all());
        return $this->created($breed);
    }

    /**
     * Updated a Breed.
     *
     * @param Breed $breed
     * @param BreedRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Breed $breed, BreedRequest $request)
    {
        $breed->update($request->all());
        return $this->updated($breed);
    }

    /**
     * Delete a breed.
     *
     * @param Breed $breed
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Breed $breed)
    {
        return $this->deleted($breed->delete());
    }
}
