<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  float price
 * @property  string name
 * @property  int pet_id
 * @property  int appointment_id
 * @property  int service_id
 * @property  int treatment_id
 * @property  int quantity
 */
class AppointmentItem extends Model
{
    /******* Properties *******/
    protected $fillable = [
        'price',
        'name',
        'pet_id',
        'service_id',
        'appointment_id',
        'treatment_id',
        'quantity',
    ];

    /******* Relations *******/

    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }

    public function service()
    {
        return $this->morphTo();
    }

    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }

    public function chat()
    {
        return $this->hasMany(ChatAppointment::class,'appointment_item_id');
    }
}
