<?php


namespace App\Helpers\Interfaces;


use App\Models\Appointment;
use App\Models\User;

interface INotifier
{
    public function newAppointmentRequest(Appointment $appointment);
    public function confirmedAppointment(Appointment $appointment);
    public function declinedAppointment(Appointment $appointment);
    public function cancelAppointment(Appointment $appointment);
    public function finishedAppointment(Appointment $appointment);
    public function customerDailyMeetingReminder(Appointment $appointment);
    public function customerSoonMeetingReminder(Appointment $appointment);
    public function walkStart(Appointment $appointment);
    public function walkEnd(Appointment $appointment);
    public function profileActivate(int $businessId);
    public function profileSuspended(int $businessId);
    public function paymentDeclined(Appointment $appointment);
    public function appointmentUpdated(Appointment $appointment);
    public function appointmentPaid(int $appointmentId);
    public function appointmentCompleted(int $appointmentId);
    public function petProfileShared(int $petId, int $recipientId);
    public function treatmentFirstReminder(string $petName, string $treatmentName, User $recipient);
    public function treatmentSecondReminder(string $petName, string $treatmentName, User $recipient);
}
