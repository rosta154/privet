<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\ChatAdminUser;
use App\Models\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function store(Request $request)
    {
        $chat = new ChatAdminUser($request->all());
        $sender = auth()->user();
        $chat->sender_id = $sender->id;
        if($sender->id == $chat->appointment->client->user->id && $request->recipient_id == $chat->appointment->business->user_id )
        {
            $chat->appointment()->update(['disput' => 1,'payment_status' => 2]);
        }


        if($sender->id == $chat->appointment->client->user->id && $request->recipient_id == 3)
        {
            $chat->appointment()->update(['disput' => 1,'payment_status' => 3]);
        }


        $chat->save();
        return $chat->makeHidden('appointment');
    }

    public function showChat(int $appointment)
    {
        $user = auth()->user();
        $chat = ChatAdminUser::where('appointment_id',$appointment)->get();

        if($user->admin == 1){

            $chat = ChatAdminUser::where('appointment_id',$appointment)->where('recipient_id',$user->id)->get();
            return $this->ok($chat);
        }
        foreach ($chat as $value) {
            $value->recipient_id;

            if ($user->id == $value->recipient_id) {

                $chat = ChatAdminUser::where('appointment_id', $appointment)->where('recipient_id', $user->id)->get();
                return $this->ok($chat->load('sender'));
            }
        }
        return $this->ok($chat->load('sender'));
    }
}
