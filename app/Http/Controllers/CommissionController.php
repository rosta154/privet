<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommissionCreateRequest;
use App\Http\Resources\CommissionResource;
use App\Models\Business;
use App\Models\Commission;
use Illuminate\Http\JsonResponse;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $businesses = Business::all()->load(['businessCommission', 'type.businessCommission']);
        $businesses = $businesses->map(function ($business) {
            $business->getCurrentCommissions();
            return $business;
        });
        return $this->ok(CommissionResource::collection($businesses));
    }

    /**
     * Get commission for all business types.
     *
     * @return JsonResponse
     */
    public function getTypeCommission()
    {
        $conditions = Commission::whereNotNull('business_type_id')->get();
        return $this->ok($conditions);
    }

    /**
     * Create or update commissions for business and business type.
     *
     * @param CommissionCreateRequest $request
     * @return JsonResponse
     */
    public function set(CommissionCreateRequest $request)
    {
        Commission::add($request->all());
        return $this->created(null);
    }

    /**
     * Display commission for current business.
     *
     * @param Business $business
     * @return JsonResponse
     */
    public function show(Business $business)
    {
        $business->getCurrentCommissions();
        return $this->ok($business->currentCommission);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Commission $commission
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Commission $commission)
    {
        if ($commission->business_type_id != null) {
            throw new \Exception("It is forbidden to remove commission for basic types; change its value to 0");
        }
        return $this->ok($commission->delete());
    }
}
