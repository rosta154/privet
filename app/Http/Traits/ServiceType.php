<?php


namespace App\Http\Traits;


use App\Models\Barbershop;
use App\Models\BusinessType;
use App\Models\Hotel;
use App\Models\Store;
use App\Models\Trainer;
use App\Models\Vet;
use App\Models\Walker;
use Exception;
use Illuminate\Database\Eloquent\Builder;

trait ServiceType
{
    /**
     * Get business type query builder.
     *
     * @param int $typeId
     * @return Builder
     * @throws Exception
     */
    public static function getType(int $typeId)
    {
        switch ($typeId) {
            case BusinessType::VETERINARIAN:
                return Vet::query();
            case BusinessType::STORE:
                return Store::query();
            case BusinessType::BARBERSHOP:
                return Barbershop::query();
            case BusinessType::TRAINER:
                return Trainer::query();
            case BusinessType::HOTEL:
                return Hotel::query();
            case BusinessType::WALKER:
                return Walker::query();
            default:
                throw new Exception("Incorrect business type");
        }
    }
}
