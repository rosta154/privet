<?php

namespace App\Console;

use App\Console\Commands\Client3HoursAppointmentReminder;
use App\Console\Commands\ClientTomorrowAppointmentReminder;
use App\Console\Commands\NextWeekTreatment;
use App\Console\Commands\ProviderDayAppointmentsREminder;
use App\Console\Commands\TreatmentInTwoDays;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ClientTomorrowAppointmentReminder::class,
        Client3HoursAppointmentReminder::class,
        ProviderDayAppointmentsREminder::class,
        NextWeekTreatment::class,
        TreatmentInTwoDays::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('reminder:client-first')->hourly();
//         $schedule->command('reminder:client-second')->cron('0 0/15 * 1/1 * ? *');
//         $schedule->command('reminder:provider-first')->everyMinute();
        $schedule->command('reminder:next-week')->cron('0 0 9 ? * MON,TUE,WED,THU,SUN *');
        $schedule->command('reminder:next-day')->cron('0 0 9 ? * MON,TUE,FRI,SAT,SUN *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
