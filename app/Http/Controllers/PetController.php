<?php

namespace App\Http\Controllers;

use App\Http\Traits\PhotoTrait;
use App\Models\Appointment;
use App\Models\AppointmentItem;
use App\Models\Pet;
use App\Models\PetType;
use App\Models\Treatment;
use App\Models\User;
use Illuminate\Http\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Requests\PetStoreRequest;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\PetUpdateRequest;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Interfaces\IFileManager;
class PetController extends Controller
{

    use PhotoTrait;
    /**
     * Get all user's pets.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $clientId = auth()->user()->client->id;
        $pets = Pet::where('client_id', $clientId)->get();
        return $this->ok($pets->load('petTypes','breeds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PetStoreRequest $request
     * @return JsonResponse
     */
    public function store(PetStoreRequest $request)
    {
        $pet = new Pet($request->all());
        $path =config('constants.image_folder.pets_photo.save_path');
        $pet->photo =  $this->savePhoto($request->photo, $path);
        $pet->save();
     return response()->json(['success' => true,'data' => $pet->load('petTypes','breeds')]);
    }

    /**
     *  Update pet information.
     *
     * @param PetUpdateRequest $request
     * @param Pet $pet
     * @return JsonResponse
     */
    public function update(PetUpdateRequest $request, Pet $pet)
    {
        //TODO Роман, перенеси логику в бут.
        if($request->has('photo')) {
            $path =config('constants.image_folder.pets_photo.save_path');
            if(!is_null($pet->photo)){
                $this->deletePhoto($pet->getOriginal('photo'), $path);
            }
            $pet->photo = $this->savePhoto($request['photo'], $path);
            $pet->update($request->except('photo'));
        }
        $pet->update($request->except('photo'));
        return $this->updated($pet->load('petTypes','breeds'));
    }

    /**
     * Display the specified resource.
     *
     * @param Pet $pet
     * @return JsonResponse
     */
    public function show(Pet $pet)
    {
        return $this->ok($pet->load('petTypes','breeds'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Pet $pet
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Pet $pet)
    {

        return $this->deleted($pet->delete());
    }

    /**
     * Get all pets.
     *
     * @return JsonResponse
     */
    public function all()
    {
        return $this->ok(Pet::all());
    }

    public function historyTreatment(int $pet)
    {
        $appointment = Appointment::
        where('payment_status',Appointment::PAID_PAYMENT_STATUS)
            ->whereHas('items',function ($query) use ($pet){
            $query->where('pet_id',$pet);
        })->with(['items' => function($q) use ($pet){
            $q->where('pet_id',$pet)->with('treatment');
        }])->get();
  return $this->ok($appointment->makeHidden(['type','pets']));
//        $appointmentItem = Appointment::where('pet_id',$pet)->get();
//        return response()->json(['success' => true,'data' => $appointmentItem]);
    }

}
