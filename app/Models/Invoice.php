<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int appointment_id
 * @property int price
 * @property int cash_back
 * @property int client_id
 */
class Invoice extends Model
{
    public const PENDING_STATUS = 1;
    public const SUCCESS_STATUS = 2;
    public const FAILED_STATUS = 3;

    protected $fillable = [
        'price',
        'appointment_id',
        'business_id',
        'client_id',
        'cash_back',
        'payme_sale_url',
        'payme_sale_id',
        'payme_sale_code',
    ];

    protected $guarded = [
        'status',
    ];
}
