<?php


namespace App\Http\Traits;


use App\Helpers\DTO\Point;

trait DistanceCalculator
{
    protected $milesToKmConversionFactor =  1.609344;
    protected $minutesInDegree = 60;
    protected $statuteMilesInNauticalMile = 1.1515;

    /**
     * Calculate distance between two geo points.
     *
     * @param Point $from
     * @param Point $to
     * @return float
     */
    public function calculateDistance(Point $from, Point $to)
    {
        $theta = $from->longitude - $to->longitude;
        $distance = (sin(deg2rad($from->latitude)) * sin(deg2rad($to->latitude)) +
            (cos(deg2rad($from->latitude)) * cos(deg2rad($to->latitude)) * cos(deg2rad($theta))));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * $this->minutesInDegree * $this->statuteMilesInNauticalMile;
        $distanceInKm = $distance * $this->milesToKmConversionFactor;

        return (round($distanceInKm,2));
    }
}
