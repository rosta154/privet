<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BusinessImage;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

$factory->define(BusinessImage::class, function (Faker $faker) {
    $path = $path = Storage::getAdapter()->getPathPrefix() . config('constants.image_folder.business_photo.save_path');
    return [
        'photo' => $faker->image($path, 400, 300, 'business', false),
    ];
});
