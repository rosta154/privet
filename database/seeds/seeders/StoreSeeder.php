<?php

use Illuminate\Database\Seeder;
use App\Models\Store;
class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $store = new Store();
        $store->name = 'name';
        $store->business_id = 2;
        $store->schedule_id = 1;
        $store->treatment_id = 1;
        $store->save();
    }
}
