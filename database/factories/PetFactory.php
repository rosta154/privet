<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pet;
use Faker\Generator as Faker;
//use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

$factory->define(Pet::class, function (Faker $faker) {
    $path = $path = Storage::getAdapter()->getPathPrefix() . config('constants.image_folder.pets_photo.save_path');
    return [
        'photo' => $faker->image($path, 400, 300, 'animals', false),
        'name' => $faker->firstName(),
        'date_of_birth' => $faker->date(),
        'weight' => rand(1,5),
        'gender' => 'male'?? 'female',
        'castrated' => $faker->boolean(),
        'client_id' =>  rand(1,20),
        'pet_type_id' =>  rand(1,2),
        'breed_id' => rand(1,4),
    ];
});
