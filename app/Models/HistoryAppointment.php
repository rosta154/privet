<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryAppointment extends Model
{
    protected $fillable = [
        'status',
        'payment_status',
        'appointment_id'
    ];

    public function appointment(){
        return $this->belongsTo(Appointment::class);
    }
}
