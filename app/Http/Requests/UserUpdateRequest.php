<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed is_client
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'max:177',
            'last_name' => 'max:177',
            'email' => 'max:177|email|',
            'address' => 'max:177',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'is_client' => 'boolean',
        ];
    }
}
