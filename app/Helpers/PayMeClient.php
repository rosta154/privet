<?php


namespace App\Helpers;


use App\Models\Appointment;
use App\Models\Business;
use App\Models\Invoice;
use App\Models\Seller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class PayMeClient
{
    private $key;
    private $secret;
    private $url;
    private $client;
    private $callBackUrl;

    private const MARKET_FEE = 5;
    private const CURRENCY = 'USD';
    private const CREATE_SELLER_LINK = "https://preprod.paymeservice.com/api/create-seller";
    private const GENERATE_SALE_LINK = "https://preprod.paymeservice.com/api/generate-sale";

    private const SALE_CALLBACK_URL = "https://www.example.com/payment/callback";

    private const SELLER_PAYME_ID = "MPL15767-064251WH-CLFOWQI5-PGZSE4P7";
    private const SELLER_PAYME_SECRET = "2XNMu3OyNaO3VfEhTNXtdKTl0Uiw2K";


    public function __construct()
    {
        $this->key = env('PAYME_CLIENT_KEY');
        $this->secret = env('PAYME_CLIENT_SECRET');
        $this->url = env('PAYME_API_URL');
        $this->client = new Client();
        $this->callBackUrl = "https://privet.qbitcom.tk/api/result";
    }

//    /**
//     * Create new seller on PayMe.
//     *
//     * @param array $data
//     * @param int $businessId
//     * @return mixed
//     * @throws \Exception
//     */
//    public function addSeller(array $data, int $businessId)
//    {
////        if($business->seller) {
////            return $business->seller;
////        }
//
//        // TODO get some info from user profile.
//        $data['payme_client_key'] = $this->key;
//        $data['market_fee'] = self::MARKET_FEE;
//        //$data['seller_id'] = $business->id;
//
//        $request = $this->client->request('POST', self::CREATE_SELLER_LINK, ['json' => $data]);
//        $body = json_decode($request->getBody());
//        $seller = Seller::create(get_object_vars($body) + ['business_id' => $businessId]);
//        return $seller;
//    }

    /**
     * Generate new sale on PayMe.
     *
     * @param Appointment $appointment
     * @return mixed
     * @throws \Exception
     */
    public function generateSale(Appointment $appointment, $cashBack)
    {
        $invoice = $this->createInvoice($appointment, $cashBack);
        $sellerId = env('PAYME_TEST_SELLER_ID');

        $request = $this->client->request('POST', self::GENERATE_SALE_LINK, ['json' =>
            $this->generateBody($appointment, $invoice, $sellerId)
        ]);

        $body = json_decode($request->getBody());
        $invoice->update([
            'payme_sale_url' => $body->sale_url,
            'payme_sale_id' => $body->payme_sale_id,
            'payme_sale_code' => $body->payme_sale_code,
        ]);

        return $invoice;
    }

    public function generateBody(Appointment $appointment, Invoice $invoice, string $sellerId)
    {
        $data = [
            //'seller_payme_id' => $appointment->business->seller->seller_payme_id,
            'seller_payme_id' => $sellerId,
            'sale_price' => $this->getPrice($invoice->price - $invoice->cash_back),
            'currency' => self::CURRENCY,
            'product_name' => "Appointment #$appointment->id",
            'transaction_id' => $invoice->id,
            'installments' => 1, // хз )))
            'capture_buyer' => 0,
            'sale_callback_url' => "$this->callBackUrl/$invoice->id",
            //'sale_return_url' => 'https://54628cd9.ngrok.io/api/result/1'
        ];
        return $data;
    }

    /**
     * Return price in cents.
     *
     * @param float $price
     * @return int
     */
    private function getPrice(float $price): int
    {
        return $price * 100;
    }

    private function createInvoice(Appointment $appointment, $cashBack)
    {
        if($cashBack != null) {
            $clientCashBack = $appointment->client->cashBack->sum('amount');
            if($clientCashBack < $cashBack) {
                throw new \Exception('Incorrect cash-back');
            }
        }
        return Invoice::create([
            'price' => $appointment->price,
            'appointment_id' => $appointment->id,
            'business_id' => $appointment->business_id,
            'client_id' => $appointment->client_id,
            'cash_back' => $cashBack ?? 0,
        ]);
    }

    public function getSeller()
    {
        $random = rand(1111111, 9999999);
        return [
            "payme_client_key" => "pri-vet_AnVx2WyW",
            "seller_id" => $random,
            "seller_first_name" => "First",
            "seller_last_name" => "Last",
            "seller_social_id" => "9999999999",
            "seller_birthdate" => "06/05/1989",
            "seller_social_id_issued" => "01/01/2000",
            "seller_gender" => 0,
            "seller_email" => "persona$random@gmail.com",
            "seller_phone" => "0540123456",
            "seller_contact_email" => "contact@example.com",
            "seller_contact_phone" => "031234567",
            "seller_bank_code" => 54,
            "seller_bank_branch" => 123,
            "seller_bank_account_number" => "123456",
            "seller_description" => "An online store which specializes in rubber ducks",
            "seller_site_url" => "www.babyducks.com",
            "seller_person_business_type" => 2000,
            "seller_inc" => 2,
            "seller_inc_code" => "123456",
            "seller_retail_type" => 1,
            "seller_merchant_name" => "Baby Ducks",
            "seller_address_city" => "Tel Aviv",
            "seller_address_street" => "Rothschild",
            "seller_address_street_number" => "1",
            "seller_address_country"=> "IL",
            "market_fee" => 5
        ];
    }
}
