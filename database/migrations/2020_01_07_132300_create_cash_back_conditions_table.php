<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashBackConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_back_conditions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('business_type_id')->nullable();
            $table->unsignedBigInteger('business_id')->nullable();
            $table->unsignedInteger('type');
            $table->unsignedInteger('amount');
            $table->unsignedInteger('life_time');

            $table->foreign('business_type_id')->references('id')->on('business_types');
            $table->foreign('business_id')->references('id')->on('businesses');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_back_conditions');
    }
}
