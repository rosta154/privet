<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\ViewsResponse;
use App\Http\Requests\StoreCreateRequest;
use App\Http\Requests\StoreUpdateRequest;
use App\Http\Requests\StoreViewsRequest;
use App\Http\Resources\StoreResource;
use App\Models\Business;
use App\Models\BusinessType;
use App\Models\Store;
use App\Models\View;
use Illuminate\Http\JsonResponse;

class StoreController extends Controller
{
    /**
     * Get store info.
     *
     * @param int store id
     * @return JsonResponse
     */
    public function show(int $store)
    {
        $store = Business::where('id', $store)
            ->with(
                'products',
                'businessItems.schedule',
                'images')
            ->first();

        View::addNewView($store->id);
        return $this->ok(new StoreResource($store));
    }

    /**
     * Get all stores from db.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $stores = Business::where('type_id', BusinessType::STORE)
            ->with('products', 'businessItems.schedule', 'images')
            ->searchByName()
            ->get();

        Store::addDistance($stores);
        return $this->ok(StoreResource::collection($stores));
    }

    /**
     * Get store views by date interval.
     *
     * @param StoreViewsRequest $request
     * @param int $store
     * @return JsonResponse
     */
    public function getViewsCollection(StoreViewsRequest $request, int $store)
    {
        $views = View::where('business_id', $store)
            ->whereBetween('date', array($request->start_date, $request->end_date))
            ->get();

        return $this->ok(new ViewsResponse($views));
    }

    /**
     * Add new store.
     *
     * @param StoreCreateRequest $request
     * @return JsonResponse
     */
    public function store(StoreCreateRequest $request)
    {
        $store = Business::create($request->all());
        $store->newToken = auth()->login(auth()->user());
        $store->load('products', 'businessItems.schedule', 'images');
        return $this->created(new StoreResource($store));
    }

    /**
     * Update store.
     *
     * @param StoreUpdateRequest $request
     * @param Business $store
     * @return JsonResponse
     */
    public function update(StoreUpdateRequest $request, Business $store)
    {
        $store->load('products', 'businessItems.schedule', 'images');
        $store->updateStore($request->all());
        return $this->ok();
    }



//    use PhotoTrait;
//    // TODO function description in comments must be at the top raw, after empty raw, after @params/@return.
//
//    protected $filemanager;
//
//    public function __construct(IFileManager $filemanager)
//    {
//        $this->filemanager = $filemanager;
//    }
//
//    /**
//     *  All Store
//     * @return \Illuminate\Http\JsonResponse
//     */

//
//    /**
//     * Store a newly created resource in storage.
//     * @param StoreRequest $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function store(StoreRequest $request)
//    {
//        $pet = new Store($request->all());
//        $path =config('constants.image_folder.store_photo.save_path');
//        $pet->image =  $this->savePhoto($request->image, $path);
//
//        $pet->save();
//        return $this->created($pet->load(['','products']));
//    }
//
//    public function addBusinessItems(array $items, int $businessId)
//    {
//        foreach ($items as $item) {
//            $businessItem = BusinessItem::create($item + ['business_id' => $businessId]);
//            $this->addSchedule($item['schedule'], $businessItem->id);
//        }
//    }
//
//    //TODO bus id from this
//    public function addSchedule(array $schedule, int $businessItemId)
//    {
//        //TODO save range
//        foreach ($schedule as $item) {
//            Schedule::create($item + ['business_item_id' => $businessItemId]);
//        }
//    }
//
//    /**
//     *
//     *update store))
//     * @param StoreUpdateRequest $request
//     * @param Store $store
//     * @return \Illuminate\Http\JsonResponse
//     */
//
//    public function update(StoreUpdateRequest $request, Store $store)
//    {
//
//        if($request->has('image')) {
//            $path =config('constants.image_folder.store_photo.save_path');
//            if(!is_null($store->image)){
//                $this->deletePhoto($store->image, $path);
//            }
//            $store->image = $this->savePhoto($request['image'], $path);
//        }
//        return $this->updated($store->load(['businesses.businessItems.schedule','products']));
//    }
//
//    /**
//     *
//     *  Display the specified resource.
//     *
//     * @param Store $store
//     * @return \Illuminate\Http\JsonResponse
//     */
//
//    public function show(Store $store)
//    {
//        return $this->ok($store->load(['businesses.businessItems.schedule','products']));
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param Store $store
//     * @return \Illuminate\Http\JsonResponse
//     */
//
//    public function destroy(Store $store)
//    {
//        //TODO delete image
//        return $this->deleted($store->delete());
//    }
//
//    public function search(StoreSearchRequest $request)
//    {
//        $shop = Store::where('name','like','%'.$request->name.'%')
//            ->with(['businesses','schedules','products'])->get();
//        return $this->ok($shop);
//    }

}
