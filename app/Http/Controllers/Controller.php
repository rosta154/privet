<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Status codes constants.
     */
    const STATUS_CODE = [
        'success' => 200,
        'created' => 201,
        'no content' => 204
    ];

    /**
     * Return success response.
     *
     * @param $result
     * @return JsonResponse
     */
    public function ok($result = null)
    {
        $statusCode = $this->getCode($result);
        return response()->json($this->getBody($result), $statusCode);
    }

    /**
     * Return created response.
     *
     * @param $result
     * @return JsonResponse
     */
    public function created($result)
    {
        return response()->json($this->getBody($result), self::STATUS_CODE['created']);
    }

    /**
     * Return updated response.
     *
     * @param null $result
     * @return JsonResponse
     */
    public function updated($result = null)
    {
        $statusCode = $this->getCode($result);
        return response()->json($this->getBody($result), $statusCode);
    }

    /**
     * Return deleted response.
     *
     * @param null $result
     * @return JsonResponse
     */
    public function deleted($result = null)
    {
        return response()->json(null, self::STATUS_CODE['no content']);
    }

    /**
     * Get status code.
     *
     * @param $result
     * @return int
     */
    private function getCode($result)
    {
        return $result == null ? self::STATUS_CODE['no content'] : self::STATUS_CODE['success'];
    }

    /**
     * Get body.
     *
     * @param $result
     * @return array
     */
    private function getBody($result)
    {
        return ['success' => true, 'data' => $result];
    }
}
