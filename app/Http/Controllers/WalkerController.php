<?php

namespace App\Http\Controllers;

use App\Http\Requests\WalkerRequest;
use App\Models\Walker;
use Illuminate\Http\Request;

class WalkerController extends Controller
{

    public function update(Request $request)
{
    foreach ($request->all() as $item)
    {
        $find = Walker::find($item['id']);
        $find->update($item);

    }
    return $this->updated($find);
}

    public function store(Request $request){
        foreach ($request->all() as $item){
            $walker = new Walker($item);
            $walker->save();
        }
        return $this->created($walker);
    }
}
