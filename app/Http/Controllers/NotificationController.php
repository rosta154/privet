<?php

namespace App\Http\Controllers;

use App\Helpers\Interfaces\INotifier;
use App\Http\Requests\ReadNotifiRequest;
use App\Models\Appointment;
use App\Models\Notification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    private $notifier;

    public function __construct(INotifier $notifier)
    {
        $this->notifier = $notifier;
    }

    /**
     * Notify client about start walk.
     *
     * @param Appointment $appointment
     * @return JsonResponse
     */
    public function walkStart(Appointment $appointment)
    {
        $appointment->load(['client.user', 'business']);
        $this->notifier->walkStart($appointment);
        return $this->ok();
    }

    /**
     * Notify client about end walk.
     *
     * @param Appointment $appointment
     * @return JsonResponse
     */
    public function walkEnd(Appointment $appointment)
    {
        $appointment->load(['client.user', 'business']);
        $this->notifier->walkEnd($appointment);
        return $this->ok();
    }

    public function changeRead (Notification $notification, ReadNotifiRequest $request)
    {
        return response()->json(['success' => $notification->update($request->only('is_read')),'data' =>$notification]);

    }

    public function clientNotifications(int $clientId)
    {
       $notifi = Notification::where('recipient_business_id', null)->where('user_id', $clientId)->get();
       return $this->ok($notifi->load('user','appointment'));
    }

    /**
     * Get all business's notifications
     *
     * @param int $providerId
     * @return JsonResponse
     */
    public function providerNotifications(int $providerId)
    {
        // TODO validation business owner.
       $notifications = Notification::where('recipient_business_id', $providerId)->get();
       return $this->ok($notifications);
    }

    public function allNotify()
    {
       $notifi = Notification::with('user')->get();
       return $this->ok($notifi);
    }

    public function countNotifyClient(int $userId)
    {
      $notify = Notification::where('recipient_business_id', null)
          ->where('is_read', false)
          ->where('user_id', $userId)
          ->count();
      return $this->ok($notify);
    }

    public function countNotifyProvider(int $businessId)
    {
      $notify = Notification::where('is_read', false)
          ->where('recipient_business_id', $businessId)
          ->count();
      return $this->ok($notify);
    }
}
