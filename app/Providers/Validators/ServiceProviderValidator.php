<?php


namespace App\Providers\Validators;


use App\Models\BusinessType;
use Illuminate\Support\ServiceProvider;

class ServiceProviderValidator extends ServiceProvider
{

    public function boot()
    {
        $this->app['validator']->extend('not_a_store', function ($attribute, $value, $parameters)
        {
            if ($value == BusinessType::STORE || $value == BusinessType::WALKER) {
                return false;
            }
            return true;
        });
    }

    public function register()
    {
        //
    }
}
