<?php

namespace App\Http\Middleware;

use App\Verification;
use Closure;

class CheckCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Verification::checkByPhone($request->phone)){
            $verification = Verification::getByPhone($request->phone);
            if($verification['phone'] == $request->phone && $verification['code'] == $request->code){
                return $next($request);
            }
        }
        return response('Unauthorized action.', 403);
    }
}
