<?php

namespace App\Models;

use App\Models\Appointment;
use Illuminate\Database\Eloquent\Model;

class DeclineComment extends Model
{
    protected $fillable = ['appointment_id','comment'];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
}
