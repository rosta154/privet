<?php

use App\Models\Appointment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('price');
            $table->float('rating')->default(0);
            $table->dateTime('date');
            $table->dateTime('end_date')->nullable();
            $table->text('comment')->nullable();
            $table->text('note')->nullable();
            $table->string('address')->nullable();
            $table->string('disput')->default(0);
            $table->double('latitude',10,6);
            $table->double('longitude',10,6);
            $table->unsignedInteger('status')->default(Appointment::PENDING_STATUS);
            $table->unsignedInteger('payment_status')->default(Appointment::APPOINTMENT_CREATED);
            $table->unsignedBigInteger('business_type_id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('business_id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('business_item_id');

            $table->foreign('business_type_id')->references('id')->on('business_types')->onDelete('cascade');
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('treatments')->onDelete('cascade');
            $table->foreign('business_item_id')->references('id')->on('business_items')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
