<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->string('address')->nullable();
            $table->double('latitude',10,6)->nullable();
            $table->double('longitude',10,6)->nullable();
            $table->integer('radius')->nullable();
            $table->boolean('main')->default(false);
            $table->string('website')->nullable();
            $table->string('about')->nullable();
            $table->unsignedBigInteger('business_id');
            $table->unsignedBigInteger('city_id')->nullable();

            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_items');
    }
}
