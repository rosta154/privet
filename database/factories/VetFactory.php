<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vet;
use Faker\Generator as Faker;

$factory->define(Vet::class, function (Faker $faker) {
    return [
        'price' => rand(50, 100),
        'min_price' => rand(30, 50),
        'non_working_price' => rand(100, 150)
    ];
});
