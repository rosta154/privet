<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property array treatments
 * @property int business_type_id
 */
class BusinessSearchByTreatmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_type_id' => 'required|exists:business_types,id',
            'treatments' => 'required|array',
            'treatments.*' => 'exists:treatments,id',
            'time' => 'required|date_format:Y-m-d H:i'
        ];
    }
}
