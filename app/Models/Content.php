<?php

namespace App\Models;

use App\Http\Traits\PhotoTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
class Content extends Model
{
    use PhotoTrait;
    protected $fillable = [
      'name','description','image'
    ];


    public function getImageAttribute($value)
    {
        $getPath = Config::get('constants.image_folder.content_photo.get_path');
        return url($getPath . $value);
    }

    public static function boot() {
        parent::boot();

        static::deleting(function (Content $content) {
            $path = config('constants.image_folder.content_photo.save_path');
            $content->deletePhoto($content->getOriginal('image'),$path);
        });
    }
}
