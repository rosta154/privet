<?php


namespace App\Providers\Validators;


use App\Models\Treatment;
use Illuminate\Support\ServiceProvider;

class CorrectServiceTypesValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('current_business_type_services', function ($attribute, $value, $parameters)
        {
            $treatments = Treatment::where('business_type_id', request()->type_id)->pluck('id');
            $serviceCollection = collect($value)->pluck('treatment_id');
            return $treatments->intersect($serviceCollection)->count() == $serviceCollection->count();
        });
    }

    public function register()
    {
        //
    }
}
