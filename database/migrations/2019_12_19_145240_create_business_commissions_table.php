<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_commissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('fix_price')->nullable();
            $table->float('price_commission')->nullable();
            $table->float('min')->nullable();
            $table->float('max')->nullable();
            $table->unsignedBigInteger('business_id')->nullable();
            $table->unsignedBigInteger('type_id')->nullable();

            $table->foreign('type_id')
                ->references('id')
                ->on('business_types')
                ->onDelete('cascade');

            $table->foreign('business_id')
                ->references('id')
                ->on('businesses')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_commissions');
    }
}
