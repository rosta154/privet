<?php

namespace App\Http\Requests;

use App\Models\Commission;
use Illuminate\Foundation\Http\FormRequest;

class CommissionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = [Commission::FIXED_TYPE, Commission::PERCENT_TYPE];

        return [
            'business_type_id' => 'exists:business_types,id|required_without:business_id',
            'business_id' => 'exists:businesses,id|required_without:business_type_id',
            'type' => 'required|in:' .implode($types, ','),
            'amount' => 'required|numeric',
            'life_time' => 'date_format:Y-m-d H:i',
        ];
    }
}
