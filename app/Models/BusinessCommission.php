<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessCommission extends Model
{
    protected $fillable = [
        'price_commission',
        'fix_price',
        'max',
        'min',
        'business_id',
        'type_id'
    ];

    public function business(){
        return $this->belongsTo(Business::class);
    }

    public function type(){
        return $this->belongsTo(BusinessType::class);
    }

}
