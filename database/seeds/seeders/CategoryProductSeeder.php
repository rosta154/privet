<?php

use Illuminate\Database\Seeder;
use App\Models\CategoryProduct;
class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pet = new CategoryProduct();
        $pet->name = 'Dog food';
        $pet->save();

        $pet = new CategoryProduct();
        $pet->name = 'Dog cloth';
        $pet->save();

        $pet = new CategoryProduct();
        $pet->name = 'Pet fish';
        $pet->save();
    }
}
