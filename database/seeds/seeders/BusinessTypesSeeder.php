<?php

use App\Models\BusinessType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class BusinessTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws ReflectionException
     */
    public function run()
    {
//        $reflection = new ReflectionClass(BusinessType::class);
//        $reflectionParent = new ReflectionClass(get_parent_class(BusinessType::class));
//        $types = Arr::except(
//            $reflection->getConstants(),
//            array_keys($reflectionParent->getConstants())
//        );
        $types = BusinessType::TYPES;

        BusinessType::unguard();

        foreach($types as $key => $value) {
            $newType = new BusinessType();
            $newType->id = $key;
            $newType->name = $value;
            $newType->save();
        }
    }
}
