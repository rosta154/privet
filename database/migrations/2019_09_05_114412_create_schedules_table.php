<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('week_day')->nullable();
            $table->string('name')->nullable();
            $table->time('opening_time');
            $table->time('closing_time');
            $table->boolean('rule')->default(false);
            $table->boolean('available')->default(true);
            $table->unsignedBigInteger('business_item_id');

            $table->foreign('business_item_id')
                ->references('id')
                ->on('business_items')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
