<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryProductRequest;
use Illuminate\Http\Request;
use App\Models\CategoryProduct;
class CategoryProductController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['success' => true, 'data' => CategoryProduct::all()]);
    }

    /**
     * @param CategoryProduct $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(CategoryProduct $product)
    {
        return response()->json(['success' => true,'data' => $product]);
    }

    /**
     * @param CategoryProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryProductRequest $request)
    {
        $product = new CategoryProduct($request->all());
        return response()->json(['success' => $product->save(),'data' => $product]);
    }

    /**
     * @param CategoryProduct $product
     * @param CategoryProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryProduct $product,CategoryProductRequest $request)
    {
        return response()->json(['success' => $product->update($request->all()),'data' => $product]);
    }

    /**
     * @param CategoryProduct $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CategoryProduct $product)
    {
        return response()->json(['success' => $product->delete(),'message' => 'Category has been deleted']);
    }
}
