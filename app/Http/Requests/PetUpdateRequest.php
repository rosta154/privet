<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PetUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => '',
            'name' => 'required',
            'pet_type_id' => 'required|exists:pet_types,id',
            'breed_id' => 'required|exists:breeds,id',
            'date_of_birth' => 'required',
            'weight' => 'required',
            'gender' => 'required|in:male,female',
            'castrated' => 'required|in:0,1',
        ];
    }
}
