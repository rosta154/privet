<?php

namespace App\Helpers;

use App\Helpers\DTO\DateSchedule;
use App\Helpers\DTO\IntervalSchedule;
use App\Helpers\DTO\ProviderCalendar;
use App\Helpers\DTO\ShortSchedule;
use App\Helpers\Interfaces\IScheduleHelper;
use App\Models\Appointment;
use App\Models\BusinessItem;
use App\Models\Schedule;
use App\Models\TemporarySchedule;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;

class ScheduleHelper implements IScheduleHelper
{
    private const INTERVAL = 15;

    /**
     * Generate week schedule for provider.
     *
     * @param array $businessItems
     * @param string $startDate
     * @param string $endDate
     * @return ProviderCalendar
     */
    public function generateProviderSchedule(array $businessItems, string $startDate, string $endDate): ProviderCalendar
    {
        $customerCalendar = $this->generateTimeSchedule($businessItems, $startDate, $endDate);

        $timeIntervals = collect($customerCalendar)
            ->pluck('timeInterval')
            ->collapse()
            ->pluck('time')
            ->unique()
            ->values();

        $daysAppointments = new Collection();
        collect($customerCalendar)->each(function ($item) use ($daysAppointments) {
            $daysAppointments->add($item->appointments);
        });

        return new ProviderCalendar($timeIntervals, $daysAppointments);
    }

    /**
     * Get time schedule.
     *
     * @param array $businessItems
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function generateTimeSchedule(array $businessItems, string $startDate, string $endDate): array
    {
        $period = $this->periodSchedule($businessItems, $startDate, $endDate);

        foreach ($period as $day) {
            $this->getDayValue($day);
        }
        return $period;
    }

    /**
     * Get specific schedule for one item.
     *
     * @param BusinessItem $item
     * @param Carbon $day
     * @return false|string|null
     */
    public function getItemTimeSchedule(BusinessItem $item, Carbon $day)
    {
        $daySchedule = $this->getDaySchedule($item->schedule, $day);
        $dayTemporarySchedules = $this->getDayTemporarySchedule($item->temporarySchedule, $day);
        $dayAppointments = $this->getDayAppointments($item->appointments, $day);

        $dateSchedule = new DateSchedule($day, $daySchedule + $dayTemporarySchedules, $dayAppointments);
        $this->getDayValue($dateSchedule);

        if($dateSchedule->available == false)
            return null;
        else {
            return date('h:i:s', $this->getNearestAvailableTime($day, $dateSchedule));
        }
    }

    /**
     * Get nearest business available time.
     *
     * @param Carbon $dateTime
     * @param DateSchedule $dateSchedule
     * @return false|float|int
     */
    private function getNearestAvailableTime(Carbon $dateTime, DateSchedule $dateSchedule)
    {
        $searchTime = strtotime($dateTime->toTimeString());
        $openingTime = strtotime($dateSchedule->openingTime);
        $closingTime = strtotime($dateSchedule->closingTime);

        if($openingTime <= $searchTime && $closingTime >= $searchTime) {
            return $searchTime;
        } else {
            $flag = 1;
            while($searchTime != $openingTime && $searchTime != $closingTime) {
                $searchTime = $searchTime + (self::INTERVAL * 60 * $flag);
                $flag = -($flag < 0 ? --$flag : ++$flag);
            }
            return $searchTime;
        }
    }

    /**
     * Get values for current day.
     *
     * @param DateSchedule $day
     * @return DateSchedule|void
     */
    private function getDayValue(DateSchedule $day)
    {
        $day->weekDay = $this->getWeekDay($day->date);
        if(count($day->schedule) > 0) {
            $interval = array();
            $schedules = collect($day->schedule);

            $startDay = $this->getStartDay($schedules);
            if($startDay == null) {
                $day->available = false;
                return;
            }
            $endDay = Carbon::parse($schedules->where('available', true)->sortByDesc('closing_time')->first()->closing_time);

            while($startDay < $endDay) {
                $flag = false;
                $intervalStart = Carbon::parse($startDay->toTimeString());
                $startDay->addMinutes(self::INTERVAL);

                foreach ($day->schedule as $schedule) {
                    if(Carbon::parse($schedule->opening_time) <= $intervalStart && Carbon::parse($schedule->closing_time) >= $startDay) {
                        $flag = $schedule->available;
                    }
                }
                $interval[] = new IntervalSchedule($day->date, $intervalStart->toTimeString(), $flag, array());
            }
            $day->timeInterval = $interval;
            $day->available = $this->checkDayAvailableStatus($interval);

            $collection = collect($interval);
            $day->openingTime = $collection->first()->time;
            $day->closingTime = Carbon::parse($collection->last()->time)->addMinutes(self::INTERVAL)->toTimeString();
        } else {
            $day->available = false;
            $day->timeInterval = array();
        }
    }

    /**
     * Get day start time with considering time interval.
     *
     * @param Collection $schedule
     * @return Carbon
     */
    private function getStartDay(Collection $schedule)
    {
        $availableSchedule = $schedule->where('available', true)->sortBy('opening_time')->first();
        if($availableSchedule != null) {
            $notFormatted = Carbon::parse($availableSchedule->opening_time);
            $start = Carbon::parse("$notFormatted->hour:00");
            while ($start < $notFormatted) {
                $start->addMinutes(self::INTERVAL);
            }
            return $start;
        }
        return null;
    }

    /**
     * Get business schedule with appointments for current period.
     *
     * @param array $businessItems
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function periodSchedule(array $businessItems, string $startDate, string $endDate) : array
    {
        $result = array();

        $schedules = Schedule::whereIn('business_item_id', $businessItems)->get();
        $temporarySchedules = TemporarySchedule::whereIn('business_item_id', $businessItems)->get();

        $appointments = Appointment::whereIn('business_item_id', $businessItems)
            ->whereIn('status', [Appointment::APPROVED_STATUS, Appointment::PENDING_STATUS])
            ->whereBetween('date', [
                Carbon::parse($startDate)->startOfDay(),
                Carbon::parse($endDate)->startOfDay()->addDay()
            ])->get();

        $appointments = $this->addEndTime($appointments);
        $appointments = $this->addAppointmentSize($appointments);

        $interval = $this->getDaysInterval($startDate, $endDate);

        foreach ($interval as $day) {
            $daySchedule = $this->getDaySchedule($schedules, $day);
            $dayTemporarySchedules = $this->getDayTemporarySchedule($temporarySchedules, $day);
            $dayAppointments = $this->getDayAppointments($appointments, $day);

            $result[] = new DateSchedule($day, $daySchedule + $dayTemporarySchedules, $dayAppointments);
        }

        return $result;
    }

    /**
     * Add end date to not confirmed appointments.
     *
     * @param Collection $appointments
     * @return Collection
     */
    private function addEndTime(Collection $appointments): Collection
    {
        return $appointments->map(function ($item, $key) {
            if($item['end_date'] == null) {
                $item['end_date'] = Carbon::parse($item['date'])->addMinutes(self::INTERVAL)->format('Y-m-d H:i:s');
            }

            return $item;
        });
    }

    private function addAppointmentSize(Collection $appointments): Collection
    {
        return $appointments->map(function ($item) {
            if($item['date'] != null && $item['end_date'] != null) {
                $item['time_interval_size'] = ceil((Carbon::parse($item['end_date']))->diffInMinutes(Carbon::parse($item['date'])) / self::INTERVAL);
            }
            return $item;
        });
    }

    /**
     * Get array of days from interval.
     *
     * @param string $startDate
     * @param string $endDate
     * @return \Carbon\CarbonInterface[]
     */
    private function getDaysInterval(string $startDate, string $endDate)
    {
        $period = CarbonPeriod::create($startDate, $endDate);
        return $period->toArray();
    }

    /**
     * Get business appointments for current day.
     *
     * @param Collection $appointments
     * @param $day
     * @return Collection
     */
    private function getDayAppointments(Collection $appointments, $day): Collection
    {
        return $appointments
            ->whereBetween('date', [Carbon::parse($day)->startOfDay(), Carbon::parse($day)->endOfDay()]);
    }

    /**
     * Get all temporary schedules for current day.
     *
     * @param array $temporarySchedules
     * @param $day
     * @return array
     */
    private function getDayTemporarySchedule(Collection $temporarySchedules, $day)
    {
        $dayTemporarySchedules = array();

        foreach ($temporarySchedules as $rule) {
            if($day->between(Carbon::parse($rule['start_date']), Carbon::parse($rule['end_date']))) {
                $dayTemporarySchedules[] = $rule;
            }
        }
        return $this->convertTemporaryScheduleToShortModel($dayTemporarySchedules);
    }

    /**
     * Get schedule and all rules for current day.
     *
     * @param array $schedules
     * @param $day
     * @return array
     */
    private function getDaySchedule(Collection $schedules, $day)
    {
        $daySchedule = collect($schedules)->where('week_day', $day->dayOfWeek)->values()->toArray();
        return $this->convertSchedulesToShortModel($daySchedule);
    }

    /**
     * Convert schedule to default model.
     *
     * @param array $schedule
     * @return array
     */
    private function convertSchedulesToShortModel(array $schedule)
    {
        $result = array();
        foreach ($schedule as $item) {
            $result[] = new ShortSchedule(
                $item['opening_time'],
                $item['closing_time'],
                $item['rule'],
                $item['available'],
                $item['updated_at']
            );
        }
        return $result;
    }

    /**
     * Convert temporary schedule to default model.
     *
     * @param array $schedule
     * @return array
     */
    private function convertTemporaryScheduleToShortModel(array $schedule)
    {
        $result = array();
        foreach ($schedule as $item) {
            $result[] = new ShortSchedule(
                $item['opening_time'],
                $item['closing_time'],
                true,
                $item['available'],
                $item['updated_at']
            );
        }
        return $result;
    }

    /**
     * Get week day from date.
     *
     * @param string $date
     * @return false|string
     */
    private function getWeekDay(string $date)
    {
        return date('N', strtotime($date));
    }

    private function checkDayAvailableStatus(array $interval)
    {
        return collect($interval)->contains('available', true);
    }
}
