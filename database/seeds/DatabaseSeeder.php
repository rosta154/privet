<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /******* SEED COUNTS CONSTANTS *******/
    const USERS_COUNT = 60;
    const CLIENTS_COUNTS = 20;
    const TREATMENT_COUNTS = 50;
    const PET_COUNTS = 20;
    const BUSINESSES_COUNT = 40;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ClientSeeder::class,

            PetTypeSeeder::class,
            BreedSeeder::class,
            PetSeeder::class,

            CategoryProductSeeder::class,

            BusinessTypesSeeder::class,
            BusinessSeeder::class,
            SubjectSeeder::class,
            QuestionSeeder::class,
            AdminSeeder::class,
            StaticTextSeeder::class,
            FAQSeeder::class,
            CityTableSeeder::class,
        ]);
    }

    /**
     * Make folder if not exist.
     *
     * @param string $folder
     */
    public static function recreatingFolder(string $folder)
    {
        if(Storage::exists($folder)) {
            Storage::deleteDirectory($folder);
            Storage::makeDirectory($folder);
        }
        else {
            Storage::makeDirectory($folder);
        }
    }
}
