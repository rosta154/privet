<?php

namespace App\Http\Controllers;

use App\Models\BusinessType;
use App\Models\Treatment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BusinessTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->ok(BusinessType::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param BusinessType $type
     * @return JsonResponse
     */
    public function update(Request $request, BusinessType $type)
    {
        $type->update(['name' => $request->name]);
        return $this->updated();
    }

    /**
     * Get all business type's treatments.
     *
     * @param $type
     * @return JsonResponse
     */
    public function treatments($type)
    {
        $treatments = Treatment::where('type_id', $type)->get();
        return $this->ok($treatments);
    }
}
