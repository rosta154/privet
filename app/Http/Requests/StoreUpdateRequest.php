<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

        return [
            'name' => 'string|max:255',
            'address' => 'string|max:255',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'website' => 'regex:' . $regex,
            'about' => 'string|max:255',
            'main_image' => 'is_base64_image',
            // TODO validate this store images
            'deleted_images' => 'array',
                'deleted_images.*' => 'exists:business_images,id',
            'schedule' => 'required|array',
//                'schedule.*.week_day' => 'numeric|required_without:schedule.*.name',
//                'schedule.*.name' => 'string:max255|required_without:schedule.*.week_day',
                'schedule.*.name' => 'required|string:max255',
                'schedule.*.opening_time' => 'required|date_format:H:i',
                'schedule.*.closing_time' => 'required|date_format:H:i|after:schedule.*.opening_time',
                //'schedule.*.available' => 'required|boolean',
        ];
    }
}
