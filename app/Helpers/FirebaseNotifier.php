<?php


namespace App\Helpers;


use App\Helpers\DTO\NotificationMetadata;
use App\Helpers\Interfaces\INotifier;
use App\Helpers\StaticClasses\NotificationUrl;
use App\Models\Appointment;
use App\Models\Business;
use App\Models\Client;
use App\Models\Notification;
use App\Models\Pet;
use App\Models\User;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\Exceptions\InvalidOptionsException;
use LaravelFCM\Message\Options;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadData;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotification as PayloadNotificationAlias;
use LaravelFCM\Message\PayloadNotificationBuilder;

class FirebaseNotifier implements INotifier
{
    private const NEW_APPOINTMENT_REQUEST = "%s would like to book an appointment. Click to review and respond.";
    private const APPOINTMENT_CONFIRMED = "Your Appointment is confirmed! %s accepted your appointment.";
    private const APPOINTMENT_DECLINED = "%s declined the appointment.";
    private const APPOINTMENT_CANCEL = "%s cancelled the appointment.";
    private const APPOINTMENT_FINISHED = "Appointment %s is finished. please approve the payment.";
    private const TOMORROW_CLIENT_REMINDER = "Reminder: Your Appointment to %s is scheduled for tomorrow.";
    private const APPOINTMENT_CLIENT_REMINDER = "Reminder: Your Appointment to %s is in 3 hours.";
    private const WALK_START = "The walk with %s has started. Track current location.";
    private const WALK_END = "The walk with %s has ended.";
    private const PROFILE_ACTIVATE = "You're profile has been activated. You can now accept appointment requests.";
    private const PROFILE_SUSPENDED = "You're Profile has been suspended. Please contact Customer Service for more details.";
    private const PAYMENT_DECLINED = "%s has declined payment for Appointment #%d.";
    private const APPOINTMENT_UPDATED = "%s has editted appointment #%d . review and approve payment.";
    private const APPOINTMENT_COMPLETED_SP = "Payment received from %s! Appointment #%d is completed.";
    private const APPOINTMENT_COMPLETED_CUSTOMER = "Appointment #%d is completed. Don't forget to rate %s.";
    private const PET_PROFILE_SHARED = "%s has shared his pets' profile with you.";
    private const TREATMENTS_FIRST_REMINDER = "Reminder: %s 's next %s is due next week. Book in advance.";
    private const TREATMENTS_SECOND_REMINDER = "Reminder: %s 's next %s is in 2 days. Book now.";



    private const TIME_TO_LIVE = 60*20;
    private $options;
    private $data;

    public function __construct()
    {
        $this->options = $this->getOptions();
        $this->data = $this->getData();
    }

    /**
     * Send new appointment notification tp business owner.
     *
     * @param Appointment $appointment
     */
    public function newAppointmentRequest(Appointment $appointment)
    {
        $type = 1; //TODO set actual type
        $sender = auth()->user();
        $recipient = $appointment->business->user;

        // TODO url constants or config
        $data = new NotificationMetadata(NotificationUrl::NEW_APPOINTMENT . $appointment->id, $appointment->id, $appointment->business_id, $type, true);

        $title = 'New appointment';
        $body = sprintf(self::NEW_APPOINTMENT_REQUEST, "$sender->first_name $sender->last_name");

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Confirm appointment by provider.
     *
     * @param Appointment $appointment
     */
    public function confirmedAppointment(Appointment $appointment)
    {
        $type = 1;
        $recipient = Client::find($appointment->client_id)->user;
        $business = Business::find($appointment->business_id);

        $title = 'Confirmed appointment';
        $body = sprintf(self::APPOINTMENT_CONFIRMED, "$business->name");

        $data = new NotificationMetadata(NotificationUrl::CLIENT_APPOINTMENT . $appointment->id, $appointment->id, null, $type, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Decline appointment by provider.
     *
     * @param Appointment $appointment
     */
    public function declinedAppointment(Appointment $appointment)
    {
        $type = 1;
        $recipient = Client::find($appointment->client_id)->user;
        $business = Business::find($appointment->business_id);

        $title = 'Declined appointment';
        $body = sprintf(self::APPOINTMENT_DECLINED, "$business->name");

        $data = new NotificationMetadata(NotificationUrl::CLIENT_APPOINTMENT . $appointment->id, $appointment->id, null, $type, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Cancel appointment.
     *
     * @param Appointment $appointment
     */
    public function cancelAppointment(Appointment $appointment)
    {
        $type = 1;
        $recipient = Client::find($appointment->client_id)->user;
        $business = Business::find($appointment->business_id);

        $title = 'Appointment cancelled';
        $body = sprintf(self::APPOINTMENT_CANCEL, "$business->name");

        $data = new NotificationMetadata(NotificationUrl::CLIENT_APPOINTMENT . $appointment->id, $appointment->id, null, $type, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Send notification to client after appointment finished.
     *
     * @param Appointment $appointment
     */
    public function finishedAppointment(Appointment $appointment)
    {
        $type = 1;
        $recipient = Client::find($appointment->client_id)->user;

        $title = 'Finished appointment';
        $body = sprintf(self::APPOINTMENT_FINISHED, $appointment->id);

        $data = new NotificationMetadata(null, $appointment->id, $appointment->business_id, $type, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Client reminder about tomorrow appointment.
     *
     * @param Appointment $appointment
     */
    public function customerDailyMeetingReminder(Appointment $appointment)
    {
        $type = 1;
        $recipient = Client::find($appointment->client_id)->user;

        $title = 'Reminder';
        $body = sprintf(self::TOMORROW_CLIENT_REMINDER, $appointment->business->name);

        $data = new NotificationMetadata(NotificationUrl::CLIENT_APPOINTMENT . $appointment->id, $appointment->id, null, $type, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Client reminder about soon appointment.
     *
     * @param Appointment $appointment
     */
    public function customerSoonMeetingReminder(Appointment $appointment)
    {
        $recipient = Client::find($appointment->client_id)->user;

        $title = 'Reminder';
        $body = sprintf(self::APPOINTMENT_CLIENT_REMINDER, $appointment->business->name);

        $data = new NotificationMetadata(NotificationUrl::CLIENT_APPOINTMENT . $appointment->id, $appointment->id, null, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify client about start walk.
     *
     * @param Appointment $appointment
     */
    public function walkStart(Appointment $appointment)
    {
        $title = "Walk started";
        $body = sprintf(self::WALK_START, $appointment->business->name);

        $recipient = Client::find($appointment->client_id)->user;

        $data = new NotificationMetadata(NotificationUrl::START_WALK, $appointment->id, null, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify client about end walk.
     *
     * @param Appointment $appointment
     */
    public function walkEnd(Appointment $appointment)
    {
        $title = "Walk ended";
        $body = sprintf(self::WALK_END, $appointment->business->name);

        $recipient = Client::find($appointment->client_id)->user;
        $data = new NotificationMetadata(NotificationUrl::END_WALK . $appointment->id, $appointment->id, null, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify provider about activation profile.
     *
     * @param int $businessId
     */
    public function profileActivate(int $businessId)
    {
        $title = "Profile Activated";
        $body = self::PROFILE_ACTIVATE;

        $recipient = Business::find($businessId)->user;
        $data = new NotificationMetadata(null, null, $businessId, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify provider about deactivation profile.
     *
     * @param int $businessId
     */
    public function profileSuspended(int $businessId)
    {
        $title = "Profile Suspended";
        $body = self::PROFILE_SUSPENDED;

        $recipient = Business::find($businessId)->user;
        $data = new NotificationMetadata(null, null, $businessId, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Decline appointment by client.
     *
     * @param Appointment $appointment
     */
    public function paymentDeclined(Appointment $appointment)
    {
        $sender = Client::find($appointment->client_id)->user;
        $recipient = Business::find($appointment->business_id)->user;

        $title = "Payment declined";
        $body = sprintf(self::PAYMENT_DECLINED, "$sender->first_name $sender->last_name", $appointment->id);

        $data = new NotificationMetadata(NotificationUrl::CLIENT_APPOINTMENT . $appointment->id, $appointment->id, $recipient->id, 1, true);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify client about updating appointment.
     *
     * @param Appointment $appointment
     */
    public function appointmentUpdated(Appointment $appointment)
    {
        $recipient = Client::find($appointment->client_id)->user;
        $sender = Business::find($appointment->business_id);

        $title = "Appointment updated";
        $body = sprintf(self::APPOINTMENT_UPDATED, $sender->name, $appointment->id);

        $data = new NotificationMetadata(NotificationUrl::CLIENT_APPOINTMENT . $appointment->id, $appointment->id, null, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify service provider about success payment.
     *
     * @param int $appointmentId
     */
    public function appointmentPaid(int $appointmentId)
    {
        $appointment = Appointment::find($appointmentId);
        $sender = Client::find($appointment->client_id)->user;
        $recipient = Business::find($appointment->business_id)->user;

        $title = "Appointment completed";
        $body = sprintf(self::APPOINTMENT_COMPLETED_SP, "$sender->first_name $sender->last_name", $appointment->id);

        $data = new NotificationMetadata(NotificationUrl::APPOINTMENT_COMPLETED . $appointment->id, $appointment->id, $recipient->id, 1, true);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify customer about success payment.
     *
     * @param int $appointmentId
     */
    public function appointmentCompleted(int $appointmentId)
    {
        $appointment = Appointment::find($appointmentId);
        $recipient = Client::find($appointment->client_id)->user;
        $sender = Business::find($appointment->business_id);

        $title = "Appointment completed";
        $body = sprintf(self::APPOINTMENT_COMPLETED_CUSTOMER, $appointmentId, $sender->name);

        $data = new NotificationMetadata(NotificationUrl::APPOINTMENT_COMPLETED . $appointment->id, $appointment->id, null, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify user about sharing pet profile for him.
     *
     * @param int $petId
     * @param int $recipientId
     */
    public function petProfileShared(int $petId, int $recipientId)
    {
        $recipient = User::find($recipientId);
        $sender = Pet::find($petId)->client->user;

        $title = "Pet Profile shared";
        $body = sprintf(self::PET_PROFILE_SHARED, "$sender->first_name $sender->last_name");

        $data = new NotificationMetadata(null, null, null, 1, true);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify client about treatment in one week.
     *
     * @param string $petName
     * @param string $treatmentName
     * @param User $recipient
     */
    public function treatmentFirstReminder(string $petName, string $treatmentName, User $recipient)
    {
        $title = "Reminder";
        $body = sprintf(self::TREATMENTS_FIRST_REMINDER, $petName, $treatmentName);

        $data = new NotificationMetadata(null, null, null, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

    /**
     * Notify client about treatment in two days.
     *
     * @param string $petName
     * @param string $treatmentName
     * @param User $recipient
     */
    public function treatmentSecondReminder(string $petName, string $treatmentName, User $recipient)
    {
        $title = "Reminder";
        $body = sprintf(self::TREATMENTS_SECOND_REMINDER, $petName, $treatmentName);

        $data = new NotificationMetadata(null, null, null, 1, false);

        $notification = $this->getNotification($title, $body);
        $this->send($notification, $recipient, $data);
    }

//    /**
//     * Send new notification.
//     *
//     * @param PayloadNotificationAlias $notification
//     * @param User $user
//     * @param array|null $data
//     * @param int $type
//     * @return mixed
//     */
//    private function sendNotification(PayloadNotificationAlias $notification, User $user, array $data = null)
//    {
//        $token = $user->deviceTokens->first()->token;
//
//        if($token == null) {
//            return;
//        }
//
//        if($data != null) {
//            $this->data = $this->addData($data);
//        }
//
//        $this->saveNotification($notification, $user->id, $data);
//
//        return FCM::sendTo($token, $this->options, $notification, $this->data);
//    }


    /**
     * Send new notification.
     *
     * @param PayloadNotificationAlias $notification
     * @param User $user
     * @param NotificationMetadata $data
     * @return mixed
     */
    private function send(PayloadNotificationAlias $notification, User $user, NotificationMetadata $data)
    {
        if(!$this->hasCorrectToken($user)) {
            return;
        }
        $token = $user->deviceTokens->first()->token;

        $metadata = $this->addData($data);
        $this->save($notification, $data, $user->id);

        return FCM::sendTo($token, $this->options, $notification, $metadata);
    }

    private function hasCorrectToken(User $user)
    {
        return $user != null && count($user->deviceTokens) != 0;
    }

    /**
     * Save notification to db.
     *
     * @param PayloadNotificationAlias $notification
     * @param NotificationMetadata $data
     * @param int $recipientId
     */
    private function save(PayloadNotificationAlias $notification, NotificationMetadata $data, int $recipientId)
    {
        $info = $notification->toArray();

        Notification::create([
            'title' => $info['title'],
            'body' => $info['body'],
            'type' => $data->type,
            'metadata' => $data,
            'appointment_id' => $data->appointmentId,
            'user_id' => $recipientId,
            'recipient_business_id' => $data->recipientBusinessId,
        ]);
    }

//    /**
//     * Save notification to db.
//     *
//     * @param PayloadNotificationAlias $notification
//     * @param int $userId
//     * @param array $data
//     * @param int $type
//     */
//    private function saveNotification(PayloadNotificationAlias $notification, int $type, array $data = null)
//    {
//        $info = $notification->toArray();
//
//        Notification::create([
//            'title' => $info['title'],
//            'body' => $info['body'],
//            'type' => $type,
//            'metadata' => $data,
//            'appointment_id' => isset($data['appointment_id']) ? $data['appointment_id'] : null,
//            //'user_id' => $userId
//        ]);
//    }

    /**
     * Generate notification.
     *
     * @param string $title
     * @param string $body
     * @return PayloadNotificationAlias
     */
    private function getNotification(string $title, string $body)
    {
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body);
        //$notificationBuilder->setIcon("http://simpleicon.com/wp-content/uploads/cute.png");
        return $notificationBuilder->build();
    }

    /**
     * Generate options.
     *
     * @return Options
     * @throws InvalidOptionsException
     */
    private function getOptions()
    {
        $optionBuilder = new OptionsBuilder();
        //$optionBuilder->setTimeToLive(self::TIME_TO_LIVE);
        return $optionBuilder->build();
    }

    /**
     * Generate data.
     */
    private function getData()
    {
        $dataBuilder = new PayloadDataBuilder();
        return $dataBuilder->build();
    }

    /**
     * Add internal metadata to notification.
     *
     * @param NotificationMetadata $data
     * @return PayloadData
     */
    private function addData(NotificationMetadata $data)
    {
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData((array)$data);
        return $dataBuilder->build();
    }
}
