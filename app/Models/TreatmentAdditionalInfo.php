<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreatmentAdditionalInfo extends Model
{
    /******* Properties *******/
    protected $fillable = [
        'description',
        'image',
        'treatment_id',
    ];

    /******* Accessors *******/
    public function getImageAttribute($value)
    {
        $getPath = config('constants.image_folder.treatment_photo.get_path');
        return url($getPath . $value);
    }
}
