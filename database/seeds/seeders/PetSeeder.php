<?php

use Illuminate\Database\Seeder;
use App\Models\Pet;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;

class PetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::fake();
        DatabaseSeeder::recreatingFolder(config('constants.image_folder.pets_photo.save_path'));
        factory(Pet::class, DatabaseSeeder::PET_COUNTS)->create();
    }
}
