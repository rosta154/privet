<?php

namespace App\Http\Controllers;

use App\Http\Requests\CityCreateRequest;
use App\Http\Requests\CitySearchRequest;
use App\Http\Requests\CityUpdateRequest;
use App\Models\City;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->ok(City::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CityCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CityCreateRequest $request)
    {
        $city = City::create($request->all());
        return $this->created($city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CityUpdateRequest $request
     * @param City $city
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CityUpdateRequest $request, City $city)
    {
        $city->update($request->all());
        return $this->ok();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param City $city
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(City $city)
    {
        $city->delete();
        return $this->deleted();
    }

    /**
     * Search city like query string.
     *
     * @param CitySearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(CitySearchRequest $request)
    {
        $cities = City::where('name', 'LIKE', "%$request->name%")->get();
        return $this->ok($cities);
    }
}
