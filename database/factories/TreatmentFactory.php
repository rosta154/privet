<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Treatment;
use Faker\Generator as Faker;

$factory->define(Treatment::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence($nbWords = 2, $variableNbWords = true),
        'min_price' => rand(50, 100),
        'max_price' => rand(101, 250),
        'business_type_id' => rand(1, 5),
        'main' => false
    ];
});
