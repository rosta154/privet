<?php


namespace App\Helpers\DTO;


class NotificationMetadata
{
    public $url;
    public $type;
    public $appointmentId;
    public $recipientBusinessId;
    public $isClientSender;

    public function __construct($url, $appointmentId, $recipientBusinessId, int $type, bool $isClientSender)
    {
        $this->url = $url;
        $this->appointmentId = $appointmentId;
        $this->recipientBusinessId = $recipientBusinessId;
        $this->type = $type;
        $this->isClientSender = $isClientSender;
    }
}
