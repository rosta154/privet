<?php

namespace App\Http\Controllers;

use App\Http\Requests\HotelRequest;
use App\Models\Hotel;
use Illuminate\Http\Request;

class HotelController extends Controller
{

    public function update(Hotel $hotel, Request $request)
    {
        foreach ($request->all() as $item)
        {
            $find = Hotel::find($item['id']);
            $find->update($item);

        }
        return $this->updated($find);
    }

    public function store(Request $request){
        foreach ($request->all() as $item){
            $hotel = new Hotel($item);
            $hotel->save();
        }
        return $this->created($hotel);
    }
}
