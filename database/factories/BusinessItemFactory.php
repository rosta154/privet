<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BusinessItem;
use Faker\Generator as Faker;

$factory->define(BusinessItem::class, function (Faker $faker) {
    return [
        'address' => $faker->address,
        'latitude' => $faker->latitude($min = 31.705791, $max = 32.919945),
        'longitude' => $faker->longitude($min = 34.643497, $max = 35.290146),
        'radius' => rand(5, 20)
    ];
});
