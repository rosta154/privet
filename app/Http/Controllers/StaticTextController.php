<?php

namespace App\Http\Controllers;

use App\Models\StaticText;
use Illuminate\Http\Request;

class StaticTextController extends Controller
{
    //TODO comments and return values.
  public function index()
  {
      return response()->json(['success' => true, 'data' => StaticText::all()]);
  }

  public function show(StaticText $staticText)
  {
      return response()->json(['success' => true,'data' => $staticText]);
  }

  public function store(Request $request)
  {
    $text = new StaticText($request->all());
    return response()->json(['success' => $text->save(),'data' => $text]);
  }

  public function update(StaticText $staticText,Request $request)
  {
      return response()->json(['success' => $staticText->update($request->all()),'data' => $staticText]);
  }

  public function destroy(StaticText $staticText)
  {
      return response()->json(['success' => $staticText->delete(),'message' => 'Static Text has been deleted']);
  }

}
