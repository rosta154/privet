<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\BusinessMainTreatments;
use App\Helpers\DTO\IncomeReport;
use App\Helpers\DTO\ProviderAppointmentReport;
use App\Http\Requests\ProviderAppointmentReportRequest;
use App\Models\Appointment;
use App\Models\Business;
use App\Models\Commission;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class ReportController extends Controller
{
    /**
     * Get provider report by appointment in time interval.
     *
     * @param ProviderAppointmentReportRequest $request
     * @return JsonResponse
     */
    public function getProviderAppointmentReport(ProviderAppointmentReportRequest $request)
    {
        $businessType = collect(User::getJWTUser()->businesses)
                ->where('id', $request->business_id)
                ->first()
                ->type_id;

        $mainTreatments = new BusinessMainTreatments($businessType);

        $appointments = Appointment::where([
            'business_id' => $request->business_id,
            'payment_status' => Appointment::PAID_PAYMENT_STATUS,
        ])
            ->whereBetween('date', [
                Carbon::parse($request->start_date)->startOfDay(),
                Carbon::parse($request->end_date)->endOfDay()])
            ->get();

        $commission = $this->getCurrentBusinessCommission($request->business_id);

        $receptions = new ProviderAppointmentReport(
            $appointments->where('type_id', $mainTreatments->receptionId)->values(),
            $commission
        );

        $trips = new ProviderAppointmentReport(
            $appointments->where('type_id', $mainTreatments->tripsId)->values(),
            $commission
        );
        return $this->ok(new IncomeReport($receptions, $trips));
    }

    /**
     * Get current business commission.
     *
     * @param int $businessId
     * @return Commission
     */
    private function getCurrentBusinessCommission(int $businessId) : Commission
    {
        return Business::find($businessId)
            ->load(['businessCommission', 'type.businessCommission'])
            ->getCurrentCommissions()
            ->currentCommission;
    }
}
