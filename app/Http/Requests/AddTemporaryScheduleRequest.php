<?php

namespace App\Http\Requests;

use App\Models\BusinessItem;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class AddTemporaryScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $businesses = collect(User::getJWTUser()->businesses)->pluck('id');
        $items = BusinessItem::whereIn('business_id', $businesses)->get();
        $usedItems = collect(request()->temporary_schedules)->pluck('business_item_id');

        return $items->whereIn('id', $usedItems)->count() == $usedItems->count();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "temporary_schedules" => 'required|array',
            "temporary_schedules.*.business_item_id" => 'required|exists:business_items,id',
            "temporary_schedules.*.schedules" => 'required|array',
            "temporary_schedules.*.schedules.*.start_date" => 'required|date_format:Y-m-d',
            "temporary_schedules.*.schedules.*.end_date" => 'required|date_format:Y-m-d|after:start_date',
            "temporary_schedules.*.schedules.*.opening_time" => 'required|date_format:H:i',
            "temporary_schedules.*.schedules.*.closing_time" => 'required|date_format:H:i',
            "temporary_schedules.*.schedules.*.available" => 'required|boolean',
        ];
    }
}
