<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int amount
 * @property int type
 */
class CashBackCondition extends Model
{
    public const DEFAULT_LIFE_TIME = 180; // half a year

    public const FIXED_TYPE = 1;
    public const PERCENT_TYPE = 2;

    protected $fillable = [
        'business_type_id',
        'business_id',
        'type',
        'amount',
        'life_time',
    ];

    /**
     * Add business or business type cash back rule.
     *
     * @param array $data
     */
    public static function add(array $data)
    {
        if(isset($data['business_type_id'])) {
            $condition = self::where('business_type_id', $data['business_type_id'])->first();
            if($condition) {
                $condition->update($data);
            } else {
                self::create($data);
            }
        }

        if(isset($data['business_id'])) {
            $condition = self::where('business_id', $data['business_id'])->first();
            if($condition != null) {
                $condition->update($data);
            } else {
                self::create($data);
            }
        }
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function (CashBackCondition $condition) {
            if(!$condition->life_time) {
                $condition->life_time = self::DEFAULT_LIFE_TIME;
            }
        });
    }
}
