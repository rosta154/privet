<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AppointmentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO validation
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $timezone = 2;
        return [
            'price' => 'numeric',
            'date' => 'date_format:Y-m-d H:i|after:' . Carbon::now()->addHour($timezone),
            'comment' => 'string|max:255',
            'business_id' => 'exists:businesses,id',
            'business_item_id' => 'current_business_item',
            'type_id' => 'is_mane_treatment',
            'treatments' => '|array',
                //'treatments.*.pet_id' => '|current_user_pet',
                'treatments.*.services' => '|array',
                'treatments.*.services.*.id' => '|current_business_service',
                'treatments.*.services.*.quantity' => '|numeric',
        ];
    }
}
