<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Business;
use Faker\Generator as Faker;
use Faker\Provider\en_ZA\Person;

$factory->define(Business::class, function (Faker $faker) {
    $faker->addProvider(new Person($faker));

    return [
        'name' => $faker->company,
        'address' => $faker->address,
        'latitude' => $faker->latitude($min = 31.705791, $max = 32.919945),
        'longitude' => $faker->longitude($min = 34.643497, $max = 35.290146),
        'license' => $faker->idNumber,
        'website' => $faker->domainName,
        'about' => $faker->text,
        'user_id' => rand(1, DatabaseSeeder::USERS_COUNT),
        'type_id' => rand(1,6),
    ];
});
