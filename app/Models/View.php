<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $fillable = [
        'business_id',
        'date',
        'quantity',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Increment views for current business.
     *
     * @param int $businessId
     */
    public static function addNewView(int $businessId)
    {
        if(!collect(User::getJWTUser()->businesses)->pluck('id')->contains($businessId)) {
            $date = Carbon::now()->toDateString();
            $view = self::where(['date' => $date, 'business_id' => $businessId])->first();

            if(!$view) {
                $view = self::create(['date' => $date, 'business_id' => $businessId, 'quantity' => 1]);
            } else {
                $view->quantity++;
                $view->save();
            }
        }
    }
}
