<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusinessItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //TODO validate item for this business and id
        return [
            'items' => 'required|array',
                'items.*.city_id' => 'numeric|exists:cities,id',
                'items.*.main' => 'required|boolean',
//                'items.*.address' => 'required_if:items.*.main,false|max:255',
//                'items.*.latitude' => 'required_if:items.*.main,false|numeric',
//                'items.*.longitude' => 'required_if:items.*.main,false|numeric',
//                'items.*.radius' => 'required_if:items.*.main,false|numeric',
                'items.*.schedule' => 'required|array',
                    'items.*.schedule.*.week_day' => 'required|numeric',
                    'items.*.schedule.*.opening_time' => 'required|date_format:H:i',
                    'items.*.schedule.*.closing_time' => 'required|date_format:H:i|after:items.*.schedule.*.opening_time',
                    'items.*.schedule.*.rule' => 'boolean',
                    'items.*.schedule.*.available' => 'boolean|required_if:items.*.schedule.*.rule,true',

        ];
    }
}
