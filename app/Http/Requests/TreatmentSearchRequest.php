<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_type' => 'exists:business_types,id|not_a_store',
            'main' => 'required|boolean',
        ];
    }

    public function messages(){
        return [
            'business_type.not_a_store' => 'There are no services for walker and shop!'
        ];
    }
}
