<?php

namespace App\Helpers\DTO;



class ShortSchedule
{
    //public $week_day;
    public $opening_time;
    public $closing_time;
    public $rule;
    public $available;
    public $updated_at;

    public function __construct(/*int $weekDay,*/ string $startTime, string $endTime, bool $rule, bool $available, string $updated)
    {
        //$this->week_day = $weekDay;
        $this->opening_time = $startTime;
        $this->closing_time = $endTime;
        $this->rule = $rule;
        $this->available = $available;
        $this->updated_at = $updated;
    }
}
