<?php

use Illuminate\Support\Facades\Route;

Route::post('admin/login','UserController@adminLogin');

Route::group(['middleware' => ['jwt.auth', 'admin']], function () {
    Route::post('questions','QuestionController@store');
    Route::put('questions/{question}','QuestionController@update');
    Route::delete('questions/{question}','QuestionController@destroy');

    Route::post('category-products','CategoryProductController@store');
    Route::put('category-products/{product}','CategoryProductController@update');
    Route::delete('category-products/{product}','CategoryProductController@destroy');

//    Route::post('business-types','BusinessTypeController@store');
//    Route::put('business-types/{type}','BusinessTypeController@update');
    Route::delete('business-types/{type}','BusinessTypeController@destroy');

    Route::post('faqs','FaqController@store');
    Route::put('faqs/{faq}','FaqController@update');
    Route::delete('faqs/{faq}','FaqController@destroy');

    Route::get('all-pets','PetController@all');


    Route::post('breeds','BreedController@store');
    Route::put('breeds/{breed}','BreedController@update');
    Route::delete('breeds/{breed}','BreedController@destroy');

    Route::post('types','PetTypeController@store');
    Route::put('types/{petType}','PetTypeController@update');
    Route::delete('types/{petType}','PetTypeController@destroy');

    Route::post('static-texts','StaticTextController@store');
    Route::put('static-texts/{staticText}','StaticTextController@update');
    Route::delete('static-texts/{staticText}','StaticTextController@destroy');

    Route::post('subjects','SubjectController@store');
    Route::put('subjects/{subject}','SubjectController@update');
    Route::delete('subjects/{subject}','SubjectController@delete');

    Route::get('contact-us', 'ContactUsController@index');
    Route::get('contact-us/{userId}', 'ContactUsController@show');


    Route::post('contents/{content}','ContentController@update');
    Route::delete('contents/{content}','ContentController@destroy');
    Route::post('contents','ContentController@store');


    Route::post('treatments', 'TreatmentController@store');
    Route::put('treatments/{treatment}', 'TreatmentController@update');
    Route::delete('treatments/{treatment}', 'TreatmentController@destroy');

    Route::post('clients', 'ClientController@store');
    Route::put('clients/{client}', 'ClientController@update');
    Route::delete('clients/{client}', 'ClientController@destroy');

    Route::post('cities','CityController@store');
    Route::put('cities/{city}','CityController@update');
    Route::delete('cities/{city}','CityController@destroy');
    Route::get('cities','CityController@index');

    Route::get('businesses-rating','AdminPanelController@avgProvidersRatings');
    Route::get('businesses-graphics','AdminPanelController@businessesGraphic');
    Route::get('businesses-list/{typeType}','AdminPanelController@businessesList');


    Route::get('user-amount','AdminPanelController@amountOfUsers');
    Route::post('admin-create','AdminPanelController@createUser');
    Route::post('admin-update/{user}','AdminPanelController@adminUpdate');
    Route::get('total-appointments','AdminPanelController@totalAppointment');
    Route::get('appointments-graphics','AdminPanelController@appointmentGraphic');

    Route::get('admin-all','AdminPanelController@adminAll');
    Route::get('pets-graphics','AdminPanelController@petGraphic');
    Route::get('dashboard/multi-statistic','AdminPanelController@multiStatistic');

    Route::get('amount-of-treatments','AdminPanelController@amountOfTreatments');
    Route::get('amount-of-users','AdminPanelController@amountOfUser');

    Route::get('appointments-list','AdminPanelController@appointments');

    Route::get('appointments-list/{appointment}','AdminPanelController@appointment');

    Route::get('appointments-client/{client}','AdminPanelController@appointmentClient');
    Route::get('contact-us-count','AdminPanelController@countContactUs');
    Route::get('comments-all','AdminPanelController@commentsAll');


    Route::get('clients','AdminPanelController@getClients');
    Route::get('client/{client}','AdminPanelController@getClient');

    Route::get('client-chat/{client}','AdminPanelController@clientByChat');
    Route::get('client-comment/{client}','AdminPanelController@commentClient');
    Route::get('comments','AdminPanelController@commentAll');

    Route::get('appointment-businesses','AdminPanelController@appointmentBusinesses');
    Route::get('businesses-all','AdminPanelController@businesses');
    Route::post('businesses/mpl/{business}','AdminPanelController@addPayMeMpl');
    Route::get('businessId/{businessId}','AdminPanelController@businessById');
    Route::get('appointment-by-business/{businessId}','AdminPanelController@appointmentByBusiness');
    Route::post('search-clients','AdminPanelController@searchClient');
    Route::get('appointment-tickets','AdminPanelController@appointmentTicket');
    Route::get('appointment-history/{appointmentId}','AdminPanelController@history');

    Route::post('send-notifications','AdminPanelController@sendNotifications');
    Route::get('admin/pet-types','AdminPanelController@indexPetTypes');

    Route::get('comment/{appointmentId}','AdminPanelController@comment');

    Route::post('subscription','AdminPanelController@subscription');

    Route::post('cash-back/business','CashBackController@setCondition');
    Route::get('cash-back/business','CashBackController@getBusinessTypeConditions');
    Route::post('cash-back/client','CashBackController@setCashBack');
    Route::get('cash-back/client/{clientId}','CashBackController@getBonuses');
    Route::delete('cash-back/{condition}/business','CashBackController@deleteCondition');
    Route::delete('cash-back/{cashBack}/client','CashBackController@deleteCashBack');

    Route::post('commissions','AdminPanelController@commission');
    Route::put('commissions','CommissionController@set');
    Route::get('commissions','CommissionController@index');
    Route::get('commissions/type','CommissionController@getTypeCommission');
    Route::get('businesses/{business}/commission','CommissionController@show');
    Route::delete('commissions/{commission}','CommissionController@destroy');
});

