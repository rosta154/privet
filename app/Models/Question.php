<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $fillable = [
      'name','percent'
    ];

    public function rating()
    {
        return $this->hasMany(Rating::class);
    }
}
