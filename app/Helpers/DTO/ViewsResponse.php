<?php


namespace App\Helpers\DTO;


use Illuminate\Support\Collection;

class ViewsResponse
{
    public $views;
    public $totalClicks;

    public function __construct(Collection $views)
    {
        $this->views = $views;
        $this->totalClicks = $views->sum('quantity');
    }
}
