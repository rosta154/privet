<?php


namespace App\Http\Traits;


use App\Helpers\Interfaces\IFileManager;
use App\Models\Barbershop;
use App\Models\Business;
use App\Models\BusinessImage;
use App\Models\BusinessItem;
use App\Models\BusinessType;
use App\Models\Hotel;
use App\Models\Schedule;
use App\Models\Trainer;
use App\Models\Vet;
use App\Models\Walker;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

trait BusinessLogic
{
    /**
     * Save new business'es services.
     *
     * @param array $services
     * @param int $businessId
     * @param int $typeId
     */
    public function addServices(array $services, int $businessId, int $typeId)
    {
        //TODO add min/max price logic
        switch ($typeId) {
            case BusinessType::VETERINARIAN;
                $this->addVet($services, $businessId);
                break;
            case BusinessType::TRAINER:
                $this->addTrainer($services, $businessId);
                break;
            case BusinessType::HOTEL:
                $this->addHotel($services, $businessId);
                break;
            case BusinessType::BARBERSHOP:
                $this->addBarberShop($services, $businessId);
                break;
            case BusinessType::WALKER:
                $this->addWalker($services, $businessId);
                break;
        }
    }

    /**
     * @param array $services
     * @param int $businessId
     */
    // TODO replace one
    public function addVet(array $services, int $businessId)
    {
        foreach ($services as $element) {
            Vet::create($element + ['business_id' => $businessId]);
        }
    }

    public function addTrainer(array $services, int $businessId)
    {
        foreach ($services as $element) {
            Trainer::create($element + ['business_id' => $businessId]);
        }
    }

    public function addBarberShop(array $services, int $businessId)
    {
        foreach ($services as $element) {
            Barbershop::create($element + ['business_id' => $businessId]);
        }
    }
    public function addWalker(array $services, int $businessId)
    {
        foreach ($services as $element) {
            Walker::create($element + ['business_id' => $businessId]);
        }
    }

    public function addHotel(array $services, int $businessId)
    {
        foreach ($services as $element) {
            Hotel::create($element + ['business_id' => $businessId]);
        }
    }

    /**
     * Create business items.
     *
     * @param array $items
     * @param Business $business
     */
    public function addBusinessItems(array $items, Business $business)
    {
        foreach ($items as $item) {
            if(!isset($item['longitude']) || !isset($item['latitude'])) {
                $item['longitude'] = $business->longitude;
                $item['latitude'] = $business->latitude;
            }
            $businessItem = BusinessItem::create($item + ['business_id' => $business->id]);
            $this->addSchedule($item['schedule'], $businessItem->id);

            if(isset($item['cities'])) {
                $this->addCities($businessItem, $item['cities']);
            }
        }
    }

    /**
     * Add city relation to business item.
     *
     * @param BusinessItem $businessItem
     * @param array $cities
     */
    private function addCities(BusinessItem $businessItem, array $cities)
    {
        $businessItem->cities()->sync($cities, true);
    }

    /**
     * Update business's items.
     *
     * @param Collection $items
     */
    public function updateBusinessItems(Collection $items)
    {
        $updatedItems = $items->where('id', '!=', null)->all();
        $this->changeBusinessItems(collect($updatedItems));

        $deleted = $this->businessItems->pluck('id')->diff(collect($updatedItems)->pluck('id'));
        $this->deleteBusinessItems($deleted);

        $this->addBusinessItems($items->where('id', null)->toArray(), $this->id);

    }

    /**
     * Change business item info and schedule.
     *
     * @param Collection $items
     */
    public function changeBusinessItems(Collection $items)
    {
        foreach ($items as $updatedItem) {
            $oldItem = BusinessItem::find($updatedItem['id']);
            $oldItem->update($updatedItem);
            $this->updateSchedule($updatedItem['schedule'], $oldItem->id);

            if(isset($updatedItem['cities'])) {
                $this->addCities($oldItem, $updatedItem['cities']);
            }
        }
    }

    /**
     * Delete old business items.
     *
     * @param Collection $itemsIds
     */
    public function deleteBusinessItems(Collection $itemsIds)
    {
        foreach ($itemsIds as $item) {
            $deletedItem = BusinessItem::find($item)->load('schedule');
            Schedule::destroy($deletedItem->schedule->pluck('id'));
            $deletedItem->delete();
        }
    }

    /**
     * Add new store and schedule.
     *
     * @param array $schedule
     * @param int $businessId
     */
    public function addStore(array $schedule, int $businessId)
    {
        $businessItem = BusinessItem::create(['business_id' => $businessId]);
        $this->addSchedule($schedule, $businessItem->id);
    }

    /**
     * Update store's schedule.
     *
     * @param array $data
     */
    public function updateStore(array $data)
    {
        $this->update($data);
        $this->saveImages($data, resolve(IFileManager::class));
        $this->updateSchedule($data['schedule'], $this->businessItems->first()->id);
    }

    /**
     * Add schedule for business.
     *
     * @param array $schedules
     * @param int $businessItemId
     */
    public function addSchedule(array $schedules, int $businessItemId)
    {
        foreach ($schedules as $item) {
            $item['business_item_id'] = $businessItemId;
            $schedule = Schedule::create($item);

            if($schedule->rule && isset($item['cities'])) {
                $schedule->cities()->attach($item['cities']);
            }
        }
    }

    /**
     * Update business item's schedule.
     *
     * @param array $schedule
     * @param int $businessItemId
     */
    public function updateSchedule(array $schedule, int $businessItemId)
    {
        $scheduleCollection = collect($schedule);

        $updatedSchedules = $scheduleCollection->where('id', '!=', null)->all();
        foreach ($updatedSchedules as $updatedSchedule) {
            $schedule = Schedule::find($updatedSchedule['id']);
            if($schedule != null) {
                $schedule->update($updatedSchedule);

                if($schedule->rule && isset($updatedSchedule['cities'])) {
                    $schedule->cities()->sync($updatedSchedule['cities']);
                }
            }
        }

        $oldSchedules = BusinessItem::find($businessItemId)->load('schedule')->schedule;
        $deleted = $oldSchedules->pluck('id')->diff(collect($updatedSchedules)->pluck('id'));
        Schedule::destroy($deleted);

        $newSchedules = $scheduleCollection->where('id',null)->all();
        $this->addSchedule($newSchedules, $businessItemId);
    }

    /**
     * Save all business images.
     *
     * @param array $data
     * @param IFileManager $fileManager
     * @return void
     */
    public function saveImages(array $data, IFileManager $fileManager)
    {
        $path = config('constants.image_folder.business_photo.save_path');
        if(Arr::has($data,'deleted_images')){
           foreach ($data['deleted_images'] as $imageId){
               $businessImage = BusinessImage::find($imageId);
               $fileManager->deleteFile($businessImage->getOriginal('photo'), $path);
               $businessImage->delete();
           }
        }
        if (Arr::has($data, 'main_image')) {
            $file = $data['main_image'];
            $imageName = $fileManager->saveBase64($file, $path);
            $this->addBusinessImage($imageName, true);
        }

        if (Arr::has($data, 'images')) {
            foreach ($data['images'] as $image) {
                $imageName = $fileManager->saveBase64($image, $path);
                $this->addBusinessImage($imageName);
            }
        }
    }

    /**
     * Add new business image to db.
     *
     * @param string $name
     * @param boolean $main
     * @return void
     */
    private function addBusinessImage(string $name, bool $main = false)
    {
        BusinessImage::create([
            'photo' => $name,
            'main' => $main,
            'business_id' => $this->id
        ]);
    }

}
