<?php

namespace App\Http\Controllers;

use App\Http\Requests\BarbershopRequest;
use App\Models\Barbershop;
use Illuminate\Http\Request;

class BarbershopController extends Controller
{

    public function update(Request $request)
    {
        foreach ($request->all() as $item)
        {
            $find = Barbershop::find($item['id']);
            $find->update($item);

        }
        return $this->updated($find);
    }

    public function store(Request $request){
        foreach ($request->all() as $item){
            $barber = new Barbershop($item);
            $barber->save();
        }
        return $this->created($barber);
    }

}
