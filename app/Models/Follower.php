<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int sender_id
 * @property string phone
 * @property boolean accepted
 */
class Follower extends Model
{
    public const FOLLOWER_EXISTS_ERROR = "This user already registered.";
    public const FOLLOWER_INVITATION_MESSAGE = " invites to download the application ";

    protected $fillable = [
        'phone',
    ];

    protected $guarded = [
        'sender_id',
        'accepted',
    ];

    /**
     * Find entity or add new.
     *
     * @param string $phone
     * @throws Exception
     */
    public static function findOrAdd(string $phone)
    {
        $follower = self::where('phone', $phone)->first();
        if($follower) {
            throw new Exception(self::FOLLOWER_EXISTS_ERROR);
        }
        self::create(['phone' => $phone]);
    }

    /**
     * Override base boot method.
     */
    public static function boot() {
        parent::boot();

        // Before create.
        static::creating(function (Follower $follower) {
            $senderId = User::getJWTUser()->id;
            $follower->sender_id = $senderId;
        });

        // After create.
        static::created(function (Follower $follower) {
            // TODO sent sms.
        });

        // Before update.
        static::updating(function (Follower $follower) {
            $follower->accepted = true;
            // TODO sent notification for sender.
        });
    }
}
