<?php


namespace App\Http\Traits;


use App\Helpers\Interfaces\IFileManager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
trait PetLogic
{
    /**
     * Update pet photo.
     *
     * @param string $base64Image
     */
    public function updatePhoto(string $base64Image)
    {
        if ($base64Image) {
            $this->deletePhoto();
            $this->addPhoto($base64Image);
        }
    }

    /**
     * Add new pet's photo.
     *
     * @param string $base64Image
     */
    public function addPhoto(string $base64Image)
    {
        $fileManager = $this->getFileManager();
        $path = $this->getFilePath();
        $this->photo = $fileManager->saveBase64($base64Image, $path);
    }

    /**
     * Delete pet's photo.
     */
    public function deletePhoto()
    {
        $fileManager = $this->getFileManager();
        $path = $this->getFilePath();
        $fileManager->deleteFile($this->getOriginal('photo'), $path);
    }

    /**
     * Get path to avatars.
     *
     * @return string
     */
    private function getFilePath()
    {
        return config('constants.image_folder.pets_photo.save_path');
    }

    /**
     * Resolve FileManager.
     *
     * @return IFileManager
     */
    private function getFileManager()
    {
        return resolve(IFileManager::class);
    }

    /**
     * @param $name
     * @param $path
     */

    public function deletePhoto64($name, $path){
        Storage::disk('local')->delete($path.$name);
    }
}
