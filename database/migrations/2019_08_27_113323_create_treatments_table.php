<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->float('min_price');
            $table->float('max_price');
            $table->boolean('main')->default(false);
            $table->unsignedBigInteger('business_type_id');
            $table->unsignedBigInteger('pet_type_id')->nullable();

            $table
                ->foreign('business_type_id')
                ->references('id')
                ->on('business_types')
                ->onDelete('cascade');

            $table
                ->foreign('pet_type_id')
                ->references('id')
                ->on('pet_types')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatments');
    }
}
