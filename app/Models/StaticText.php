<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticText extends Model
{
    protected $fillable = [
        'title',
        'name',
        'field_1',
        'field_2',
        'field_3',
        'field_4',
        'field_5',
        'field_6',
    ];
}
