<?php


namespace App\Providers\Validators;


use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class IsSundayValidator extends ServiceProvider
{
    private const SUNDAY = 0;

    public function boot()
    {
        $this->app['validator']->extend('sunday', function ($attribute, $value, $parameters)
        {
            return Carbon::parse($value)->dayOfWeek == self::SUNDAY;
        });
    }
}
