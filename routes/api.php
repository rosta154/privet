<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('contents','ContentController@index');
Route::get('contents/{content}','ContentController@show');

Route::post('result/{invoice}','PayController@payResult');
//TODO this request must be authorized )))
Route::get('invoice/{appointment}','PayController@generateInvoice');

/***REGISTRATION***/

Route::post('verify', 'Auth\LoginController@verifyPhone');
Route::post('login', 'Auth\LoginController@login');

/***END-REGISTRATION***/

Route::group(['middleware' => ['jwt.auth']], function () {

    Route::post('invite','InviteController@sendInvite');
    Route::get('invite','InviteController@getInvite');
    Route::delete('invite/{inv_id}','InviteController@deleteInvite');
    Route::post('chat-appointment','AdminPanelController@chatAppointment');
    Route::put('logout', 'Auth\LoginController@logOut');

    Route::get('user/profile','UserController@profile');

    Route::post('user/update','UserController@update');
    Route::post('user/invite','UserController@addFollower');

    /*** Breeds ***/
    Route::get('breeds','BreedController@index');
    Route::get('breeds/{breed}','BreedController@show');
    Route::get('breeds/{breed}','BreedController@show');

    /*** End Breeds ***/

    // TODO roles

    Route::get('clients', 'ClientController@index');
    Route::get('clients/{client}', 'ClientController@show');


    Route::get('treatments/{treatment}', 'TreatmentController@show');

    /*** Pet Type ***/

    Route::get('pet-types/{petType}','PetTypeController@show');
    Route::get('pet-types/{petType}/breeds','PetTypeController@breeds');
    /*** End Pet Type ***/

    /*** Pet ***/
    Route::get('pets','PetController@index');
    Route::get('pets-all','PetController@all');
    Route::post('pets','PetController@store');

    Route::group(['middleware' => ['pet_owner']], function ()
    {
        Route::delete('pets/{pet}', 'PetController@destroy');
        Route::post('pets/{pet}', 'PetController@update');
    });

    Route::get('pets/{pet}','PetController@show');
    /*** End Pet***/

    Route::get('faqs/{faq}','FaqController@show');

    Route::get('static-texts','StaticTextController@index');
    Route::get('static-texts/{staticText}','StaticTextController@show');

    Route::get('business-types', 'BusinessTypeController@index');
    Route::get('business-types/{type}', 'BusinessTypeController@treatments');

    Route::get('businesses/name-search','BusinessController@searchByName');
    Route::get('businesses/income-report','ReportController@getProviderAppointmentReport');
    Route::post('businesses/treatment-search','BusinessController@searchByTreatment');
    Route::get('businesses','BusinessController@index');
    Route::get('businesses/{business}','BusinessController@show');
    Route::get('businesses/{business}/customer-calendar','BusinessController@getCustomerCalendar');
    Route::get('businesses/{business}/provider-calendar','BusinessController@getProviderCalendar');
    Route::post('businesses','BusinessController@store');
    Route::post('businesses/{business}','BusinessController@update');
    Route::put('businesses/{business}/services','BusinessController@updateServices');
    Route::put('businesses/{business}/items','BusinessItemController@update');
    Route::delete('businesses/{business}','BusinessController@destroy');

    Route::delete('businesses/images/{image}', 'BusinessController@destroyImage');
    Route::get('businesses/treatments/{business}', 'BusinessController@businessTreatments');

    Route::get('appointments/client','AppointmentController@clientAppointments');
    Route::get('appointments/business/{businessId}','AppointmentController@businessAppointments');
    Route::get('appointments/{appointment}','AppointmentController@show');
    Route::get('appointments/nearest/{businessId}','AppointmentController@businessNearest');
    Route::get('appointments-client/nearest','AppointmentController@businessNearestClient');
    Route::post('appointments','AppointmentController@store');
    Route::post('appointments/search','AppointmentController@searchUser');

    Route::post('appointments/{appointment}','AppointmentController@changeStatus');
    Route::post('appointments/{appointment}/notify','AppointmentController@notifyAdmin');

    Route::put('appointments/{appointment}','AppointmentController@update');
    Route::delete('appointments/{appointment}','AppointmentController@destroy');

    Route::get('category-products','CategoryProductController@index');
    Route::get('category-products/{product}','CategoryProductController@show');

    Route::get('products','ProductController@index');
    Route::get('products/{product}','ProductController@show');
    Route::post('products','ProductController@store');
    Route::put('products','ProductController@update');
    Route::delete('products/{product}','ProductController@destroy');

    Route::post('contact-us', 'ContactUsController@store');

    Route::get('stores/{store}/views','StoreController@getViewsCollection');

    Route::get('stores/{store}','StoreController@show');
    Route::post('stores','StoreController@store');
    Route::put('stores/{store}','StoreController@update');
//    Route::delete('stores/{store}','StoreController@destroy');

    Route::get('hotels','HotelController@index');
    Route::get('hotels/{hotel}','HotelController@show');

    Route::post('hotels','HotelController@store');
    Route::post('hotels/update','HotelController@update');

    Route::delete('hotels/{hotel}','HotelController@destroy');

    Route::post('vets/update','VetController@update');
    Route::post('vets/create','VetController@store');

    Route::post('trainers','TrainerController@store');
    Route::post('trainers/update','TrainerController@update');

    Route::post('walkers','WalkerController@store');
    Route::post('walkers/update','WalkerController@update');

    Route::post('barbershops','BarbershopController@store');
    Route::post('barbershops/update','BarbershopController@update');


    Route::get('stores','StoreController@index');

    Route::get('questions','QuestionController@index');
    Route::get('questions/{question}','QuestionController@show');

    Route::get('ratings','RatingController@index');
    Route::post('ratings','RatingController@store');
    Route::put('ratings/{rating}','RatingController@update');
    Route::get('ratings/{rating}','RatingController@show');
    Route::delete('ratings/{rating}','RatingController@destroy');

    /* Business Items  */
    Route::post('business-items/schedule','BusinessItemController@addTemporarySchedule');
    Route::put('business-items/schedule','BusinessItemController@deleteTemporarySchedule');

    Route::get('cities-search','CityController@search');

    Route::get('treatment-history/{pet}','PetController@historyTreatment');

    Route::get('history-treatment/{pet}','PetController@newHistoryTreatment');

    Route::post('notifications/{appointment}','NotificationController@walkStart');
    Route::put('notifications/{appointment}','NotificationController@walkEnd');

    Route::get('notifications-count/clients/{userId}','NotificationController@countNotifyClient');
    Route::get('notifications-count/providers/{businessId}','NotificationController@countNotifyProvider');

    Route::get('notifications-client/{clientId}','NotificationController@clientNotifications');
    Route::get('notifications-provider/{providerId}','NotificationController@providerNotifications');
    Route::post('notification-read/{notification}','NotificationController@changeRead');
    Route::get('chat-show/{appointment}','AdminPanelController@showChat');

    Route::post('seller/{business}','PayController@addSeller');

    Route::post('search-vets','VetController@search');


    Route::post('appointment-chat','ChatController@store');
    Route::get('show-chat/{appointment}','ChatController@showChat');
});

Route::post('test', 'TestController@test');

Route::get('pet-types','PetTypeController@index');
Route::get('treatments', 'TreatmentController@index');
Route::get('treatments/{treatment}', 'TreatmentController@show');
Route::get('subjects','SubjectController@index');

Route::get('search/treatments', 'TreatmentController@search');
Route::get('faqs','FaqController@index');
Route::get('static-texts/{staticText}','StaticTextController@show');