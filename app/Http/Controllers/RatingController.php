<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Models\Appointment;
use App\Models\Business;
use App\Models\BusinessRating;
use App\Models\Rating;

class RatingController extends Controller
{


    //TODO add function description to comment.
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->ok(Rating::all());
    }

    public function store(RatingRequest $request)
    {
        $rating = new Rating($request->all());
        $rating->save();
        $avg = Rating::where('appointment_id',$request->appointment_id)->pluck('rating')->avg();
         $appointment = Appointment::find($rating->appointment_id);
        $amount_user = Rating::where('appointment_id',$request->appointment_id)->count()/3;

        $business = Business::where('id',$appointment->business_id)->first();
        $business->count_user = $amount_user;
        $business->rating = $avg;
        $business->save();
        $appointment->rating = $avg;
        $appointment->save();
        return $this->created($rating);
    }

    /**
     * @param RatingRequest $request
     * @param Rating $rating
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RatingRequest $request, Rating $rating)
    {
        $rating->update($request->all());
        return $this->updated($rating);
    }

    /**
     * @param Rating $rating
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Rating $rating)
    {
        return $this->ok($rating);
    }

    /**
     * @param Rating $rating
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Rating $rating)
    {
        return $this->deleted($rating->delete());
    }

}
