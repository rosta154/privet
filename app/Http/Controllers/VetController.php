<?php

namespace App\Http\Controllers;

use App\Http\Requests\VetRequest;
use App\Models\Vet;
use Illuminate\Http\Request;

class VetController extends Controller
{
    public function update(Vet $vet, Request $request)
    {
        foreach ($request->all() as $item)
        {
            $find = Vet::find($item['id']);
            $find->update($item);

        }
        return $this->updated($find);
    }

    public function store(Request $request){
        foreach ($request->all() as $item){
            $vet = new Vet($item);
            $vet->save();
        }
        return $this->created($vet);
    }

    public function search(Request $request)
    {
     $vet = Vet::where('treatment_id',$request->treatment_id)->where('business_id',$request->business_id)->get();
   return $this->ok($vet);
    }
}
