<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'rating',
        'appointment_id',
        'created_by',
        'question_id',
    ];

    public static function boot() {
        parent::boot();

        static::creating(function (Rating $rating) {
            $rating->created_by =  User::getJWTUser()->id;
        });
    }


    public function appointments()
    {
        return $this->belongsTo(Appointment::class);
    }

    public function questions()
    {
        return $this->belongsTo(Question::class);
    }
}

