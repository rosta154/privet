<?php

use Illuminate\Database\Seeder;
use App\Models\Breed;
class BreedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $breed = new Breed();
        $breed->name = 'Shepherd dog';
        $breed->pet_type_id = 1;
        $breed->save();

        $breed = new Breed();
        $breed->name = 'Bulldog';
        $breed->pet_type_id = 1;
        $breed->save();

        $breed = new Breed();
        $breed->name = 'Flap-eared';
        $breed->pet_type_id = 2;
        $breed->save();

        $breed = new Breed();
        $breed->name = 'British';
        $breed->pet_type_id = 2;
        $breed->save();

        $breed = new Breed();
        $breed->name = 'Mustang';
        $breed->pet_type_id = 3;
        $breed->save();

        $breed = new Breed();
        $breed->name = 'Budgerigar';
        $breed->pet_type_id = 4;
        $breed->save();

        $breed = new Breed();
        $breed->name = 'Miniature pig';
        $breed->pet_type_id = 5;
        $breed->save();


    }
}
