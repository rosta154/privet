<?php

namespace App\Http\Controllers;

use App\Http\Requests\InviteRequest;
use App\Models\Invite;
use Illuminate\Http\Request;
use App\Models\User;
class InviteController extends Controller
{
    public function sendInvite(InviteRequest $request)
    {
     $user = auth()->user();
     $invite = new Invite($request->all());
     if(User::where('phone',$request->phone)->exists()){
         return response()->json(['success' => false,'message' => 'User is registered']);
     }
     $invite->sender_id = $user->id;
     $invite->user_status = 1;
     return response()->json(['success' => $invite->save(),'data' => $invite]);

    }
    public function getInvite()
    {
        $user = auth()->user();
        $invite = Invite::where('sender_id',$user->id)->get();
        return response()->json(['success' => true,'data' => $invite->load('recipient')]);
    }
    public function deleteInvite(int $inv_id)
    {
        $invite = Invite::where('id',$inv_id)->delete();
        return response()->json(['success' => $invite],204);
    }
}
