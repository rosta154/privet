<?php


namespace App\Http\Traits;


use App\Helpers\DTO\Point;
use App\Helpers\Interfaces\IScheduleHelper;
use App\Models\BusinessItem;
use App\Models\BusinessType;
use App\Models\Treatment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

trait BusinessSearch
{
    /**
     * Filter business by all conditions.
     *
     * @param Collection $businesses
     * @param Request $request
     * @return array
     */
    private function filterBusinesses(Collection $businesses, Request $request)
    {
        $businessesWithDistance = $this->addDistance($businesses, $request);
        $filteredBusiness = $this->filterBusinessByDistance($businessesWithDistance);
        $orderedBusinesses = $this->orderBusinessByDistance($filteredBusiness);
        return $this->removeUnnecessaryInformation($orderedBusinesses);
    }

    /**
     * Get business name by id.
     *
     * @param int $typeId
     * @return string
     */
    private function getType(int $typeId)
    {
        return Str::plural(BusinessType::TYPES[$typeId]);
    }

    /**
     * Get customer location.
     *
     * @param Request $request
     * @return Point
     */
    private function getCustomerLocation(Request $request)
    {
        if(!$request->home) {
            $customer = auth()->user();
            return new Point($customer->latitude, $customer->longitude);
        } else {
            return new Point($request->latitude, $request->longitude);
        }
    }

    /**
     * Add distance property to all businesses items.
     *
     * @param Collection $businesses
     * @param Request $request
     * @return Collection
     */
    private function addDistance(Collection $businesses, Request $request)
    {
        $mainTreatments = Treatment::mainTreatments($request->business_type_id)->toArray();
        $customerLocation = $this->getCustomerLocation($request);
        $scheduleHelper = resolve(IScheduleHelper::class);
        $searchTime = Carbon::parse($request->time);

        return $businesses->map(function ($business) use ($customerLocation, $request, $mainTreatments, $scheduleHelper, $searchTime) {
            $itemsWithDistance = $business->businessItems->where('main', !$request->home)->map(function ($item) use ($customerLocation) {
                $item->distance = $this->getNearestDistance($item, $customerLocation);
                return $item;
            });

            if($request->home) {
                $itemsWithDistance = $itemsWithDistance->filter(function ($item) {
                    return $item->distance < $item->radius;
                })->values();
            }

            // TODO change for all business types.
            $business->nonWorkingPrice = $business->vets
                ->whereIn('treatment_id', $mainTreatments)
                ->first()->non_working_price;

            if($itemsWithDistance->count() != 0) {
                $business->fitItems = $itemsWithDistance;
                $business->fitItem = $itemsWithDistance->first();
                $nearestTime = $scheduleHelper->getItemTimeSchedule($business->fitItem, $searchTime);
                if ($nearestTime != null) {
                    $business->nearestTime = $nearestTime;
                    return $business;
                }
            }
        });
    }

    /**
     * Get minimal distance.
     *
     * @param BusinessItem $item
     * @param Point $customerLocation
     * @return mixed
     */
    private function getNearestDistance(BusinessItem $item, Point $customerLocation)
    {
        if($item->cities->count() != null) {
            $item->radius = BusinessItem::DEFAULT_RADIUS;
            $distances = new Collection();
            foreach ($item->cities as $city) {
              $distances->add($this->calculateDistance($customerLocation, new Point($city['latitude'], $city['longitude'])));

                $distance = $this->calculateDistance($customerLocation, new Point($item->latitude, $item->longitude));
                $minutes = $distance * 5;
                $hours = $minutes / 60;
                if($minutes > 120){
                    $item->time = 'more than 2 hours';
                }
                else if($minutes < 120) {
                    $item->time = date('H:i', mktime($hours, $minutes));
                }
            }

            return $distances->min();
        }
        $distance = $this->calculateDistance($customerLocation, new Point($item->latitude, $item->longitude));
           $minutes = $distance * 5;
           $hours = $minutes / 60;
           if($minutes > 120){
               $item->time = 'more than 2 hours';
           }
           else if($minutes < 120) {
               $item->time = date('H:i', mktime($hours, $minutes));
           }
        return $distance;
    }


    /**
     * Remove businesses which unsuitable.
     *
     * @param Collection $business
     * @return Collection
     */
    private function filterBusinessByDistance(Collection $business)
    {
        return $business->filter(function ($business) {
            return $business != null && count($business->fitItems) > 0;
        });
    }

    private function orderBusinessByDistance(Collection $business)
    {
        return $business->sortBy('fitItem.distance');
    }

    /**
     * Remove unnecessary information from collection.
     *
     * @param Collection $business
     * @return array
     */
    private function removeUnnecessaryInformation(Collection $business)
    {
        $array = $business->toArray();
        for($i = 0; $i < count($array); $i++) {
            unset($array[$i]['business_items']);
        }
        return collect($array)->values()->toArray();
    }

}
