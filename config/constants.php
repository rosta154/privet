<?php

return [
    'image_folder' => [
        'pets_photo' => [
            'save_path' => 'public/img/photo_pets/',
            'get_path' => '/storage/img/photo_pets/',
        ],
        'business_photo' => [
            'save_path' => 'public/img/photo_business/',
            'get_path' => '/storage/img/photo_business/',
        ],
        'product_photo' => [
            'save_path' => 'public/img/photo_product/',
            'get_path' => '/storage/img/photo_product/',
        ],
        'store_photo' => [
            'save_path' => 'public/img/photo_store/',
            'get_path' => '/storage/img/photo_store/',
        ],

        'content_photo' => [
            'save_path' => 'public/img/photo_content/',
            'get_path' => '/storage/img/photo_content/',
        ],
        'treatment_photo' => [
            'save_path' => 'public/img/photo_treatment/',
            'get_path' => '/storage/img/photo_treatment/',
        ],
    ],
    'appointment_count' => 3,
    'app_url' => 'https://app.downlod'
];
