<?php

namespace App\Http\Controllers;

use App\Http\Requests\AppointmentApproveRequest;
use App\Http\Requests\AppointmentClientRequest;
use App\Http\Requests\AppointmentCreateRequest;
use App\Http\Requests\AppointmentNotifyAdminRequest;
use App\Http\Requests\AppointmentsBusinessRequest;
use App\Http\Requests\AppointmentUpdateRequest;
use App\Models\Appointment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\HistoryAppointment;
class AppointmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param AppointmentCreateRequest $request
     * @return JsonResponse
     */
    public function store(AppointmentCreateRequest $request)
    {
        $appointment = Appointment::create($request->all());
        return $this->created($appointment);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AppointmentUpdateRequest $request
     * @param Appointment $appointment
     * @return JsonResponse
     */
    public function update(AppointmentUpdateRequest $request, Appointment $appointment)
    {
        $user = auth()->user();
        if($user->client && $user->client->id == $appointment->client_id) {
            $appointment->status = Appointment::PENDING_STATUS;
        }

        $appointment->update($request->all());
        $appointment->load(['items'])->updateServices();
        return $this->ok();
    }
    /**
     * Display the specified resource.
     *
     * @param Appointment $appointment
     * @return JsonResponse
     */
    public function show(Appointment $appointment)
    {
        return $this->ok($appointment->load('client.user', 'businessItem.business.user','comments','decline_comments'));
    }

    public function searchUser(Request $request)
    {
        $user = Appointment::whereHas('items.pet.client.user', function ($q) use ($request) {
            return $q->where('phone', $request->phone);
        })->with(['items.pet.client.user' => function ($query) use ($request) {
            return $query->where('phone', $request->phone);
        }])
            ->whereDate('date', '>', Carbon::now())
            ->orderBy('date', 'asc')
            ->first();

        return $this->ok($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //TODO + delete items
    }

    /**
     * Get client's appointments by business type.
     *
     * @param AppointmentClientRequest $request
     * @return JsonResponse
     */
    public function clientAppointments(AppointmentClientRequest $request)
    {
        $clientId = User::getJWTUser()->client;
        $operand = $request->history ? '<' : '>=';
        $appointments = Appointment::where('client_id', $clientId)
            ->getByBusinessType()
            ->getByType()
            ->where('date', $operand, Carbon::now())
            ->takeCount()
            ->get();

        return $this->ok($appointments);
    }

    /**
     * Get all business appointments between time interval.
     *
     * @param AppointmentsBusinessRequest $request
     * @param $businessId
     * @return JsonResponse
     */
    public function businessAppointments(AppointmentsBusinessRequest $request, $businessId)
    {
        $appointments = Appointment::whereBetween('date', [$request->start_date, $request->end_date])
            ->where('business_id', $businessId)
            ->getByType()
            ->getByStatus()
            ->payed()
            ->approved()
            ->with(['client', 'client.user', 'items.pet', 'business'])
            ->get();

        return $this->ok($appointments);
    }

    /**
     * Get business's nearest appointments.
     *
     * @param int $businessId
     * @return JsonResponse
     */
    public function businessNearest(int $businessId)
    {
        $appointments = Appointment::where('business_id', $businessId)
            ->where('status',Appointment::APPROVED_STATUS)
            ->whereDate('date', '>', Carbon::now())
            ->orderBy('date', 'desc')
            ->take(config('constants.appointment_count'))
            ->with(['client', 'client.user', 'items.pet', 'business'])
            ->get();

        return $this->ok($appointments);
    }

    /**
     * Change appointment status by provider.
     *
     * @param AppointmentApproveRequest $request
     * @param Appointment $appointment
     * @return JsonResponse
     */
    public function changeStatus(AppointmentApproveRequest $request, Appointment $appointment)
    {
        $appointment->changeStatus($request);
        return $this->ok();
    }

    /**
     * Change appointment status as notify admin.
     *
     * @param AppointmentNotifyAdminRequest $request
     * @param Appointment $appointment
     * @return JsonResponse
     */
    public function notifyAdmin(AppointmentNotifyAdminRequest $request, Appointment $appointment)
    {
        $appointment->notifyAdmin($request);
        return $this->ok();
    }

    public function businessNearestClient()
    {
        $clientId = auth()->user()->client->id;
        $appointments = Appointment::where('client_id', $clientId)
            ->where('status',Appointment::APPROVED_STATUS)
            ->whereDate('date', '>', Carbon::now())
            ->orderBy('date', 'desc')
            ->take(2)
            ->with(['client', 'client.user', 'items.pet', 'business'])
            ->get();

        return $this->ok($appointments);
    }
}
