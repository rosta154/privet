<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed week_day
 */
class Schedule extends Model
{
    /******* Constants *******/
    public const WEEK_DAYS = [
        0 => 'יום ראשון',
        1 => 'יום שני',
        2 => 'יום שלישי',
        3 => 'יום רביעי',
        4 => 'יום חמישי',
        5 => 'יום שישי',
        6 => 'יום שבת',
    ];


    /******* Properties *******/
    protected $fillable =[
        'week_day',
        'name',
        'opening_time',
        'closing_time',
        'rule',
        'available',
        'business_item_id',
    ];

    protected $casts = [
        'available' => 'boolean',
        'rule' => 'boolean',
    ];


    /******* Relations *******/
    public function business()
    {
        return $this->belongsTo(BusinessItem::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }

    /******* Accessors *******/
    public function getNameAttribute($value)
    {
        if($value == null) {
            return self::WEEK_DAYS[$this->week_day];
        }
        return $value;
    }
}
