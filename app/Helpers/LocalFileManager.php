<?php

namespace App\Helpers;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Interfaces\IFileManager;
use Intervention\Image\Facades\Image;

class LocalFileManager implements IFileManager
{
    /**
     * Save new file to storage.
     *
     * @param UploadedFile $tempFile
     * @param string $path
     * @return string new file name
     */
    public function saveFile(UploadedFile $tempFile, string $path)
    {
        $fileName = $this->generateName($tempFile);
        Storage::putFileAs($path, $tempFile, $fileName);
        return $fileName;
    }

    /**
     * Save new file in base64 format to storage.
     *
     * @param string $file
     * @param string $path
     * @return string
     */
    public function saveBase64(string $file, string $path){
        $image = Image::make(file_get_contents($file))->encode('jpg');
        $fileName = 'image_'.time().rand(111,999).'.jpg';
        Storage::disk('local')->put($path.$fileName, $image);
        return $fileName;
    }

    /**
     * Delete file from storage.
     *
     * @param string $name
     * @param string $path
     * @return void
     */
    public function deleteFile(string $name, string $path)
    {
        Storage::delete($path . '/' . $name);
    }

    /**
     * Generate new file name.
     *
     * @param UploadedFile $tempFile
     * @return string
     */
    private function generateName(UploadedFile $tempFile)
    {
        $name = Str::random();
        $extension = $tempFile->getClientOriginalExtension();
        return $name . '.' . $extension;
    }
}
