<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AppointmentChangeStatusRequest
 * @package App\Http\Requests
 * @property  boolean approved
 */
class AppointmentApproveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        $appointment = request()->appointment;
//        $businesses = collect(User::getJWTUser()->businesses)->pluck('id');
//        return $businesses->contains($appointment->business_id);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'approved' => 'boolean',
            'ready' => 'boolean',
            'note' => 'max:255',
            'endDate' => 'required_if:approved,true'
        ];
    }
}
