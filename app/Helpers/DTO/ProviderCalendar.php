<?php


namespace App\Helpers\DTO;


use Illuminate\Support\Collection;

class ProviderCalendar
{
    public $timeIntervals;
    public $daysAppointments;

    public function __construct(Collection $timeIntervals, Collection $daysAppointments)
    {
        $this->timeIntervals = $timeIntervals;
        $this->daysAppointments = $daysAppointments;
    }
}
