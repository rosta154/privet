<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatAppointment extends Model
{
    protected $fillable = [
        'message','appointment_item_id','sender_id'
    ];

    public static function boot() {
        parent::boot();

        static::creating(function (ChatAppointment $appointment) {
            $appointment->sender_id = auth()->user()->id;
        });
    }

    public function appointmentItem()
    {
        return $this->belongsTo(AppointmentItem::class,'appointment_item_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }



}
