<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsRequest;
use Illuminate\Http\Request;
use App\Models\ContactUs;
use Illuminate\Support\Facades\Mail;
class ContactUsController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['success' => true, 'data' => ContactUs::with('user.client','subjects')->get()]);
    }

    public function show(int $userId)
    {
        $contact = ContactUs::where('user_id',$userId)->get();
        return $this->ok($contact->load('user.client','subjects'));
    }

    /**
     * @param ContactUsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ContactUsRequest $request)
    {
        $user = auth()->user()->id;
        $contactUs = new  ContactUS($request->all());
        $contactUs->user_id = $user;
$contactUs->save();
//        Mail::send('mail.contactUs', compact('contactUs'), function ($message) use ($contactUs) {
//            $message->to('rosta154@gmail.com')
//                ->subject('test');
//        });

        return response()->json(['success' => true, 'data' => $contactUs]);
    }



}
