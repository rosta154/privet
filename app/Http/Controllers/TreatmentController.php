<?php

namespace App\Http\Controllers;

use App\Http\Requests\TreatmentSearchRequest;
use App\Models\Treatment;
use Exception;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\TreatmentCreateRequest;
use App\Http\Requests\TreatmentUpdateRequest;
use Illuminate\Http\Request;

class TreatmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->ok(Treatment::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TreatmentCreateRequest  $request
     * @return JsonResponse
     */
    public function store(TreatmentCreateRequest $request)
    {
        $treatment = Treatment::create($request->all());
        return $this->created($treatment);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Treatment $treatment
     * @return JsonResponse
     */
    public function show(Request $request, Treatment $treatment)
    {
        if($request->full) {
            $treatment->load(['info']);
        }
        return $this->ok($treatment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TreatmentUpdateRequest  $request
     * @param  Treatment  $treatment
     * @return JsonResponse
     */
    public function update(TreatmentUpdateRequest $request, Treatment $treatment)
    {
        $treatment->update($request->all());
        return $this->updated();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Treatment $treatment
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Treatment $treatment)
    {
        if($treatment->main) {
            throw new Exception('This treatment can not be deleted');
        }
        return $this->deleted($treatment->delete());
    }

    /**
     * Search treatment by business type and pet type.
     *
     * @param TreatmentSearchRequest $request
     * @return JsonResponse
     */
    public function search(TreatmentSearchRequest $request)
    {
        $treatments = Treatment::businessType()
            ->pets()
            ->whereName()
            ->main()
            ->get();

        $result = Treatment::addPriceRange($treatments);
        return $this->ok($result);
    }
}
