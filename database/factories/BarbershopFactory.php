<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Barbershop;
use Faker\Generator as Faker;

$factory->define(Barbershop::class, function (Faker $faker) {
    return [
        'price' => rand(100, 150)
    ];
});
