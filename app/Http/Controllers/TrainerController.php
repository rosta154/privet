<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrainerRequest;
use App\Models\Trainer;
use Illuminate\Http\Request;

class TrainerController extends Controller
{

    public function update(Request $request)
    {
        foreach ($request->all() as $item)
        {
            $find = Trainer::find($item['id']);
            $find->update($item);

        }
        return $this->updated($find);
    }

    public function store(Request $request){
        foreach ($request->all() as $item){
            $trainer = new Trainer($item);
            $trainer->save();
        }
        return $this->created($trainer);
    }
}
