<?php


namespace App\Providers\Validators;


use App\Models\Business;
use App\Models\Treatment;
use Illuminate\Support\ServiceProvider;

class IsManeTreatmentValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('is_mane_treatment', function ($attribute, $value, $parameters)
        {
            $businessId = request()->business_id;
            if($businessId) {
                $business = Business::find($businessId);
                if($business == null) {
                    return false;
                }
                $mainTreatments = Treatment::mainTreatments($business->type_id);
                return in_array($value, $mainTreatments->toArray());
            }
            return Treatment::isMane($value);
        });
    }

    public function register()
    {
        //
    }
}
