<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatAdminUser extends Model
{
    protected $fillable = [
        'appointment_id',
        'message',
        'sender_id',
        'recipient_id',
    ];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }

    public function recipient()
    {
        return $this->belongsTo(User::class,'recipient_id');
    }
}
