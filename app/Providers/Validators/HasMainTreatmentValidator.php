<?php


namespace App\Providers\Validators;


use App\Models\Treatment;
use Illuminate\Support\ServiceProvider;

class HasMainTreatmentValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('has_main_treatment', function ($attribute, $value, $parameters)
        {
            $serviceCollection = collect($value)->pluck('treatment_id');
            $mainTreatments = Treatment::mainTreatments(request()->type_id);
            return $serviceCollection->intersect($mainTreatments)->isNotEmpty();
        });
    }

    public function register()
    {
        //
    }
}
