<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [

        'user_id' =>$faker->unique()->numberBetween($min = 1, $max = DatabaseSeeder::USERS_COUNT),
    ];
});
