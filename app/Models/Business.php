<?php

namespace App\Models;

use App\Helpers\Interfaces\IFileManager;
use App\Helpers\Interfaces\IScheduleHelper;
use App\Http\Traits\BusinessLogic;
use App\Http\Traits\DistanceCalculator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property integer user_id
 * @property mixed treatments
 * @property integer id
 * @property integer type_id
 * @property string payme_id
 */

class Business extends Model
{
    use BusinessLogic, DistanceCalculator;

    private const ONLY_MAIN_TYPE = 1;
    private const ONLY_SECONDARY_TYPE = 2;
    private const BOTH_TYPES = 3;

    /******* Attributes *******/
    protected $fillable = [
        'name',
        'address',
        'latitude',
        'longitude',
        'license',
        'website',
        'about',
        'type_id',
        'rating',
        'count_user',
        'canceled_policy',
        'payme_id',
    ];

    protected $guarded = [
        'user_id'
    ];

    protected $appends = [
        'businessStatus'
    ];


    /******* Relations *******/
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(BusinessType::class);
    }

    public function images()
    {
        return $this->hasMany(BusinessImage::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class,'business_id');
    }

    public function mainImage()
    {
        return $this->hasOne(BusinessImage::class)->where('main', true);
    }

    public function treatments()
    {
        return $this->hasMany(Treatment::class);
    }

    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    public function barbershops()
    {
        return $this->hasMany(Barbershop::class);
    }

    public function trainers()
    {
        return $this->hasMany(Trainer::class);
    }

    public function walkers()
    {
        return $this->hasMany(Walker::class);
    }

    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }

    public function vets()
    {
        return $this->hasMany(Vet::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function businessItems()
    {
        return $this->hasMany(BusinessItem::class);
    }

    public function services()
    {
        $types = Str::plural(strtolower($this->type->name));
        return $this->$types();
    }

    public function mainService(int $serviceId)
    {
        $types = Str::plural(strtolower($this->type->name));
        return $this->$types()->where('id', $serviceId);
    }

    public function seller()
    {
        return $this->hasOne(Seller::class);
    }

    public function commission()
    {
        return $this->hasOne(BusinessCommission::class);
    }

    public function businessCommission()
    {
        return $this->hasOne(Commission::class)
            ->where('life_time', null)
            ->orWhereDate('life_time', '>', Carbon::now());
    }


    /**
     * Override base method for getting user id field in saving.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        // Before create.
        static::creating(function (Business $business) {
            $business->user_id = $business->user_id ?? User::getJWTUser()->id;
        });

        // After create.
        static::created(function (Business $business) {
            $request = request();
            $fileManager = resolve(IFileManager::class);
            if ($request->type_id == BusinessType::STORE) {
                $business->addStore($request->schedule, $business->id);
            } else {
                $business->addServices($request->services, $business->id, $business->type_id);
                $business->addBusinessItems($request->items, $business);
            }
            $business->saveImages($request->all(), $fileManager);
        });

        // After update.
        static::updated(function (Business $business) {
//            $request = request();
//            $fileManager = resolve(IFileManager::class);
//            if ($request->type_id == BusinessType::STORE) {
//                $business->updateStore($business, $request->schedule, $business->id);
//            } else {
//                $business->updateServices($request->services, $business->id, $business->type_id);
//                $business->updateBusinessItems($request->items, $business->id,$business);
//            }
//            $business->updateImages($request->all(), $fileManager);
        });
    }

    /**
     * Get current commission value.
     */
    public function getCurrentCommissions()
    {
        if($this->businessCommission == null) {
            $this->currentCommission = $this->type->businessCommission;
        } else {
            $this->currentCommission = $this->businessCommission;
        }
        return $this;
    }

    /**
     * Generate customer calendar.
     *
     * @param array $data
     * @return array
     */
    public function customerCalendar(array $data)
    {
        $this->load('businessItems');
        $helper = resolve(IScheduleHelper::class);
        $itemIds = $this->businessItems->where('main', $data['main'])->pluck('id');

        return array(
            'main' => $data['main'],
            'schedule' => $helper->generateTimeSchedule($itemIds->toArray(), $data['start'], $data['end'])
        );
    }

    /**
     * Generate provider calendar.
     *
     * @param array $data
     * @return array
     */
    public function providerCalendar(array $data)
    {
        $this->load('businessItems');
        $helper = resolve(IScheduleHelper::class);
        $itemIds = $this->businessItems->where('main', $data['main'])->pluck('id');

        $providerCalendar = $helper->generateProviderSchedule($itemIds->toArray(), $data['start'], $data['end']);

        return array(
            'main' => $data['main'],
            'timeInterval' => $providerCalendar->timeIntervals,
            'daysAppointments' => $providerCalendar->daysAppointments,
        );
    }

//    public function cities()
//    {
//        $cities = $this->businessItems->pluck('city_id');
//        $this->cities = City::whereIn('id', $cities)->get();
//    }

    /******* Scopes *******/
    public function scopeSearchByName($query)
    {
        $request = request();
        if($request->name) {
            $query->where('name', 'LIKE', "%$request->name%");
        }
        return $query;
    }

    /******* Accessors *******/
    public function getBusinessStatusAttribute()
    {
        $main = $this->businessItems->where('main', true)->first();
        $secondaryType = $this->businessItems->where('main', false)->first();

        if($main && $secondaryType) {
            return self::BOTH_TYPES;
        } elseif ($main) {
            return self::ONLY_MAIN_TYPE;
        }
        return self::ONLY_SECONDARY_TYPE;
    }
}
