<?php

namespace App\Helpers;

use Exception;
use Authy\AuthyApi;
use App\Helpers\Interfaces\ISmsService;


class TwilioSmsClient implements ISmsService
{
    private const COUNTRY_CODE = '972';
    private const DEVELOPERS_PHONES = [
        '0502216092',
        '0660012027'
    ];

    private $twilioClient;

    public function __construct()
    {
        $this->twilioClient = new AuthyApi(env('TWILIO_API_KEY'));
    }

    /**
     * Sent sms to user phone.
     *
     * @param string $phone
     * @return bool
     * @throws \Exception
     */
    public function checkPhone(string $phone)
    {
        if($this->isDeveloper($phone)) {
            return true;
        }
        $result = $this->twilioClient->phoneVerificationStart($phone, self::COUNTRY_CODE);

        if (!$result->ok()){
            throw new Exception($result->message());
        }
        return true;
    }

    /**
     * Verify sms code.
     *
     * @param string $phone
     * @param int $code
     * @return bool
     * @throws \Exception
     */
    public function codeVerification(string $phone, int $code)
    {
        if($this->isDeveloper($phone)) {
            return true;
        }
            $result = $this->twilioClient->phoneVerificationCheck(
                $phone,
                self::COUNTRY_CODE,
                $code
            );
        if (!$result->ok()){
            throw new \Exception($result->message());
        }
        return true;
    }

    /**
     * Check that user is developer.
     *
     * @param string $phone
     * @return boolean
     */
    private function isDeveloper(string $phone)
    {
        return true;
        //return App::isLocal() && in_array($phone, self::DEVELOPERS_PHONES);
    }
}
