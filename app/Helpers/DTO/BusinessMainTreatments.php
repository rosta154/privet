<?php


namespace App\Helpers\DTO;


use App\Models\BusinessType;
use App\Models\Treatment;

class BusinessMainTreatments
{
    public $receptionId;
    public $tripsId;

    public function __construct(int $businessType)
    {
        switch ($businessType) {
            case BusinessType::VETERINARIAN;
                $this->receptionId = Treatment::VET_HOME_TREATMENT_ID;
                $this->tripsId = Treatment::VET_CLINIC_TREATMENT_ID;
                break;
            case BusinessType::TRAINER:
                $this->receptionId = Treatment::TRAINER_DAY_TREATMENT_ID;
                $this->tripsId = Treatment::TRAINER_NIGHT_TREATMENT_ID;
                break;
            case BusinessType::HOTEL:
                $this->receptionId = Treatment::HOTEL_DAY_TREATMENT_ID;
                $this->tripsId = Treatment::HOTEL_NIGHT_TREATMENT_ID;
                break;
            case BusinessType::BARBERSHOP:
                $this->receptionId = Treatment::BARBERSHOP_TREATMENT_ID;
                $this->tripsId = Treatment::PORTABLE_BARBERSHOP_TREATMENT_ID;
                break;
            case BusinessType::WALKER:
                $this->receptionId = Treatment::WALKER_DAY_TREATMENT_ID;
                $this->tripsId = Treatment::WALKER_NIGHT_TREATMENT_ID;
                break;
        }
    }
}
