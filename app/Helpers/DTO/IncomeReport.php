<?php


namespace App\Helpers\DTO;


class IncomeReport
{
    public $receptions;
    public $trips;
    public $totalIncome;
    public $totalCommission;
    public $totalNetto;

    public function __construct(ProviderAppointmentReport $receptions, ProviderAppointmentReport $trips)
    {
        $this->receptions = $receptions;
        $this->trips = $trips;
        $this->totalIncome = $receptions->income + $trips->income;
        $this->totalCommission = $receptions->commission + $trips->commission;
        $this->totalNetto = $this->totalIncome - $this->totalCommission;
    }

}
