<?php

namespace App\Models;

use App\Helpers\DTO\Point;
use App\Http\Traits\BusinessLogic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Store extends Model
{
    use BusinessLogic;

    /******* Properties *******/
    protected $fillable = [
        'image',
        'name',
        'website',
        'about',
        'business_id',
        'treatment_id',
    ];

    /******* Relations *******/
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function businesses()
    {
        return $this->belongsTo(Business::class,'business_id');
    }

    /******* Scopes *******/
    public function scopeSearchByName($query)
    {
        $request = request();
        if($request->name) {
            $query->where('name', 'LIKE', "%$request->name%");
        }
        return $query;
    }

    /******* Static methods *******/

    /**
     * Add distance from current user to store.
     *
     * @param Collection $businesses
     */
    public static function addDistance(Collection $businesses)
    {
        $user = auth()->user();
        $businesses->each(function ($business) use ($user) {
            $business->distance = $business->calculateDistance(new Point($user->latitude, $user->longitude), new Point($business->latitude, $business->longitude));
        });
    }
}
