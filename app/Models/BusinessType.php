<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property  int id
 */
class BusinessType extends Model
{
    /******* TYPES CONSTANTS *******/
    const TYPES = [
        1 => 'Vet',
        2 => 'Store',
        3 => 'Barbershop',
        4 => 'Trainer',
        5 => 'Hotel',
        6 => 'Walker'
    ];
    const VETERINARIAN = 1;
    const STORE = 2;
    const BARBERSHOP = 3;
    const TRAINER = 4;
    const HOTEL = 5;
    const WALKER = 6;


    /******* Attributes *******/
    protected $fillable = [
        'name',
        'cancellation_fee'
    ];


    /******* Relations *******/
    public function businesses()
    {
        return $this->hasMany(Business::class, 'type_id');
    }

    public function treatments()
    {
        return $this->hasMany(Treatment::class, 'business_type_id');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function commission()
    {
        return $this->hasOne(BusinessCommission::class);
    }

    public function businessCommission()
    {
        return $this->hasOne(Commission::class)
            ->where('life_time', null)
            ->orWhereDate('life_time', '>', Carbon::now());
    }
}
