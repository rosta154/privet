<?php

namespace App\Helpers\DTO;


class DaySchedule
{
    public $date;
    public $availableTimes;
    public $nonWorkingTimes;

    public function __construct(string $date, array $availableTimes, array $nonWorkingTimes)
    {
        $this->date = $date;
        $this->availableTimes = $availableTimes;
        $this->nonWorkingTimes = $nonWorkingTimes;
    }
}
