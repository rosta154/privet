<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusinessCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

        return [
            'type_id' => 'required|available_business_type',
            'name' => 'required',
            'address' => 'required|string|max:255',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'license' => 'string|max:255',
            'website' => 'regex:' . $regex,
            'about' => 'string|max:255',
            'main_image' => 'is_base64_image',
            'services' => 'required|array|current_business_type_services|has_main_treatment',
                'services.*' => 'correct_price',
                'services.*.treatment_id' => 'required|distinct',
                'services.*.price' => 'required|numeric',
            'items' => 'required|array',
                'items.*.city_id' => 'numeric|exists:cities,id',
                'items.*.main' => 'required|boolean',
//                'items.*.address' => 'required_if:items.*.main,false|max:255',
//                'items.*.latitude' => 'required_if:items.*.main,false|numeric',
//                'items.*.longitude' => 'required_if:items.*.main,false|numeric',
//                'items.*.radius' => 'required_if:items.*.main,false|numeric',
                'items.*.schedule' => 'required|array',
                    'items.*.schedule.*.week_day' => 'required|numeric',
                    'items.*.schedule.*.opening_time' => 'required|date_format:H:i',
                    'items.*.schedule.*.closing_time' => 'required|date_format:H:i|after:items.*.schedule.*.opening_time',
                    'items.*.schedule.*.rule' => 'boolean',
                    'items.*.schedule.*.available' => 'boolean|required_if:items.*.schedule.*.rule,true',
            'images' => 'array|max:3',
                'images.*' => 'is_base64_image',
        ];
    }
}
