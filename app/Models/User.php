<?php

namespace App\Models;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int id
 * @property string phone
 * @property boolean admin
 * @property mixed client
 * @property mixed businesses
 * @property string device_token
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;

    public const DEFAULT_LATITUDE = 31.47;
    public const DEFAULT_LONGITUDE = 35.14;

    /******* Properties *******/
    protected $fillable = [
        'phone',
        'first_name',
        'last_name',
        'email',
        'address',
        'latitude',
        'longitude',
        'device_token',
        'admin',
        'admin_password',
        'password'

    ];

    protected $guarded = [
        'admin',
        'password',
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
        'deleted_at',
        'password',
        'admin_password',
    ];


    /******* Relations *******/
    public function businesses()
    {
        return $this->hasMany(Business::class)->select(['id', 'name', 'type_id', 'user_id']);
    }

    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function deviceTokens()
    {
        return $this->hasMany(DeviceToken::class);
    }

    public function permission()
    {
       return $this->hasMany(AdminPermission::class,'admin_id');
     }

    public function senders()
    {
        return $this->hasMany(ChatAppointment::class,'sender_id');
    }

    public function sender()
    {
        return $this->hasMany(Invite::class,'sender_id');
    }
    public function recipient()
    {
        return $this->hasMany(Invite::class,'recipient_id');
    }
    public function contacts()
    {
        return $this->hasMany(ContactUs::class);
    }

    public function contact_us()
    {
        return $this->hasMany(ContactUs::class,'provider_id');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class,'user_id');
    }

    public function chatSender()
    {
        return $this->hasMany(ChatAdminUser::class,'sender_id');
    }

    public function chatRecipient()
    {
        return $this->hasMany(ChatAdminUser::class,'recipient_id');
    }


    /******* JWT *******/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function scopeGetByEmail($query, $email)
    {
        return $query->where('email', $email)->first();
    }
    public function scopeGetByName($query, $first_name)
    {
        if (!empty($first_name)) {
            return $query->orWhere('first_name','LIKE' ,"%$first_name%");
        }
        return $query;
    }
    public function scopeByEmail($query, $email)
    {
        if (!empty($email)) {
            return $query->orWhere('email','LIKE', "%$email%");
        }
        return $query;
    }
    public function scopeGetByPhone($query, $phone)
    {
        if (!empty($phone)) {
            return $query->orWhere('phone', 'LIKE',"%$phone%");
        }
        return $query;
    }

    public function getJWTCustomClaims()
    {
        return [
            'user' => [
                'id' => $this->id,
                'phone' => $this->phone,
                'admin' => $this->admin,
                'client' => $this->client ? $this->client->id : null,
                'businesses' => $this->businesses()->select(['id', 'type_id'])->get()
            ]
        ];
    }


    /**
     * Get user info from jwt claims.
     *
     * @return User
     */
    public static function getJWTUser()
    {
        return JWTAuth::parseToken()->getPayload()->get('user');
    }


}
