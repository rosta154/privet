<?php

namespace App\Http\Requests;

use App\Models\BusinessItem;
use App\Models\TemporarySchedule;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class DeleteTemporaryScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $businesses = collect(User::getJWTUser()->businesses)->pluck('id');
        $items = BusinessItem::whereIn('business_id', $businesses)->pluck('id');

        $schedulesIds = request()->temporary_schedules;
        $deletedSchedules = TemporarySchedule::whereIn('id', $schedulesIds)->get();

        return $deletedSchedules->whereIn('business_item_id', $items)->count() == count($schedulesIds);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "temporary_schedules" => 'required|array',
            "temporary_schedules.*" => 'required|exists:temporary_schedules,id',
        ];
    }
}
