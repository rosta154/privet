<?php

namespace App\Helpers\Interfaces;

interface ISmsService
{
    function checkPhone(string $phone);
    function codeVerification(string $phone, int $code);
}
