<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = [
        'subject_id',
        'message',
        'status',
        'user_role',
        'user_id',
        'provider_id'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function (ContactUs $contactUs) {
            $contactUs->status = 0;

        });

    }

        public function subjects()
    {
        return $this->belongsTo(Subject::class,'subject_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function provider()
    {
        return $this->belongsTo(User::class,'provider_id');
    }
}

