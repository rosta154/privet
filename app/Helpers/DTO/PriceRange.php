<?php


namespace App\Helpers\DTO;


class PriceRange
{
    public $max;
    public $min;

    public function __construct($min, $max)
    {
        $this->max = $max;
        $this->min = $min;
    }
}
