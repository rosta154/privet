<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPetType extends Model
{
    protected $fillable = ['subscription_id','pet_type_id'];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function petType()
    {
        return $this->belongsTo(PetType::class);
    }
}
