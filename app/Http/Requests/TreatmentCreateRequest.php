<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'min_price' => 'required|numeric',
            'max_price' => 'required|numeric',
            'business_type_id' => 'required|exists:business_types,id',
            'pet_type_id' => 'exists:pet_types,id',
        ];
    }
}
