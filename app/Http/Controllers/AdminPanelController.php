<?php

namespace App\Http\Controllers;

use App\Http\Requests\BusinessAddPayMeInfoRequest;
use App\Http\Traits\PushNotificationTrait;
use App\Models\AppointmentComment;
use App\Models\AppointmentItem;
use App\Models\BusinessCommission;
use App\Models\ChatAppointment;
use App\Http\Requests\ChatAppointmentRequest;
use App\Http\Requests\RatingRequest;
use App\Models\AdminPermission;
use App\Models\Client;
use App\Models\ContactUs;
use App\Models\DeviceToken;
use App\Models\Notification;
use App\Models\Rating;
use App\Models\Subscription;
use App\Models\Treatment;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Models\Business;
use App\Models\BusinessType;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Hash;
use App\Models\Pet;
use App\Models\PetType;
use Illuminate\Support\Facades\DB;

class AdminPanelController extends Controller
{
    use PushNotificationTrait;

    private const NOTIFICATION_TITLE = "Pri-Vet";


// Total Appointment for admin
    public function totalAppointment()
    {
        $totalAppointment = Appointment::count();
        return $this->ok($totalAppointment);
    }

    public function appointment(Appointment $appointment)
    {
        return $this->ok($appointment->load('client.user', 'businessItem', 'business', 'ratings','cashbacks'));
    }

    public function appointmentClient(Client $client)
    {
        $appointment = Appointment::where('client_id', $client->id)->get();
        return $this->ok($appointment->load('client.user', 'businessItem', 'business', 'ratings'));
    }


    //Appointments graphic for admin
    public function appointmentGraphic()
    {
        $pending_approval = Appointment::where('status', Appointment::PENDING_STATUS)->count();
        $approved = Appointment::where('status', Appointment::APPROVED_STATUS)->count();
        $pending_payment = Appointment::where('payment_status', Appointment::PENDING_PAYMENT_STATUS)->count();

        return response()->json([
            'pending_approval' => $pending_approval,
            'approved' => $approved,
            'pending_payment' => $pending_payment,

        ]);
    }

    //for admin
    public function businessesGraphic()
    {
        $veterinarian = Business::where('type_id', BusinessType::VETERINARIAN)->count();
        $store = Business::where('type_id', BusinessType::STORE)->count();
        $barbershop = Business::where('type_id', BusinessType::BARBERSHOP)->count();
        $trainer = Business::where('type_id', BusinessType::TRAINER)->count();
        $hotel = Business::where('type_id', BusinessType::HOTEL)->count();
        $walker = Business::where('type_id', BusinessType::WALKER)->count();
        $all = $veterinarian + $store + $barbershop + $trainer + $hotel + $walker;

        return response()->json([
            'veterinarian' => $veterinarian,
            'store' => $store,
            'barbershop' => $barbershop,
            'trainer' => $trainer,
            'hotel' => $hotel,
            'walker' => $walker,
            'all' => $all
        ]);
    }

    //for admin
    public function businessesList(int $typeType)
    {
        $businessesList = Business::where('type_id', $typeType)->get();
        return $this->ok($businessesList);

    }

    //Amount of users for admin
    public function amountOfUsers()
    {
        $amount_user = User::count();
        $total_appointment = Appointment::count();
        $business_rating = Business::where('rating', '>=', '1.00')->pluck('rating')->avg();


        return response()->json([
            'amount_user' => $amount_user,
            'total_appointment' => $total_appointment,
            'business_rating' => $business_rating,
        ]);

    }

    //for admin
    public function adminAll()
    {
        $admin = User::where('admin', true)->get();

        return $this->ok($admin->load('permission')->makeVisible('admin_password'));

    }
    //for admin
    // TODO request validation
    public function createUser(Request $request)
    {
        $admin = User::create([


            'password' => Hash::make($request['password']),
            'phone' => $request['phone'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'admin' => 1,
            'admin_password' => $request->password,
        ]);
        foreach ($request->all()['permission'] as $item) {
            AdminPermission::create([
                'admin_id' => $admin->id,
                'permission' => $item,
            ]);
        }
        return response()->json(['success' => true, 'data' => $admin->load('permission')]);
    }

    public function adminUpdate(User $user, Request $request)
    {
        $user->permission()->delete();
        $user->update([
            'password' => Hash::make($request['password']),
            'phone' => $request['phone'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'admin' => 1,
            'admin_password' => $request->password,
        ]);
        foreach ($request->all()['permission'] as $item) {
            AdminPermission::create([
                'admin_id' => $user->id,
                'permission' => $item,
            ]);
        }
        return response()->json(['success' => true, 'data' => $user->load('permission')]);
    }

    public function petGraphic()
    {
        $pet_dogs = Pet::where('pet_type_id', PetType::PET_TYPE_DOGS)->count();
        $pet_cat = Pet::where('pet_type_id', PetType::PET_TYPE_CAT)->count();
        $pet_horse = Pet::where('pet_type_id', PetType::PET_TYPE_HORSE)->count();
        $pet_parrot = Pet::where('pet_type_id', PetType::PET_TYPE_PARROT)->count();
        $pet_pig = Pet::where('pet_type_id', PetType::PET_TYPE_PIG)->count();

        return response()->json([
            'dogs' => $pet_dogs,
            'cat' => $pet_cat,
            'horse' => $pet_horse,
            'parrot' => $pet_parrot,
            'pig' => $pet_pig,
        ]);
    }

    public function multiStatistic()
    {
        $veterinarianAll = Appointment::where('business_type_id', BusinessType::VETERINARIAN)->count();
        $veterinarian = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::VETERINARIAN)->count();
        $countVet = 0;
        if (!Empty($veterinarian)) {
            $countVet = substr(($veterinarian / $veterinarianAll) * 100, '0', '4');
        }
        $storeAll = Appointment::where('business_type_id', BusinessType::STORE)->count();
        $store = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::STORE)->count();
        $countStore = 0;
        if (!Empty($store)) {
            $countStore = substr(($store / $storeAll) * 100, '0', '4');
        }
        $barbershopAll = Appointment::where('business_type_id', BusinessType::BARBERSHOP)->count();
        $barbershop = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::BARBERSHOP)->count();
        $countBarbershop = 0;
        if (!Empty($barbershop)) {
            $countBarbershop = substr(($barbershop / $barbershopAll) * 100, '0', '4');
        }
        $trainerAll = Appointment::where('business_type_id', BusinessType::TRAINER)->count();
        $countTrainer = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::TRAINER)->count();
        $trainer = 0;
        if (!Empty($trainer)) {
            $countTrainer = substr(($trainer / $trainerAll) * 100, '0', '4');
        }
        $hotelAll = Appointment::where('business_type_id', BusinessType::HOTEL)->count();
        $hotel = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::HOTEL)->count();
        $countHotel = 0;
        if (!Empty($hotel)) {
            $countHotel = substr(($hotel / $hotelAll) * 100, '0', '4');
        }
        $walkerAll = Appointment::where('business_type_id', BusinessType::WALKER)->count();
        $walker = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::WALKER)->count();
        $countWalker = 0;
        if (!Empty($walker)) {
            $countWalker = substr(($walker / $walkerAll) * 100, '0', '4');
        }
        $totalAll = Appointment::whereIn('business_type_id', [1, 2, 3, 4, 5, 6])->count();
        $total = Appointment::where('status', Appointment::CANCELED_STATUS)->whereIn('business_type_id', [1, 2, 3, 4, 5, 6])->count();
        $countTotal = 0;
        if (!Empty($total)) {
            $countTotal = substr(($total / $totalAll) * 100, 0, 4);
        }
        $vet = Appointment::where('business_type_id', BusinessType::VETERINARIAN)->pluck('price')->avg();
        $barber = Appointment::where('business_type_id', BusinessType::BARBERSHOP)->pluck('price')->avg();
        $trainer = Appointment::where('business_type_id', BusinessType::TRAINER)->pluck('price')->avg();
        $hotel = Appointment::where('business_type_id', BusinessType::HOTEL)->pluck('price')->avg();
        $walker = Appointment::where('business_type_id', BusinessType::WALKER)->pluck('price')->avg();
        $store = Appointment::where('business_type_id', BusinessType::STORE)->pluck('price')->avg();
        return response()->json(
            ['avr_appointment_price' =>
                ['vet' => $vet,
                    'barber' => $barber,
                    'trainer' => $trainer,
                    'hotel' => $hotel,
                    'walker' => $walker,
                    'store' => $store,],
                'cancellation' =>
                    [
                        'veterinarian' => $countVet . '%',
                        'store' => $countStore . '%',
                        'barbershop' => $countBarbershop . '%',
                        'trainer' => $countTrainer . '%',
                        'hotel' => $countHotel . '%',
                        'Dog-walker' => $countWalker . '%',
                        'total cancellation' => $countTotal . '%'
                    ]
            ]
        );
    }


    public function appointmentBusinesses()
    {
        $veterinarianAll = Appointment::where('business_type_id', BusinessType::VETERINARIAN)->count();
        $veterinarian = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::VETERINARIAN)->count();
        $countVet = 0;
        if (!Empty($veterinarian)) {
            $countVet = substr(($veterinarian / $veterinarianAll) * 100, '0', '4');
        }
        $storeAll = Appointment::where('business_type_id', BusinessType::STORE)->count();
        $store = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::STORE)->count();
        $countStore = 0;
        if (!Empty($store)) {
            $countStore = substr(($store / $storeAll) * 100, '0', '4');
        }
        $barbershopAll = Appointment::where('business_type_id', BusinessType::BARBERSHOP)->count();
        $barbershop = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::BARBERSHOP)->count();
        $countBarbershop = 0;
        if (!Empty($barbershop)) {
            $countBarbershop = substr(($barbershop / $barbershopAll) * 100, '0', '4');
        }
        $trainerAll = Appointment::where('business_type_id', BusinessType::TRAINER)->count();
        $countTrainer = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::TRAINER)->count();
        $trainer = 0;
        if (!Empty($trainer)) {
            $countTrainer = substr(($trainer / $trainerAll) * 100, '0', '4');
        }
        $hotelAll = Appointment::where('business_type_id', BusinessType::HOTEL)->count();
        $hotel = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::HOTEL)->count();
        $countHotel = 0;
        if (!Empty($hotel)) {
            $countHotel = substr(($hotel / $hotelAll) * 100, '0', '4');
        }
        $walkerAll = Appointment::where('business_type_id', BusinessType::WALKER)->count();
        $walker = Appointment::where('status', Appointment::CANCELED_STATUS)->where('business_type_id', BusinessType::WALKER)->count();
        $countWalker = 0;
        if (!Empty($walker)) {
            $countWalker = substr(($walker / $walkerAll) * 100, '0', '4');
        }
        $totalAll = Appointment::whereIn('business_type_id', [1, 2, 3, 4, 5, 6])->count();
        $total = Appointment::where('status', Appointment::CANCELED_STATUS)->whereIn('business_type_id', [1, 2, 3, 4, 5, 6])->count();
        $countTotal = 0;
        if (!Empty($total)) {
            $countTotal = substr(($total / $totalAll) * 100, 0, 4);
        }

//        $total = $countVet+$countBarbershop+$countTrainer+$countHotel+$countWalker;

        return response()->json([
//            'veterinarian' => $veterinarian,
            'veterinarian' => $countVet . '%',
            'store' => $countStore . '%',
            'barbershop' => $countBarbershop . '%',
            'trainer' => $countTrainer . '%',
            'hotel' => $countHotel . '%',
            'Dog-walker' => $countWalker . '%',
            'total cancellation' => $countTotal . '%'

        ]);
    }

    public function amountOfTreatments()
    {
        $vet = Treatment::where('business_type_id', BusinessType::VETERINARIAN)->count();
        $barber = Treatment::where('business_type_id', BusinessType::BARBERSHOP)->count();
        $trainer = Treatment::where('business_type_id', BusinessType::TRAINER)->count();
        $hotel = Treatment::where('business_type_id', BusinessType::HOTEL)->count();
        $walker = Treatment::where('business_type_id', BusinessType::WALKER)->count();
        $store = Treatment::where('business_type_id', BusinessType::STORE)->count();
        return response()->json(
            [
                'vet' => $vet,
                'barber' => $barber,
                'trainer' => $trainer,
                'hotel' => $hotel,
                'walker' => $walker,
                'store' => $store,
            ]
        );

    }

    public function amountOfUser()
    {
        $vet = Business::where('type_id', BusinessType::VETERINARIAN)->pluck('user_id')->count();
        $barber = Business::where('type_id', BusinessType::BARBERSHOP)->pluck('user_id')->count();
        $trainer = Business::where('type_id', BusinessType::TRAINER)->pluck('user_id')->count();
        $hotel = Business::where('type_id', BusinessType::HOTEL)->pluck('user_id')->count();
        $walker = Business::where('type_id', BusinessType::WALKER)->pluck('user_id')->count();
        $store = Business::where('type_id', BusinessType::STORE)->pluck('user_id')->count();
        return response()->json(
            [
                'vet' => $vet,
                'barber' => $barber,
                'trainer' => $trainer,
                'hotel' => $hotel,
                'walker' => $walker,
                'store' => $store,
            ]
        );
    }

    public function businesses()
    {

        $business = Business::get()->map(function ($item) {
            $appointment_canceled = Appointment::where('status', 3)->where('business_id', $item['id'])->count();
            $appointment = Appointment::where('business_id', $item['id'])->count();
            $appointment_pending = Appointment::where('business_id', $item['id'])->whereIn('payment_status', [
                Appointment::PROVIDER_NOTIFIED_PAYMENT_STATUS,
                Appointment::ADMIN_NOTIFIED_PAYMENT_STATUS,
                Appointment::PENDING_PAYMENT_STATUS])->count();

            $item['appointment_canceled'] = $appointment_canceled;
            $item['appointment'] = $appointment;
            $item['appointment_pending'] = $appointment_pending;

            return $item;
        });


//        $users = $business->orderBy('appointment','desc')->get();


        return $this->ok($business->load(['images', 'user', 'appointments']));

    }

    public function appointments()
    {
        return response()->json(['success' => true, 'data' => Appointment::all()]);
    }

    public function getClients()
    {
        $client = Client::get()->map(function ($item) {
            $appointment_canceled = Appointment::where('status', 3)->where('client_id', $item['id'])->count();
            $appointment = Appointment::where('client_id', $item['id'])->count();
            $appointment_pending = Appointment::where('client_id', $item['id'])->whereIn('payment_status', [
                Appointment::PROVIDER_NOTIFIED_PAYMENT_STATUS,
                Appointment::ADMIN_NOTIFIED_PAYMENT_STATUS,
                Appointment::PENDING_PAYMENT_STATUS])->count();

            $item['appointment_canceled'] = $appointment_canceled;
            $item['appointment'] = $appointment;
            $item['appointment_pending'] = $appointment_pending;

            return $item;
        });

        return response()->json(['success' => true, 'data' => $client->load('user')]);
    }

    public function getClient(Client $client)
    {
        $user = Client::where('id', $client->id)->get()->map(function ($item) {
            $appointment_canceled = Appointment::where('status', 3)->where('client_id', $item['id'])->count();
            $appointment = Appointment::where('client_id', $item['id'])->where('payment_status', 4)
                ->whereDate('end_date', '<', Carbon::today()->toDateString())->count();
            $appointment_future = Appointment::where('client_id', $item['id'])->whereIn('status',
                [Appointment::PENDING_STATUS,
                    Appointment::APPROVED_STATUS,
                ])->whereDate('end_date', '>', Carbon::today()->toDateString())
                ->count();
            $appointment_pending = Appointment::where('client_id', $item['id'])->whereIn('payment_status', [
                Appointment::PROVIDER_NOTIFIED_PAYMENT_STATUS,
                Appointment::ADMIN_NOTIFIED_PAYMENT_STATUS,
                Appointment::PENDING_PAYMENT_STATUS])->count();

            $item['appointment_canceled'] = $appointment_canceled;
            $item['appointment_completed'] = $appointment;
            $item['appointment_pending'] = $appointment_pending;
            $item['future_appointment'] = $appointment_future;

            return $item;
        });

        return $this->ok($user->load('user', 'pets.appointments.treatment', 'pets.breeds', 'pets.petTypes', 'appointments'));
    }

    public function businessById(int $businessId)
    {

        $business = Business::where('id', $businessId)->get()->map(function ($item) {
            $appointment_canceled = Appointment::where('status', 3)->where('business_id', $item['id'])->count();
            $appointment = Appointment::where('business_id', $item['id'])->count();
            $appointment_pending = Appointment::where('business_id', $item['id'])->whereIn('payment_status', [
                Appointment::PROVIDER_NOTIFIED_PAYMENT_STATUS,
                Appointment::ADMIN_NOTIFIED_PAYMENT_STATUS,
                Appointment::PENDING_PAYMENT_STATUS])->count();

            $item['appointment_canceled'] = $appointment_canceled;
            $item['appointment'] = $appointment;
            $item['appointment_pending'] = $appointment_pending;

            return $item;
        });


//        $users = $business->orderBy('appointment','desc')->get();


        return $this->ok($business->load(['images', 'user', 'appointments.Items.chat']));

    }

    public function appointmentByBusiness(int $businessId)
    {
        $appointment = Appointment::where('business_id', $businessId)->with('business.vets.treatment')->get();
        return $this->ok($appointment);
    }

    public function searchClient(Request $request)
    {

//        $client = Client::whereHas('user')->with('user')->get();
        $search = Client::whereHas('user', function ($query) use ($request) {

            $query->where('first_name', 'LIKE', "%$request->search%")
                ->orWhere('email', 'LIKE', "%$request->search%")
                ->orWhere('phone', 'LIKE', "%$request->search%");

        })->with(['user' => function ($q) use ($request) {
            $q->where('first_name', 'LIKE', "%$request->search%")
                ->orWhere('email', 'LIKE', "%$request->search%")
                ->orWhere('phone', 'LIKE', "%$request->search%");


//               ->ByEmail($request->search)
//                ->GetByPhone($request->search)
//                ->GetByName($request->search);
//                ->get()


        }])->get()
//        User::ByEmail($request->search)
//            ->GetByPhone($request->search)
//            ->GetByName($request->search)
//            ->get()


            ->map(function ($item) {
                $appointment_canceled = Appointment::where('status', 3)->where('client_id', $item['id'])->count();
                $appointment = Appointment::where('client_id', $item['id'])->count();
                $appointment_pending = Appointment::where('client_id', $item['id'])->whereIn('payment_status', [
                    // TODO У тебя этот код 6 раз повторяеться, я заипался менять!!! вынеси в функцию.
                    Appointment::PROVIDER_NOTIFIED_PAYMENT_STATUS,
                    Appointment::ADMIN_NOTIFIED_PAYMENT_STATUS,
                    Appointment::PENDING_PAYMENT_STATUS])->count();

                $item['appointment_canceled'] = $appointment_canceled;
                $item['appointment'] = $appointment;
                $item['appointment_pending'] = $appointment_pending;

                return $item;
            });

        return $this->ok($search);
    }


    public function countContactUs()
    {
        $contactUs = ContactUs::count();
        return $this->ok($contactUs);
    }

//    public function commentsAll()
//    {
//        $comments = Appointment::get();
//        return $this->ok($comments);
//    }
    public function chatAppointment(ChatAppointmentRequest $request)
    {
        $chat = new ChatAppointment($request->all());
        $chat->save();
        return $this->created($chat);
    }

    public function clientByChat(Client $client)
    {

        $appointmentItem = Appointment::where('client_id', $client->id)->get();


        foreach ($appointmentItem as $item) {
            $name = $appointmentItem->business_name = $item->business->name;
        }
        if (!Empty($name)) {
            $appointmentItem->business_name = $name;
        }
        return response()->json(['data' => $appointmentItem->makeHidden(['pets', 'type', 'notifications', 'business'])->load('Items.chat')]);
    }

    public function commentClient(Client $client)
    {
        $appointment = Appointment::where('client_id', $client->id)->get();
        return $this->ok($appointment->makeHidden(['pets', 'type', 'notifications']));
    }

    public function commentAll()
    {
        $appointment = Appointment::all();
        return $this->ok($appointment->makeHidden(['pets', 'type', 'notifications']));
    }

    public function appointmentTicket()
    {
        $ticket = AppointmentItem::all();
        return $this->ok($ticket->load('appointment.business.type')->makeHidden(['appointment.pets', 'appointment.type', 'appointment.notifications']));
    }

    public function commission(Request $request)
    {
        $commission = new BusinessCommission($request->all());
        
        return response()->json(['success' => $commission->save(), 'data' => $commission]);
    }

    public function history(int $appointmentId)
    {
        $appointment = Appointment::where('id', $appointmentId)->whereHas('history', function ($query) {

        })->with(['history' => function ($query) {

        }])->get();
        return $this->ok($appointment->makeHidden(['pets', 'type', 'notifications']));
    }

    public function showChat(int $appointment)
    {
        $chat = ChatAppointment::whereHas('appointmentItem.appointment', function ($query) use ($appointment) {
            $query->where('id', $appointment);
        })->with(['appointmentItem.appointment' => function ($query) use ($appointment) {
            $query->where('id', $appointment);
        }])
            ->get();
        return $this->ok($chat);
    }

    public function sendNotifications(Request $request)
    {
        $message = $request->message;
        $types = $request->types;
        $providers = Business::whereIn('type_id', $types)->get();
        $providers->each(function ($item) use ($message) {
            foreach ($item->user->deviceTokens as $token) {
                $this->sendNotification($token->token, self::NOTIFICATION_TITLE, $message
                //,['title' => 'Pri-Vet', 'body' =>  $message] Я думаю це зайве)))
                );
                Notification::createNew(self::NOTIFICATION_TITLE, $message, $item->user->id, $item->id);
            }
        });

        $client = $request->client;
        if (isset($client)) {
            $clients = Client::get();
            $clients->each(function ($item) use ($message) {
                foreach ($item->user->deviceTokens as $token) {
                    $this->sendNotification($token->token, self::NOTIFICATION_TITLE, $message
                    //,['title' => 'Pri-Vet', 'body' =>  $message]
                    );
                    Notification::createNew(self::NOTIFICATION_TITLE, $message, $item->user->id);
                }
            });


        }
        return response()->json(['success' => true, 'message' => 'Notifications was successfully sent ']);
    }

    public function indexPetTypes()
    {

        return response()->json(['success' => true, 'data' => PetType::with('breeds')->get()]);
    }

    /**
     * Add unique PayMe id to current provider.
     *
     * @param Business $business
     * @param BusinessAddPayMeInfoRequest $request
     * @return JsonResponse
     */
    public function addPayMeMpl(Business $business, BusinessAddPayMeInfoRequest $request)
    {
        $business->payme_id = $request->payme_id;
        $business->save();
        return $this->ok();
    }

    public function comment(int $appointmentId)
    {
        $comment = AppointmentComment::where('appointment_id', $appointmentId)->get();
        return $this->ok($comment->load('pet'));
    }

    public function subscription(Request $request)
    {
     $subscription = Subscription::create($request->all());
     return response()->json(['success' => true,'data' => $subscription->load('subscriptionTreatments.treatment','subscriptionPetTypes.petType')]);
    }
}

