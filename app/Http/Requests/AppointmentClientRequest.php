<?php

namespace App\Http\Requests;

use App\Models\Treatment;
use Illuminate\Foundation\Http\FormRequest;

class AppointmentClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'history' => 'required|boolean',
            'businessType'  => 'exists:business_types,id',
            'type' => 'numeric|is_mane_treatment',
            'take' => 'numeric',
        ];
    }
}
