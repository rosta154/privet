<?php


namespace App\Providers\Validators;


use App\Models\Pet;
use App\Models\User;
use Illuminate\Support\ServiceProvider;

class CurrentUserPetValidator extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('current_user_pet', function ($attribute, $value, $parameters)
        {
            $clientId = User::getJWTUser()->client;
            $pets = Pet::where('client_id', $clientId)->pluck('id');
            return $pets->contains($value);
        });
    }

    public function register()
    {
        //
    }
}
