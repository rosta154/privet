<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;
use App\Http\Requests\SubjectRequest;
class SubjectController extends Controller
{
    //TODO comments and return values.
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['success' => true,'data' => Subject::all()]);
    }

    /**
     * @param Subject $subject
     * @param SubjectRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Subject $subject, SubjectRequest $request)
    {
        return response()->json(['success' => $subject->update($request->all()), 'data' => $subject]);
    }

    /**
     * @param SubjectRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SubjectRequest $request)
    {
        $subject = new Subject($request->all());
        return response()->json(['success' => $subject->save(), 'data' => $subject]);
    }

    /**
     * @param Subject $subject
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Subject $subject)
    {
        return response()->json(['success' => $subject->delete() , 'message' => 'deleted']);
    }
}
