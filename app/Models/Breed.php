<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    protected $fillable = [
      'name',
      'pet_type_id'
    ];


    public function pet_types()
    {
        return $this->belongsTo('App\Models\PetType');
    }

    public function pets()
    {
        return $this->hasOne('App\Models\Pet');
    }
}
