<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Walker;
use Faker\Generator as Faker;

$factory->define(Walker::class, function (Faker $faker) {
    return [
        'price' => rand(60, 120),
    ];
});
