<?php

use App\Models\Barbershop;
use App\Models\BusinessImage;
use App\Models\BusinessItem;
use App\Models\Hotel;
use App\Models\Schedule;
use App\Models\Trainer;
use App\Models\Treatment;
use App\Models\Vet;
use App\Models\Walker;
use Illuminate\Database\Seeder;
use App\Models\Business;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;

class BusinessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::fake();
        DatabaseSeeder::recreatingFolder(config('constants.image_folder.business_photo.save_path'));
        $treatments = $this->fakeTreatments();

        factory(Business::class, DatabaseSeeder::BUSINESSES_COUNT)->create()->each(function(Business $business) use ($treatments){
            $this->fakeBusinessImages($business->id);

            $homeItem = $this->fakeBusinessItems($business->id, 1, true);
            $this->fakeSchedules($homeItem->first()->id);

            $this->fakeBusinessItems($business->id, rand(0 , 3))->each(function(BusinessItem $businessItem) {
                $this->fakeSchedules($businessItem->id);
            });

            $currentTreatments = $treatments->where('business_type_id', $business->type_id)->all();
            foreach ($currentTreatments as $treatment) {
                $params = $this->getRelations($business->id, $treatment['id']);
                $this->fakeProviders($business->type_id, $params);
            }
        });
    }

    /**
     * Generate fake treatments.
     *
     * @return Collection
     */
    protected function fakeTreatments()
    {
        $mainTreatments = collect(MainTreatmentSeeder::$treatmentCollection);
        $this->call(MainTreatmentSeeder::class);
        $otherTreatments = collect(factory(Treatment::class, DatabaseSeeder::TREATMENT_COUNTS)->create());
        return $mainTreatments->merge($otherTreatments);
    }

    /**
     * Generate fake BusinessItems.
     *
     * @param int $businessId
     * @param int $count
     * @param bool $isHome
     * @return Collection
     */
    protected function fakeBusinessItems(int $businessId, int $count, $isHome = false)
    {
        return factory(BusinessItem::class, $count)->create(['business_id' => $businessId, 'main' => $isHome]);
    }

    /**
     * Generate fake Schedules.
     *
     * @param int $businessItemId
     */
    protected function fakeSchedules(int $businessItemId)
    {
        factory(Schedule::class, rand(1, 5))->create(['business_item_id' => $businessItemId]);
    }

    /**
     * Generate fake BusinessImages.
     *
     * @param int $businessId
     */
    protected function fakeBusinessImages(int $businessId)
    {
        factory(BusinessImage::class)->create(['business_id' => $businessId, 'main' => true]);
        factory(BusinessImage::class, rand(1, 3))->create(['business_id' => $businessId, 'main' => false]);
    }


    /**
     * Get necessary relations.
     *
     * @param int $businessId
     * @param int $treatmentId
     * @return array
     */
    protected function getRelations(int $businessId, int $treatmentId)
    {
        return  [
            'business_id' => $businessId,
            'treatment_id' => $treatmentId
        ];
    }

    /**
     * Generate fake providers by type.
     *
     * @param int $typeId
     * @param array $params
     */
    protected function fakeProviders(int $typeId, array $params)
    {
        switch ($typeId) {
            case 1:
                factory(Vet::class)->create($params);
                break;
            case 3:
                factory(Barbershop::class)->create($params);
                break;
            case 4:
                factory(Trainer::class)->create($params);
                break;
            case 5:
                factory(Hotel::class)->create($params);
                break;
            case 6:
                factory(Walker::class)->create($params);
                break;
            default:
                break;
        }
    }
}
