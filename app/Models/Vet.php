<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vet extends Model
{
    protected $fillable = [
        'price',
        'min_price',
        'non_working_price',
        'business_id',
        'treatment_id',
    ];

    /******* Relations *******/
    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }

    public function items()
    {
        return $this->morphMany(AppointmentItem::class, 'service');
    }
}
