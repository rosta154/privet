<?php

use Illuminate\Database\Seeder;
use App\Models\PetType;
class PetTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pet = new PetType();
        $pet->name = 'Dog';
        $pet->save();

        $pet = new PetType();
        $pet->name = 'Cat';
        $pet->save();

        $pet = new PetType();
        $pet->name = 'Horse';
        $pet->save();

        $pet = new PetType();
        $pet->name = 'Parrot';
        $pet->save();

        $pet = new PetType();
        $pet->name = 'Pig';
        $pet->save();
    }
}
