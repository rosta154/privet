<?php

namespace App\Console\Commands;

use App\Helpers\Interfaces\INotifier;
use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ProviderDayAppointmentsREminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:provider-first';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder about count of today appointments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO possible time zone conflicts. carbon return Greenwich.
        //resolve(INotifier::class)->confirmedAppointment(1,1);
//        $appointments = Appointment::where('status', Appointment::APPROVED_STATUS)
//            ->whereBetween('date', [Carbon::now()->addHours(-1), Carbon::now()->endOfDay()])
//            ->with('business.user')
//            ->get();
//
//        $businesses = $appointments->pluck('business');
//        if($appointments != null) {
//            $notifier = resolve(INotifier::class);
//            foreach ($businesses as $business) {
//
//                //$notifier->customerDailyMeetingReminder($appointment->business->name, $appointment->client->user);
//            }
//        }
    }
}
