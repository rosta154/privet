<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusinessUpdateServicesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO owner validation.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'services' => 'required|array',
            'services*.treatment_id' => 'required|exists:treatments,id',
            'services*.visit_price' => 'required_without:price',
            'services*.price' => 'required_without:visit_price',
        ];
    }
}
