<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed business_id
 * @property mixed start_date
 * @property mixed end_date
 */
class ProviderAppointmentReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $businesses = collect(User::getJWTUser()->businesses)->where('id', request()->business_id)->first();
        return $businesses != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "start_date" => 'required|date_format:Y-m-d',
            "end_date" => 'required|date_format:Y-m-d',
            //after_oe_equal
            "business_id" => 'required|exists:businesses,id',
        ];
    }
}
